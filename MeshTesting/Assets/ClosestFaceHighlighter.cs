﻿using UnityEngine;

public class ClosestFaceHighlighter : MonoBehaviour {

	public PlayerData playerData;
	public JQuadMeshFilter filter;

	int lastHighlightedID = -1;

	public void Update()
	{

		var handPos = transform.InverseTransformPoint(playerData.rightHand.position);
		int closestFace = filter.mesh.octree.ClosestQuadID(handPos);

		if (closestFace != lastHighlightedID)
		{
			UpdateFaceHighlight(lastHighlightedID, closestFace);
			lastHighlightedID = closestFace;
		}

	}

	private void UpdateFaceHighlight(int last, int current)
	{
		filter.mesh.ClearQuadHighlights(new int[] { last });
		filter.mesh.HighlightQuads(new int[] { current });
	}

}
