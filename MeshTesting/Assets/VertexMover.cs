﻿using System.Collections.Generic;
using UnityEngine;

public class VertexMover : MeshTool {

	public PlayerData player;
	int[] vertsInRange;
	List<Vector3> vertPositions = new List<Vector3>();
	Vector3 originalHandPos;
	VertexUpdate[] update;
	public Transform handTransform;

	public void StartMovingVertices()
	{
		var handPos = player.rightHand.position;
		handPos = transform.InverseTransformPoint(handPos);
		float handScale = handTransform.localScale.x;
		float radius = handScale / transform.localScale.x;
		vertsInRange = mesh.octree.VerticesWithinRange(handPos, radius).ToArray();
		vertPositions.Clear();

		for (int i = 0; i < vertsInRange.Length; i++)
		{
			vertPositions.Add((Vector3)mesh.dMesh.GetVertex(vertsInRange[i]));
		}

		originalHandPos = handPos;

		if (vertPositions.Count > 0) {
			 update = new VertexUpdate[vertPositions.Count];
		}


	}

	public void MoveVertices()
	{

		if (vertPositions.Count == 0)
		{
			return;
		}

		var dir = transform.InverseTransformPoint(player.rightHand.position) - originalHandPos;

		for (int i = 0; i < vertPositions.Count; i++)
		{
			var newPosition = vertPositions[i] + dir;
			var vertData = mesh.dMesh.GetVertexAll(vertsInRange[i]);
			vertData.v = newPosition;
			mesh.dMesh.SetVertex(vertsInRange[i], vertData);
			update[i] = VertexUpdate.FromVertexInfo(vertsInRange[i], vertData);
		}

		mesh.PushVertexUpdates(update);

	}

	public void FinishMovingVertices()
	{

	}

}
