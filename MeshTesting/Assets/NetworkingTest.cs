﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkingTest : MonoBehaviour
{
	private const string _clientID = "4cwWKPzmJZXXDiX2iH0Wydn2J58xa5nTe9oPaAsU";
	private const string _clientSecret = "RRzf128YChnHhxKniBqLiVMD0JYHz9AF7YcVTLwLPs23Oeoi6eN5RQ0LuSGl6PDK7DyxlGMrmWcjWOX3igILJT9MaM46FH14bCfHugR0TjtOCwgDNDq3zTAmaWqiMUrX";

	public struct UserAccountMessage
	{
		public bool successful;
		public string textData;

		static public UserAccountMessage Success()
		{
			UserAccountMessage msg = new UserAccountMessage();
			msg.successful = true;
			msg.textData = "success";
			return msg;
		}

		static public UserAccountMessage Failed()
		{
			UserAccountMessage msg = new UserAccountMessage();
			msg.successful = false;
			msg.textData = "failed";
			return msg;
		}

		public void Log()
		{
			Debug.Log("__LOG MESSAGE START__");
			Debug.Log("Successful: " + successful.ToString());
			Debug.Log("TexData: " + textData);
			Debug.Log("__LOG MESSAGE END__");
		}
	}

	public delegate void UserAccountCallback(UserAccountMessage msg);

	private IEnumerator SendWebRequest(UnityWebRequest webRequest, UserAccountCallback callback)
	{

		yield return webRequest.SendWebRequest();

		if (webRequest.isNetworkError)
		{
			Debug.Log(webRequest.error);	
		} else
		{
			Debug.Log(webRequest.downloadHandler.text);
		}
	}

	public void Register()
	{

		var r = GetRegisterRequest("devinsheppy1", "brinxfast01", "devinsheppy@gmail.com", _clientID, _clientSecret);

		StartCoroutine(SendWebRequest(r,
		(msg) =>
		{
			msg.Log();
		}));

	}

	private UnityWebRequest GetRegisterRequest(string userName, string userPassword, string userEmail, string clientID, string clientSecret)
	{
		List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
		formData.Add(new MultipartFormDataSection("password", userPassword));
		formData.Add(new MultipartFormDataSection("username", userName));
		formData.Add(new MultipartFormDataSection("email", userEmail));
		formData.Add(new MultipartFormDataSection("client_id:", clientID));
		formData.Add(new MultipartFormDataSection("client_secret:", clientSecret));
		Debug.Log(formData);
		UnityWebRequest r = UnityWebRequest.Post("https://masterpiecevr-api.zination.com/api/user/register/", formData);
		r.SetRequestHeader("Content-Type", "application/json");

		return r;
	}

}