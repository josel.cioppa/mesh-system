﻿using UnityEngine;

public class ToolSwitcher : MonoBehaviour {

	public GameObject left, right, up, down;
	public GameObject currentActive;

	public void Left()
	{
		currentActive.SetActive(false);
		left.SetActive(true);
		currentActive = left;
	}

	public void Right()
	{
		currentActive.SetActive(false);
		right.SetActive(true);
		currentActive = right;
	}

	public void Up()
	{
		currentActive.SetActive(false);
		up.SetActive(true);
		currentActive = up;
	}

	public void Down()
	{
		currentActive.SetActive(false);
		down.SetActive(true);
		currentActive = down;
	}

}
