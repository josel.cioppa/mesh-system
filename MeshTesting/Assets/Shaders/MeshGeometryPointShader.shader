﻿Shader "Unlit/MeshGeometryPointShader"
{
	Properties
{

    _WireColor ("Wireframe color", Color) = (1, .00, .00, 1) // color
	_ScaleX ("Scale X", Range(0.0, 0.01)) = 0.001
	_ScaleY ("Scale Y", Range(0.0, 0.01)) = 0.001
	_WireOffset ("Wire Offset", Range(0.0, 0.1)) = 0.001
	_SelectionColor("Selection Color", Color) = (0.2, 0.2, 0.2)
}

	SubShader {


		Pass{

			
			ZWrite Off
			Cull Off

			CGPROGRAM

			#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#include "Assets/Scripts/JMesh/Compute/MeshData.cginc"

			
			fixed4 _WireColor, _SelectionColor;
			float _ScaleX;
			float _ScaleY;
			float _WireOffset;

			uniform StructuredBuffer<Triangle> triangleData;
			uniform StructuredBuffer<Vertex> vertexData;

			// NOTE: do we need this? can we just use the Triangle struct?
			struct geometryInput
			{
				int3 tri : TEXCOORD0;
				int flags : TEXCOORD1;
			};

			struct fragmentInput
			{
				float4 clipPos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 color : TEXCOORD1;
			};

			struct fragOutput
			{
				fixed4 color : SV_Target;
			};

			geometryInput vert(uint id : SV_VertexID)
			{
				
				geometryInput output;
				
				output.tri = int3(
					triangleData[id].a,
					triangleData[id].b,
					triangleData[id].c
				);

				output.flags = triangleData[id].flags;

				return output;

			}


			[maxvertexcount(3)]
			void geom(point geometryInput p[1], inout PointStream<fragmentInput> triStream)
			{
						
				int3 tri =  p[0].tri;
				
								// skip inactive triangles
				if ((p[0].flags & 0x00000001) == 0) { 
					return;
				}

				fragmentInput pIn;

				// render triangle
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = _SelectionColor;
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.y].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.y].normal, 0.0f)).xyz;
				pIn.color = _SelectionColor;
				triStream.Append(pIn);
	
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.z].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color =  _SelectionColor;
				triStream.Append(pIn);
			
			}

			fragOutput frag(fragmentInput input)
			{	
				fragOutput o;
				o.color = fixed4(input.color,1);
				return o;
			}

			ENDCG
		
		}
	}
}

