﻿Shader "Unlit/MeshGeometryShader"
{
	Properties
{

    _WireColor ("Wireframe color", Color) = (1, .00, .00, 1) // color
	_ScaleX ("Scale X", Range(0.0, 0.01)) = 0.001
	_ScaleY ("Scale Y", Range(0.0, 0.01)) = 0.001
	_WireOffset ("Wire Offset", Range(0.0, 0.1)) = 0.001
	_SelectionColor("Selection Color", Color) = (0.2, 0.2, 0.2)
	_StandardColor("Standard Color", Color) = (0.8, 0.2, 0.2)

}

	SubShader {


		Pass{

			
			ZWrite On
			Cull Off

			CGPROGRAM

			#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#include "Assets/Scripts/JMesh/Compute/MeshData.cginc"

		
			fixed4 _WireColor, _SelectionColor, _StandardColor;
			float _ScaleX;
			float _ScaleY;
			float _WireOffset;

			uniform StructuredBuffer<Triangle> triangleData;
			uniform StructuredBuffer<Vertex> vertexData;

			// NOTE: do we need this? can we just use the Triangle struct?
			struct geometryInput
			{
				int3 tri : TEXCOORD0;
				int flags : TEXCOORD1;
			};

			struct fragmentInput
			{
				float4 clipPos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 color : TEXCOORD1;
			};

			struct fragOutput
			{
				fixed4 color : SV_Target;
			};

			geometryInput vert(uint id : SV_VertexID)
			{
				
				geometryInput output;
				
				output.tri = int3(
					triangleData[id].a,
					triangleData[id].b,
					triangleData[id].c
				);

				output.flags = triangleData[id].flags;

				return output;

			}

	
			[maxvertexcount(3)]
			void geom(point geometryInput p[1], inout TriangleStream<fragmentInput> triStream)
			{
						

				int3 tri =  p[0].tri;
				
				fragmentInput pIn;

				bool triangleActive = ((p[0].flags & 0x00000001) > 0);

				if (!triangleActive) { 
					return;
				}

				bool triangleSelected = (p[0].flags & 0x00000002) > 0;
				float3 selectedColor = _SelectionColor;

				if (triangleSelected) { 	
					selectedColor *=  clamp(0.5f * (1.0f + sin(_Time.x * 60.0f)), 0.3f, 0.7f);
				}

				float3 triColor =  !triangleSelected ? _StandardColor : selectedColor;
				
				// render triangle
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.y].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.y].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
	
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.z].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
	
			/*

				// render wireframe 
			
				bool renderWireframe = true;

			
				float3 a = vertexData[tri.x].position;
				float3 aN = normalize(vertexData[tri.x].normal);
				float3 b = vertexData[tri.y].position;
				float3 bN = normalize(vertexData[tri.y].normal);
				float3 c = vertexData[tri.z].position;
				float3 cN = normalize(vertexData[tri.z].normal);
				float3 ab = normalize(b - a);
				float3 bc = normalize(c - b);
				float3 ca = normalize(a - c);
				
				float3 scale = float3(_ScaleX, _ScaleY, _WireOffset);
				float3 wireColor = _WireColor;

				// line from a to b 
			
				float3 i = normalize(cross(aN, ab));
				float3 j = normalize(cross(bN, ab));
			
				float3 c1 = a - ab * scale.x - i * scale.y;
				float3 c2 = a - ab * scale.x + i * scale.y;
				float3 c3 = b + ab * scale.x - j * scale.y;
				float3 c4 = b + ab * scale.x + j * scale.y;


				

				if (renderWireframe) {
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2+ scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4+ scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					triStream.RestartStrip();

					// line from b to c 
		
					i = normalize(cross(bN, bc));
					j = normalize(cross(cN, bc));

					c1 = b - bc * scale.x - i * scale.y;
					c2 = b - bc * scale.x + i * scale.y;
					c3 = c + bc * scale.x - j * scale.y;
					c4 = c + bc * scale.x + j * scale.y;

					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					triStream.RestartStrip();

					// line from c to a 

					i = normalize(cross(cN, ca));
					j = normalize(cross(aN, ca));
			
					c1 = c - ca * scale.x - i * scale.y;
					c2 = c - ca * scale.x + i * scale.y;
					c3 = a + ca * scale.x - j * scale.y;
					c4 = a + ca * scale.x + j * scale.y;

					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = wireColor;
					triStream.Append(pIn);

					triStream.RestartStrip();
				}

				// check if individual vertices are selected
				bool vertexASelected = (vertexData[tri.x].flags & 0x00000001) > 0;
				bool vertexBSelected = (vertexData[tri.y].flags & 0x00000001) > 0;
				bool vertexCSelected = (vertexData[tri.z].flags & 0x00000001) > 0;

				// render selected vertices

				float3 selectedVertexColor = float3(1,1,0);
				scale = float3(0.01f, 0.01f, 0.001f);
				scale *= sin(_Time.x * 10.0f);


				if (vertexASelected) { 
				
					i = normalize(cross(aN, ab));
				
					c1 = a - ab * scale.x - i * scale.y;
					c2 = a - ab * scale.x + i * scale.y;
					c3 = a + ab * scale.x - i * scale.y;
					c4 = a + ab * scale.x + i * scale.y;

					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4 + scale.z * aN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(aN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					triStream.RestartStrip();

				}

				if (vertexBSelected) { 

					i = normalize(cross(bN, bc));
				
					c1 = b - bc * scale.x - i * scale.y;
					c2 = b - bc * scale.x + i * scale.y;
					c3 = b + bc * scale.x - i * scale.y;
					c4 = b + bc * scale.x + i * scale.y;

					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4+ scale.z * bN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(bN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					triStream.RestartStrip();
				}


				if (vertexCSelected) { 
				
					i = normalize(cross(cN, ca));
				
					c1 = c - ca * scale.x - i * scale.y;
					c2 = c - ca * scale.x + i * scale.y;
					c3 = c + ca * scale.x - i * scale.y;
					c4 = c + ca * scale.x + i * scale.y;

					pIn.clipPos = UnityObjectToClipPos(float4(c2 + scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
				
					pIn.clipPos = UnityObjectToClipPos(float4(c1 + scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);
			
					triStream.RestartStrip();
				
					pIn.clipPos = UnityObjectToClipPos(float4(c2+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c3+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					pIn.clipPos = UnityObjectToClipPos(float4(c4+ scale.z * cN, 1.0f));
					pIn.normal =  mul(UNITY_MATRIX_M, float4(cN, 0.0f)).xyz;
					pIn.color = selectedVertexColor;
					triStream.Append(pIn);

					triStream.RestartStrip();
				}

				*/

			}

			fragOutput frag(fragmentInput input)
			{	
				fragOutput o;
				o.color = fixed4(input.color,1);
				return o;
			}

			ENDCG
		
		}
	}
}

