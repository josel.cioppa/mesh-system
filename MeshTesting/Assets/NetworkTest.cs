﻿
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

public static class NetworkTest
 {
	[MenuItem("Network/SendRequest")]
	private static void DoSomething()
	{
		var test = GameObject.Find("GameObject").GetComponent<NetworkingTest>();
		test.Register();
	}

 }
