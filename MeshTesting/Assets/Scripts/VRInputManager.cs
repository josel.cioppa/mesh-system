﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VRInputAxes
{
	public static string LeftTrigger = "Left Trigger";
	public static string RightTrigger = "Right Trigger";
	public static string LeftGrip = "Left Grip";
	public static string RightGrip = "Right Grip";
	public static string RightThumbH = "Right Thumb H";
	public static string RightThumbV = "Right Thumb V";
	public static string LeftThumbH = "Left Thumb H";
	public static string LeftThumbV = "Left Thumb V";
}

public class VRInputManager : MonoBehaviour
{

	private static bool m_isKeyPressed = false;         
	private static Transform m_controllerTransform;     
	private static bool m_controllerActive = true;
	private bool m_leftTriggerDown, m_rightTriggerDown, m_leftGripDown, m_rightGripDown;

	public float inputThreshold = 0.1f;

	public Event
		rtPressed, rtHeld, rtReleased,
		ltPressed, ltHeld, ltReleased,
		lgPressed, lgHeld, lgReleased,
		rgPressed, rgHeld, rgReleased,
		bothGripsPressed, bothGripsHeld;

	public static Vector2 leftThumbstick, rightThumbstick;

	/// <summary>
	/// Here we process all VR button input, and invoke the appropriate events for external listeners
	/// </summary>
	private void Update()
	{

		var rt = Input.GetAxis(VRInputAxes.RightTrigger);
		var lt = Input.GetAxis(VRInputAxes.LeftTrigger);
		var rg = Input.GetAxis(VRInputAxes.RightGrip);
		var lg = Input.GetAxis(VRInputAxes.LeftGrip);

		leftThumbstick = new Vector2(
			Input.GetAxis(VRInputAxes.LeftThumbH),
			Input.GetAxis(VRInputAxes.LeftThumbV)
		);

		rightThumbstick = new Vector2(
			Input.GetAxis(VRInputAxes.RightThumbH),
			Input.GetAxis(VRInputAxes.RightThumbV)
		);

		if (rt > inputThreshold)
		{
			if (!m_rightTriggerDown)
			{
				m_rightTriggerDown = true;
				rtPressed.Invoke();
			} else
			{
				rtHeld.Invoke();	
			}
		} else
		{
			if (m_rightTriggerDown)
			{
				m_rightTriggerDown = false;
				rtReleased.Invoke();
			}

		}

		if (lt > inputThreshold)
		{ 
			if (!m_leftTriggerDown)
			{
				m_leftTriggerDown = true;
				ltPressed.Invoke();
			}
			else
			{
				ltHeld.Invoke();
			}
		} else
		{
			if (m_leftTriggerDown)
			{
				ltReleased.Invoke();
				m_leftTriggerDown = false;
			}

		}

		// holding both grips down at the same time
		if (m_leftGripDown && m_rightGripDown)
		{
			bothGripsHeld.Invoke();
		}

		// right grip button is down
		if (rg > inputThreshold)
		{
			// pressed, not held
			if (!m_rightGripDown)
			{
				m_rightGripDown = true;
				rgPressed.Invoke();

				// left grip already down, trigger both grips pressed
				if (m_leftGripDown)
				{
					bothGripsPressed.Invoke();
				}
			}
			else
			{
				rgHeld.Invoke();
			}
		} else
		{
			if (m_rightGripDown)
			{
				rgReleased.Invoke();
				m_rightGripDown = false;
			}
		}

		// left grip input is on
		if (lg > inputThreshold)
		{
			// pressing left grip
			if (!m_leftGripDown)
			{

				lgPressed.Invoke();
				m_leftGripDown = true;
				
				// right grip already down, trigger both grips pressed
				if (m_rightGripDown)
				{
					bothGripsPressed.Invoke();
				}


			}
			// holding left grip
			else
			{
				lgHeld.Invoke();
			}
		} else
		{
			if (m_leftGripDown)
			{
				lgReleased.Invoke();
				m_leftGripDown = false;
			}
		}

		

	}

	public static void SetIsControllerButtonPressed(bool isPressed)
	{
		m_isKeyPressed = isPressed;
	}

	public static bool GetIsControllerButtonPressed()
	{
		return m_isKeyPressed;
	}

	public static void SetControllerTransform(Transform transform)
	{
		m_controllerTransform = transform;
	}

	public static Transform GetControllerTransform()
	{
		return m_controllerTransform;
	}

	public static void SetControllerActive(bool active)
	{
		m_controllerActive = active;
	}

	public static bool GetControllerActive()
	{
		return m_controllerActive;
	}
	
}