﻿using UnityEngine;

public class CollisionDetector : MonoBehaviour {

	public ActiveGameObject activeObject;

	public void OnTriggerEnter(Collider other)
	{

		var meshEditor = other.gameObject.GetComponent<JMeshOneRingMover>();

		if (meshEditor == null)
		{
			return;
		}

        activeObject.SetActiveObject(other.gameObject);

	}

}
