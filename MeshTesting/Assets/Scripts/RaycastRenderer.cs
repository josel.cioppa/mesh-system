﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastRenderer : MonoBehaviour {

	public Camera camera;
	public LineRenderer lineRenderer;
	public float length = 5.0f;
	public Color color = Color.red;

	public UIHitData hitData;

	
	public void OnUIHit () {
		lineRenderer.SetPosition(0, hitData.raycastOrigin);
		lineRenderer.SetPosition(1, hitData.hitPosition);
        lineRenderer.SetColors(color, color);
		lineRenderer.SetWidth(0.01f, 0.01f);

	}
}
