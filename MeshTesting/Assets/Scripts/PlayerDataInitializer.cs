﻿using UnityEngine;

public class PlayerDataInitializer : MonoBehaviour {

    public Transform head, rh, lh;
    public PlayerData playerData;

    public void Awake()
    {
        playerData.head = head;
        playerData.rightHand = rh;
        playerData.leftHand = lh;
    }

}
