﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventListener : MonoBehaviour {

	public Event m_event;
	public UnityEvent m_unityEvent;

	private void OnEnable()
	{
		m_event.AddListener(this);
	}

	private void OnDisable()
	{
		m_event.RemoveListener(this);
	}

	public void Invoke()
	{
		m_unityEvent.Invoke();
	}

}
