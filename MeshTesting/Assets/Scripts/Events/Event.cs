﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Event : ScriptableObject {

	public List<EventListener> listeners;

	public void Invoke()
	{
		foreach(var l in listeners)
		{
			l.Invoke();
		}
	}

	internal void AddListener(EventListener eventListener)
	{
		listeners.Add(eventListener);
	}

	internal void RemoveListener(EventListener eventListener)
	{
		listeners.Remove(eventListener);
	}
}
