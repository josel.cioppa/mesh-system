﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGrabber : MonoBehaviour {

	public ActiveGameObject target;

	public void OnGrab()
	{
		if (target.activeObject == null)
			return;


		target.activeObject.transform.SetParent(this.transform);
	}

	public void OnRelease()
	{
		if (target.activeObject == null)
			return;


		target.activeObject.transform.SetParent(null);
	}

}
