﻿using UnityEngine;

public class JMeshRenderer : MonoBehaviour {

    public Material triMat;
	public Material wireMat;
	public Material pointMat;
	public bool m_renderTriangles, m_renderWireframe, m_renderPoints;
	public JMeshFilter meshFilter;

	private void RenderMesh(Material mat)
	{
		mat.SetPass(0);
		GL.PushMatrix();
		GL.MultMatrix(transform.localToWorldMatrix);
		meshFilter.SetShaderVariables(mat);
		int trisToRender = meshFilter.mesh.MaxTriangleID + 1;
		Graphics.DrawProcedural(MeshTopology.Points, trisToRender);
		GL.PopMatrix();
	}

    private void OnRenderObject()
    {

		if (m_renderTriangles)
		{
			RenderMesh(triMat);
		}

		if (m_renderWireframe) {
			RenderMesh(wireMat);
		}

		if (m_renderPoints)
		{
			RenderMesh(pointMat);
		}

	}

}

