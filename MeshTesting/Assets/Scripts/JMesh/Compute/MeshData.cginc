#ifndef MESH_DATA_CG
#define MESH_DATA_CG


/* a vertex currently holds 9 floats => 36 bytes per vertex
   in a mesh with 500,000 vertices, this amounts to about 18 megs of vertex data
*/
struct Vertex { 
	float3 position;
	float3 normal;
	float3 color;	
	int flags;
};

struct VertexUpdate { 
	float3 position;
	float3 normal;
	float3 color;
	int index;
};

/* a triangle contains 16 bytes of data. With 500,000 triangles, thats about 8 megs of triangle data */
struct Triangle {
	int a,b,c;
	int flags;
};

struct TriangleUpdate { 
	int a,b,c;
	int index;
};


/* a triangle contains 16 bytes of data. With 500,000 triangles, thats about 8 megs of triangle data */
struct Quad {
	int a,b,c,d;
	int flags;
};

struct QuadUpdate { 
	int a,b,c,d;
	int index;
};


/* triangle/vertex flags*/
int ACTIVE_FLAG =   0x00000001;
int SELECTED_FLAG = 0x00000002;

bool IsActive(int flag) { 
	return (flag & ACTIVE_FLAG) > 0;
}

bool IsSelected(int flag) { 
	return (flag & SELECTED_FLAG) > 0;
}

#endif
