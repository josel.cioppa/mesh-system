﻿using UnityEngine;
using System.Collections.Generic;

public struct TriangleUpdate
{
	public int a, b, c;
	public int index;
}

public struct VertexUpdate
{
	public Vector3 position;
	public Vector3 normal;
	public Vector3 color;
	public int vID;


#if G3_USING_UNITY
	public static VertexUpdate FromVertexInfo(int vertID, g3.NewVertexInfo info)
	{
		return new VertexUpdate()
		{
			position = (Vector3)info.v,
			normal = info.n,
			color = info.c,
			vID = vertID
		};
	}
#endif
}

public struct Vertex
{
	public Vector3 position;
	public Vector3 normal;
	public Vector3 color;
	public int flags;
}

public struct Triangle
{
	public int a;
	public int b;
	public int c;
	public int flags;
}

public struct TriangleFlags
{
	public static int ACTIVE_FLAG =   0x00000001;
	public static int SELECTED_FLAG = 0x00000002;
}


public class GPUMesh
{
    public static int FLOATS_PER_VERTEX = 9;
    public static int INTS_PER_TRIANGLE = 4;

    public int maxVertices = 500000;
    public int maxTriangles = 500000;

    public int numTriangles;
    public int numVertices;


	public int maxVertIndex = 0;
	public int maxTriIndex = 0;

    public int TriangleCount
    {
        get
        {
            return numTriangles;
        }
    }

    public ComputeBuffer triangleData;
    public ComputeBuffer vertexData;

    public ComputeShader updateMeshShader;

	public static int MAX_NEW_TRI_APPEND = 100;
	ComputeBuffer triangleUpdateBuffer = null;

	static int MAX_VERT_UPDATES = 1000;
	ComputeBuffer vertUpdateBuffer = null;

	Dictionary<int, bool> kernelsInitialize = new Dictionary<int, bool>();


	public GPUMesh()
    {
        vertexData = new ComputeBuffer(maxVertices, FLOATS_PER_VERTEX * sizeof(float) + sizeof(int));
        triangleData = new ComputeBuffer(maxTriangles, INTS_PER_TRIANGLE * sizeof(int));
    }

	public GPUMesh(Mesh uMesh)
	{
		var triangles = uMesh.triangles;
		var normals = uMesh.normals;
		var verts = uMesh.vertices;

		numVertices = verts.Length;
		numTriangles = triangles.Length / 3;

		Vertex[] vertices = new Vertex[numVertices];
		Triangle[] triangleDatum = new Triangle[numTriangles];

		for (int i = 0; i < numVertices; i++)
		{

			var n = normals[i];
			var v = verts[i];

			vertices[i] = new Vertex()
			{
				position = v,
				normal = n,
				color = new Vector3(1.0f, 1.0f, 1.0f),
				flags = 0x00000001
			};

		}

		for (int i = 0; i < numTriangles; i++)
		{
			triangleDatum[i] = new Triangle()
			{
				a = triangles[0 + 3 * i],
				b = triangles[1 + 3 * i],
				c = triangles[2 + 3 * i],
				flags = TriangleFlags.ACTIVE_FLAG
			};
		}

		SetMeshData(vertices, triangleDatum);

	}


#if G3_USING_UNITY
	public GPUMesh(g3.DMesh3 dmesh)
    {

        var triangles = dmesh.TrianglesBuffer;
        Vertex[] vertices = new Vertex[dmesh.VertexCount];
        Triangle[] triangleDatum = new Triangle[triangles.Length / 3];

        numVertices = vertices.Length;
        numTriangles = triangles.Length / 3;
		
        for (int i = 0; i < numVertices; i++)
        {
            vertices[i] = new Vertex()
            {
                position = new Vector3((float)dmesh.vertices[3 * i], (float)dmesh.vertices[3 * i + 1], (float)dmesh.vertices[3 * i + 2]),
                normal = new Vector3((float)dmesh.normals[3 * i], (float)dmesh.normals[3 * i + 1], (float)dmesh.normals[3 * i + 2]),
                color = new Vector3(1.0f, 1.0f, 1.0f),
				flags = 0x00000001
            };
        }

        for (int i = 0; i < numTriangles; i++)
        {
            triangleDatum[i] = new Triangle()
            {
                a = triangles[0 + 3 * i],
                b = triangles[1 + 3 * i],
                c = triangles[2 + 3 * i],
                flags = TriangleFlags.ACTIVE_FLAG
            };
        }

        SetMeshData(vertices, triangleDatum);

    }
#endif


	private void SetMeshData(Vertex[] vertices, Triangle[] triangleDatum)
    {

        triangleData = new ComputeBuffer(maxTriangles, sizeof(int) * INTS_PER_TRIANGLE);
        triangleData.SetData(triangleDatum, 0, 0, triangleDatum.Length);

        vertexData = new ComputeBuffer(maxVertices, FLOATS_PER_VERTEX * sizeof(float) + sizeof(int));
        vertexData.SetData(vertices, 0, 0, vertices.Length);

		triangleUpdateBuffer = new ComputeBuffer(MAX_NEW_TRI_APPEND, 4 * sizeof(int));
		vertUpdateBuffer = new ComputeBuffer(MAX_VERT_UPDATES, 9 * sizeof(float) + sizeof(int));

	}

	int removeTrianglesKernel, updateTrianglesKernel, updateTriFlagsKernel, updateVerticesKernel;
	int vertexDataPID, triangleDataPID, vertexUpdatePID, triangleUpdatesIDSPID, triangleUpdatesPID, enableFlagPID, flagToUpdatePID;

	private void InitializeComputeShader()
	{

		removeTrianglesKernel = updateMeshShader.FindKernel("RemoveTriangles");
		updateVerticesKernel = updateMeshShader.FindKernel("UpdateVertices");
		updateTrianglesKernel = updateMeshShader.FindKernel("UpdateTriangles");
		updateTriFlagsKernel = updateMeshShader.FindKernel("UpdateTriangleFlags");

		vertexDataPID = Shader.PropertyToID("vertexData");
		triangleDataPID = Shader.PropertyToID("triangleData");
		vertexUpdatePID = Shader.PropertyToID("vertexUpdates");
		triangleUpdatesIDSPID = Shader.PropertyToID("triangleUpdateIDS");
		triangleUpdatesPID = Shader.PropertyToID("triangleUpdates");
		enableFlagPID = Shader.PropertyToID("enableFlag");
		flagToUpdatePID = Shader.PropertyToID("flagToUpdate");

		updateMeshShader.SetBuffer(removeTrianglesKernel, vertexDataPID, vertexData);
		updateMeshShader.SetBuffer(updateVerticesKernel, vertexDataPID, vertexData);
		updateMeshShader.SetBuffer(updateTrianglesKernel, vertexDataPID, vertexData);
		updateMeshShader.SetBuffer(updateTriFlagsKernel, vertexDataPID, vertexData);

		updateMeshShader.SetBuffer(removeTrianglesKernel, triangleDataPID, triangleData);
		updateMeshShader.SetBuffer(updateVerticesKernel, triangleDataPID, triangleData);
		updateMeshShader.SetBuffer(updateTrianglesKernel, triangleDataPID, triangleData);
		updateMeshShader.SetBuffer(updateTriFlagsKernel, triangleDataPID, triangleData);

	}

	/// <summary>
	/// This takes a list of vertex updates, and updates the GPU vertex data. Assumes these are NOT new vertices, so doesn't increase number of vertices in the list
	/// </summary>
	/// <param name="newVertices"></param>
	internal void PushVertexUpdates(VertexUpdate[] newVertices)
	{

		if (newVertices.Length >= MAX_VERT_UPDATES)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateVertices");
	
		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "vertexData", vertexData);
			updateMeshShader.SetBuffer(kernel, "vertexUpdates", vertUpdateBuffer);
			kernelsInitialize.Add(kernel, true);
		}

		vertUpdateBuffer.SetData(newVertices, 0, 0, newVertices.Length);			
		updateMeshShader.Dispatch(kernel, newVertices.Length, 1, 1);

	}


	/// <summary>
	/// updates vertex data on GPU and increases number of vertex count
	/// </summary>
	/// <param name="newVertices"></param>
	internal void PushNewVertices(VertexUpdate[] newVertices)
	{
		PushVertexUpdates(newVertices);
		numVertices += newVertices.Length;
	}


	ComputeBuffer triangleUpdateIDS = null;

	internal void HighlightTriangles(int[] tids)
	{

		if (tids.Length >= MAX_NEW_TRI_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateTriangleFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			triangleUpdateIDS = new ComputeBuffer(MAX_NEW_TRI_APPEND, sizeof(int));
			updateMeshShader.SetBuffer(kernel, "triangleUpdateIDS", triangleUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "triangleData", triangleData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", true);
		updateMeshShader.SetInt("flagToUpdate", 0x00000002);
		triangleUpdateIDS.SetData(tids, 0, 0, tids.Length);
		updateMeshShader.Dispatch(kernel, tids.Length, 1, 1);

	}

	internal void ClearTriangleHighlight(int[] tids)
	{
		if (tids.Length >= MAX_NEW_TRI_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateTriangleFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			triangleUpdateIDS = new ComputeBuffer(MAX_NEW_TRI_APPEND, sizeof(int));
			updateMeshShader.SetBuffer(kernel, "triangleUpdateIDS", triangleUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "triangleData", triangleData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", false);
		updateMeshShader.SetInt("flagToUpdate", 0x00000002);
		triangleUpdateIDS.SetData(tids, 0, 0, tids.Length);
		updateMeshShader.Dispatch(kernel, tids.Length, 1, 1);
	}

	/// <summary>
	/// Updates triangles on GPU. Does NOT increase number of triangles, assumes we're merely updating an active triangle
	/// </summary>
	/// <param name="newTriangles"></param>
	internal void PushTriangleUpdates(TriangleUpdate[] newTriangles)
	{

		if (newTriangles.Length >= MAX_NEW_TRI_APPEND)
		{
			Debug.LogError("Exceeded max triangle append count");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateTriangles");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "triangleUpdates", triangleUpdateBuffer);
			updateMeshShader.SetBuffer(kernel, "triangleData", triangleData);
			kernelsInitialize.Add(kernel, true);
		}

		triangleUpdateBuffer.SetData(newTriangles, 0, 0, newTriangles.Length);
		updateMeshShader.Dispatch(kernel, newTriangles.Length, 1, 1);

	}


	/// <summary>
	/// Updates triangle data, and increases number of triangles counter
	/// </summary>
	/// <param name="newTriangles"></param>
	internal void PushNewTriangles(TriangleUpdate[] newTriangles)
	{
		PushTriangleUpdates(newTriangles);
		numTriangles += newTriangles.Length;
	}


	internal void PushRemoveTriangles(TriangleUpdate[] trisToRemove)
	{
		if (trisToRemove.Length >= MAX_NEW_TRI_APPEND)
		{
			Debug.LogError("Exceeded max triangle append count");
			return;
		}

		int kernel = updateMeshShader.FindKernel("RemoveTriangles");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "triangleUpdates", triangleUpdateBuffer);
			updateMeshShader.SetBuffer(kernel, "triangleData", triangleData);
			kernelsInitialize.Add(kernel, true);
		}

		triangleUpdateBuffer.SetData(trisToRemove, 0, 0, trisToRemove.Length);
		updateMeshShader.Dispatch(kernel, trisToRemove.Length, 1, 1);
	}


	public void Destroy()
    {

		// free buffers holding mesh data
        if (triangleData != null) triangleData.Release();
        if (vertexData != null) vertexData.Release();

		// free buffers used to push updates to the GPU
		if (triangleUpdateBuffer != null) triangleUpdateBuffer.Release();
		if (vertUpdateBuffer != null) vertUpdateBuffer.Release();

	}

}