﻿Shader "Unlit/QuadWireframeShader"
{
	Properties
{

    _WireColor ("Wireframe color", Color) = (1, .00, .00, 1) // color
	_ScaleX ("Scale X", Range(0.0, 0.01)) = 0.001
	_ScaleY ("Scale Y", Range(0.0, 0.01)) = 0.001
	_WireOffset ("Wire Offset", Range(0.0, 0.1)) = 0.001
	_SelectionColor("Selection Color", Color) = (0.2, 0.2, 0.2)
	_StandardColor("Standard Color", Color) = (0.8, 0.2, 0.2)

}

	SubShader {

		Pass{
		
			ZWrite On
			Cull Off

			CGPROGRAM

			#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#include "Assets/Scripts/JMesh/Compute/MeshData.cginc"

		
			fixed4 _WireColor, _SelectionColor, _StandardColor;
			float _ScaleX;
			float _ScaleY;
			float _WireOffset;

			uniform StructuredBuffer<Quad> quadData;
			uniform StructuredBuffer<Vertex> vertexData;

			// NOTE: do we need this? can we just use the Triangle struct?
			struct geometryInput
			{
				int4 quad : TEXCOORD0;
				int flags : TEXCOORD1;
			};

			struct fragmentInput
			{
				float4 clipPos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 color : TEXCOORD1;
			};

			struct fragOutput
			{
				fixed4 color : SV_Target;
			};

			geometryInput vert(uint id : SV_VertexID)
			{
				
				geometryInput output;
				
				output.quad = int4(
					quadData[id].a,
					quadData[id].b,
					quadData[id].c,
					quadData[id].d
				);

				output.flags = quadData[id].flags;

				return output;

			}

	
			[maxvertexcount(6)]
			void geom(point geometryInput p[1], inout LineStream<fragmentInput> triStream)
			{
						
				int4 tri =  p[0].quad;
				
				fragmentInput pIn;

				bool triangleActive = ((p[0].flags & 0x00000001) > 0);

				if (!triangleActive) { 
					return;
				}

				bool triangleSelected = (p[0].flags & 0x00000002) > 0;
				float3 selectedColor = _SelectionColor;

				if (triangleSelected) { 	
					selectedColor *=  clamp(0.5f * (1.0f + sin(_Time.x * 60.0f)), 0.3f, 0.7f);
				}

				float3 triColor = _WireColor.xyz;
				
				// render triangle
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.y].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.y].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);

				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.z].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);

					// render triangle
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);

				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.w].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.w].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);

				
				/*

				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.w].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.w].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor;
				triStream.Append(pIn);

			*/


			}

			fragOutput frag(fragmentInput input)
			{	
				fragOutput o;
				o.color = fixed4(input.color,1);
				return o;
			}

			ENDCG
		
		}
	}
}

