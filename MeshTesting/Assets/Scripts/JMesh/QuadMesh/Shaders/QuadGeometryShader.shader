﻿Shader "Unlit/QuadGeometryShader"
{
	Properties
    {
	    u_renderWireFrame ("Render Wireframe", Range(0,1)) = 1
        u_wireColour ("Wire Colour", Color) = (0.83, 0.93, 0.92, 0.80)
        u_polyColour ("Poly Colour", Color) = (0.60, 0.71, 1.00, 0.85)
        u_antiAliasing ("Anti Aliasing", Range(0, 1)) = 1
        u_wireWidth ("Wire Width", Range(0, 1)) = 0.6
        u_fresnelAperture ("Fresnel Aperture", Range(0.0001, 1)) = 0.7
        u_fresnelStrength ("Fresnel Strength", Range(0, 1)) = 0.75
        u_brighten ("Brighten", Range(0, 1)) = 0.4
        u_roughness ("Roughness", Range(0.0001, 1)) = 0.1
        u_metallic ("Metallic", Range(0, 1)) = 0.4
        u_backOpacity ("Back Opacity", Range(0, 1)) = 0.25
        u_selectionColor ("Selection Color", Color) = (0.5, 0.5, 0.5, 1.0)
		u_standardColor ("Standard Color", Color) = (0.5, 0.2, 0.6, 1.0)
    }

	SubShader {


		Pass{
	
			ZWrite On
			Cull Off

			CGPROGRAM

			#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			
			#include "UnityLightingCommon.cginc"
			#include "UnityCG.cginc"
			#include "Assets/Scripts/JMesh/Compute/MeshData.cginc"
						uniform int u_renderWireFrame;

			uniform half4 u_wireColour;
			uniform half4 u_polyColour;
			uniform float u_antiAliasing;
			uniform float u_wireWidth;
			uniform float u_fresnelAperture;
			uniform float u_fresnelStrength;
			uniform float u_brighten;
			uniform float u_roughness;
			uniform float u_metallic;
			uniform float u_backOpacity;

			uniform float4 u_selectionColor;
			uniform float4 u_standardColor;


			uniform StructuredBuffer<Quad> quadData;
			uniform StructuredBuffer<Vertex> vertexData;

			// NOTE: do we need this? can we just use the Triangle struct?
			struct geometryInput
			{
				int4 quad : TEXCOORD0;
				int flags : TEXCOORD1;
			};

			struct fragmentInput
			{
				float4 clipPos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 color : TEXCOORD1;
				float2 triangleVertexID : TEXCOORD2;
			};

			struct fragOutput
			{
				fixed4 color : SV_Target;
			};

			geometryInput vert(uint id : SV_VertexID)
			{
				
				geometryInput output;
				
				output.quad = int4(
					quadData[id].a,
					quadData[id].b,
					quadData[id].c,
					quadData[id].d
				);

				output.flags = quadData[id].flags;

				return output;

			}

	
			[maxvertexcount(6)]
			void geom(point geometryInput p[1], inout TriangleStream<fragmentInput> triStream)
			{
						
				float3 lightDir = _WorldSpaceLightPos0.xyz;

				int4 tri =  p[0].quad;
				
				fragmentInput pIn;

				bool triangleActive = ((p[0].flags & 0x00000001) > 0);

				if (!triangleActive) { 
					return;
				}

				bool triangleSelected = (p[0].flags & 0x00000002) > 0;
				float3 selectedColor = u_selectionColor.rgb;

				if (triangleSelected) { 	
					selectedColor *=  clamp(0.5f * (1.0f + sin(_Time.x * 60.0f)), 0.3f, 0.7f);
				}

				bool boundaryQuad =  (p[0].flags & 0x00000004) > 0;
			
				float3 _StandardColor = u_standardColor.rgb;
				float3 triColor =  !triangleSelected ? _StandardColor : selectedColor;
				
				triColor = boundaryQuad ? float3(1, 0, 1) : triColor;

				// render triangle
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor ;
				pIn.triangleVertexID = float2(0,0);
				triStream.Append(pIn);
				
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.y].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.y].normal, 0.0f)).xyz;
				pIn.color = triColor;
				pIn.triangleVertexID = float2(1,0);
				triStream.Append(pIn);
	
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.z].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color = triColor;
				pIn.triangleVertexID = float2(0,1);
				triStream.Append(pIn);

				triStream.RestartStrip();

				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.x].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.x].normal, 0.0f)).xyz;
				pIn.color = triColor;
				pIn.triangleVertexID = float2(0,0);

				triStream.Append(pIn);
	
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.z].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.z].normal, 0.0f)).xyz;
				pIn.color = triColor;
				pIn.triangleVertexID = float2(1,0);

				triStream.Append(pIn);
	
				pIn.clipPos = UnityObjectToClipPos(float4(vertexData[tri.w].position, 1.0f));
				pIn.normal =  mul(UNITY_MATRIX_M, float4(vertexData[tri.w].normal, 0.0f)).xyz;
				pIn.color = triColor;
				pIn.triangleVertexID = float2(0,1);

				triStream.Append(pIn);
		
				triStream.RestartStrip();


			}

		


			fragOutput frag(fragmentInput a_fragIn)
			{	


				fragOutput o;
				
				float4 colour = float4(a_fragIn.color, 1.0f);

				if (u_renderWireFrame  == 1) { 
					float3 vertexProximities = float3(a_fragIn.triangleVertexID.x, a_fragIn.triangleVertexID.y, 1 - a_fragIn.triangleVertexID.x - a_fragIn.triangleVertexID.y);
					float3 vertexProximitiesFixedWidth = fwidth(vertexProximities);
					float3 antiAliasingWeight = vertexProximitiesFixedWidth * u_antiAliasing;
					float3 wireWidthWeight = vertexProximitiesFixedWidth * u_wireWidth;
					vertexProximities = smoothstep(wireWidthWeight, wireWidthWeight + antiAliasingWeight, vertexProximities);
					float minVertexProximity = min(min(vertexProximities.x, vertexProximities.y), vertexProximities.z);
					colour = lerp(u_wireColour, colour,  minVertexProximity);
					colour.a *= u_backOpacity;
				}


				o.color = colour;

				return o;

			}

			ENDCG
		
		}
	}
}

