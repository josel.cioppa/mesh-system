﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace g3
{
	public class DQuadMesh 
	{

		public const int InvalidID = -1;
		public const int NonManifoldID = -2;

		public static readonly Vector3d InvalidVertex = new Vector3d(Double.MaxValue, 0, 0);
		public static readonly Index4i InvalidQuad = new Index4i(InvalidID, InvalidID, InvalidID, InvalidID);
		public static readonly Index2i InvalidEdge = new Index2i(InvalidID, InvalidID);


		// stores the (x,y,z) of each vertex 
		public DVector<double> vertices;
		public DVector<float> normals;
		public DVector<float> colors;
		public DVector<float> uv;
		RefCountVector vertices_refcount;

		// [TODO] this is optional if we only want to use this class as an iterable mesh-with-nbrs
		//   make it optional with a flag? (however find_edge depends on it...)
		SmallListSet vertex_edges;


		// stores the (i,j,k,l) indices of the vertices of each quad. If you have a quad ID qID := Int, then you get the vertex indices of the quad
		// quads[4 * qID + i] where i = 0, 1, 2, 3
		public DVector<int> quads;


		/* for each quad q = (i,j,k,l) stores pointers to the edges into the edge list. So if we have something like:
		 * 
		 *  verts v0, v1, v2, v3
		 *  fID quad(v0, v1, v2, v3)
		 *  edges e0 = edge(v0, v1), e1 = edge(v1, v2), ...
		 *  quad_edges[fID] = (e0, e1, e2, e3)
		 * 
		 */
		public DVector<int> quad_edges;
		public RefCountVector quad_refCount;
		public DVector<int> quad_groups;


		// each edge stores (vid0, vid1, qID0, qID1) where vid are the start/end vertex ids, and qids are the ids into the quad list of the faces 
		// to the left and right of the edge; 
		// [NOTE] qID0 is never invalid (it points to its triangle) whereas qID1 can be invalidID (if its a boundary edge)
		DVector<int> edges;
		RefCountVector edges_refcount;

		int max_group_id = 0;

		public DQuadMesh(bool bWantNormals = true, bool bWantColors = false, bool bWantUVs = false, bool bWantQuadGroups = false)
		{

			vertices = new DVector<double>();

			if (bWantNormals)
				normals = new DVector<float>();

			if (bWantColors)
				colors = new DVector<float>();

			if (bWantUVs)
				uv = new DVector<float>();

			vertex_edges = new SmallListSet();

			vertices_refcount = new RefCountVector();

			quads = new DVector<int>();
			quad_edges = new DVector<int>();
			quad_refCount = new RefCountVector();

			if (bWantQuadGroups)
				quad_groups = new DVector<int>();

			max_group_id = 0;

			edges = new DVector<int>();
			edges_refcount = new RefCountVector();

		}

		#region vector conversion statics
		// stupid per-type conversion functions because fucking C# 
		// can't do typecasts in generic functions
		public static Vector3[] dvector_to_vector3(DVector<double> vec)
		{


			int nLen = vec.Length / 3;
			Vector3[] result = new Vector3[nLen];
			for (int i = 0; i < nLen; ++i)
			{
				result[i].x = (float)vec[3 * i];
				result[i].y = (float)vec[3 * i + 1];
				result[i].z = (float)vec[3 * i + 2];
			}
			return result;
		}
		public static Vector3[] dvector_to_vector3(DVector<float> vec)
		{
			int nLen = vec.Length / 3;
			Vector3[] result = new Vector3[nLen];
			for (int i = 0; i < nLen; ++i)
			{
				result[i].x = vec[3 * i];
				result[i].y = vec[3 * i + 1];
				result[i].z = vec[3 * i + 2];
			}
			return result;
		}
		public static Vector2[] dvector_to_vector2(DVector<float> vec)
		{
			int nLen = vec.Length / 2;
			Vector2[] result = new Vector2[nLen];
			for (int i = 0; i < nLen; ++i)
			{
				result[i].x = vec[2 * i];
				result[i].y = vec[2 * i + 1];
			}
			return result;
		}
		public static Color[] dvector_to_color(DVector<float> vec)
		{
			int nLen = vec.Length / 3;
			Color[] result = new Color[nLen];
			for (int i = 0; i < nLen; ++i)
			{
				result[i].r = vec[3 * i];
				result[i].g = vec[3 * i + 1];
				result[i].b = vec[3 * i + 2];
			}
			return result;
		}
		public static int[] dvector_to_int(DVector<int> vec)
		{
			// todo this could be faster because we can directly copy chunks...
			int nLen = vec.Length;
			int[] result = new int[nLen];
			for (int i = 0; i < nLen; ++i)
				result[i] = vec[i];
			return result;
		}
		#endregion

		#region properties

		public int VertexCount
		{
			get { return vertices_refcount.count; }
		}
		public int QuadCount
		{
			get { return quad_refCount.count; }
		}
		public int EdgeCount
		{
			get { return edges_refcount.count; }
		}

		// these values are (max_used+1), ie so an iteration should be < MaxTriangleID, not <=
		public int MaxVertexID
		{
			get { return vertices_refcount.max_index; }
		}
		public int MaxQuadID
		{
			get { return quad_refCount.max_index; }
		}
		public int MaxEdgeID
		{
			get { return edges_refcount.max_index; }
		}
		public int MaxGroupID
		{
			get { return max_group_id; }
		}

		public bool HasVertexColors { get { return colors != null; } }
		public bool HasVertexNormals { get { return normals != null; } }
		public bool HasVertexUVs { get { return uv != null; } }
		public bool HasQuadGroups { get { return quad_groups != null; } }


		public MeshComponents Components
		{
			get
			{
				MeshComponents c = 0;
				if (normals != null) c |= MeshComponents.VertexNormals;
				if (colors != null) c |= MeshComponents.VertexColors;
				if (uv != null) c |= MeshComponents.VertexUVs;
				if (quad_groups != null) c |= MeshComponents.FaceGroups;
				return c;
			}
		}

		#endregion

		#region topology validation
		public bool IsVertex(int vID)
		{
			return vertices_refcount.isValid(vID);
		}

		public bool IsQuad(int tID)
		{
			return quad_refCount.isValid(tID);
		}

		public bool IsEdge(int eID)
		{
			return edges_refcount.isValid(eID);
		}
		#endregion

		#region vertex getters/setters

		public Vector3 GetVertexAsVector3(int vID)
		{
			int i = 3 * vID;
			return new Vector3((float)vertices[i], (float)vertices[i + 1], (float)vertices[i + 2]);
		}
		public void SetVertexFromVector3(int vID, Vector3 vNewPos)
		{
			int i = 3 * vID;
			vertices[i] = vNewPos.x; vertices[i + 1] = vNewPos.y; vertices[i + 2] = vNewPos.z;
		}

		public Vector3d GetVertex(int vID)
		{
			int i = 3 * vID;
			return new Vector3d(vertices[i], vertices[i + 1], vertices[i + 2]);
		}

		public Vector3f GetVertexf(int vID)
		{
			int i = 3 * vID;
			return new Vector3f((float)vertices[i], (float)vertices[i + 1], (float)vertices[i + 2]);
		}

		public void SetVertex(int vID, Vector3d vNewPos)
		{
			int i = 3 * vID;
			vertices[i] = vNewPos.x; vertices[i + 1] = vNewPos.y; vertices[i + 2] = vNewPos.z;
		}


		public Vector3f GetVertexNormal(int vID)
		{
			if (normals == null)
			{
				return Vector3f.AxisY;
			}
			else
			{
				int i = 3 * vID;
				return new Vector3f(normals[i], normals[i + 1], normals[i + 2]);
			}
		}

		public void SetVertexNormal(int vID, Vector3f vNewNormal)
		{
			if (HasVertexNormals)
			{
				int i = 3 * vID;
				normals[i] = vNewNormal.x; normals[i + 1] = vNewNormal.y; normals[i + 2] = vNewNormal.z;
			}
		}

		public Vector3f GetVertexColor(int vID)
		{
			if (colors == null)
			{
				return Vector3f.One;
			}
			else
			{
				int i = 3 * vID;
				return new Vector3f(colors[i], colors[i + 1], colors[i + 2]);
			}
		}

		public void SetVertexColor(int vID, Vector3f vNewColor)
		{
			if (HasVertexColors)
			{
				int i = 3 * vID;
				colors[i] = vNewColor.x; colors[i + 1] = vNewColor.y; colors[i + 2] = vNewColor.z;
			}
		}

		public Vector2f GetVertexUV(int vID)
		{
			if (uv == null)
			{
				return Vector2f.Zero;
			}
			else
			{
				int i = 2 * vID;
				return new Vector2f(uv[i], uv[i + 1]);
			}
		}

		public void SetVertexUV(int vID, Vector2f vNewUV)
		{
			if (HasVertexUVs)
			{

				int i = 2 * vID;
				uv[i] = vNewUV.x; uv[i + 1] = vNewUV.y;
			}
		}

		public bool GetVertex(int vID, ref NewVertexInfo vinfo, bool bWantNormals, bool bWantColors, bool bWantUVs)
		{
			if (vertices_refcount.isValid(vID) == false)
				return false;
			vinfo.v.Set(vertices[3 * vID], vertices[3 * vID + 1], vertices[3 * vID + 2]);
			vinfo.bHaveN = vinfo.bHaveUV = vinfo.bHaveC = false;
			if (HasVertexNormals && bWantNormals)
			{
				vinfo.bHaveN = true;
				vinfo.n.Set(normals[3 * vID], normals[3 * vID + 1], normals[3 * vID + 2]);
			}
			if (HasVertexColors && bWantColors)
			{
				vinfo.bHaveC = true;
				vinfo.c.Set(colors[3 * vID], colors[3 * vID + 1], colors[3 * vID + 2]);
			}
			if (HasVertexUVs && bWantUVs)
			{
				vinfo.bHaveUV = true;
				vinfo.uv.Set(uv[2 * vID], uv[2 * vID + 1]);
			}
			return true;
		}



		public int GetVtxEdgeCount(int vID)
		{
			return vertices_refcount.isValid(vID) ? vertex_edges.Count(vID) : -1;
		}

		public int GetMaxVtxEdgeCount()
		{
			int max = 0;
			foreach (int vid in vertices_refcount)
				max = Math.Max(max, vertex_edges.Count(vid));
			return max;
		}

		public NewVertexInfo GetVertexAll(int i)
		{
			NewVertexInfo vi = new NewVertexInfo();
			vi.v = GetVertex(i);
			if (HasVertexNormals)
			{
				vi.bHaveN = true;
				vi.n = GetVertexNormal(i);
			}
			else
				vi.bHaveN = false;
			if (HasVertexColors)
			{
				vi.bHaveC = true;
				vi.c = GetVertexColor(i);
			}
			else
				vi.bHaveC = false;
			if (HasVertexUVs)
			{
				vi.bHaveUV = true;
				vi.uv = GetVertexUV(i);
			}
			else
				vi.bHaveUV = false;
			return vi;
		}

		#endregion

		#region quad getters/setters

		public Index4i GetTriangle(int tID)
		{
			int i = 4 * tID;
			return new Index4i(quads[i], quads[i + 1], quads[i + 2], quads[i+3]);
		}

		public Index4i GetQuadEdges(int tID)
		{
			int i = 4 * tID;
			return new Index4i(quad_edges[i], quad_edges[i + 1], quad_edges[i + 2], quad_edges[i+3]);
		}

		public int GetQuadEdge(int tid, int j)
		{
			return quad_edges[4 * tid + j];
		}

		public Index4i GetQuadNeighbourQuads(int tID)
		{
			if (quad_refCount.isValid(tID))
			{
				int tei = 4 * tID;
				Index4i nbr_t = Index4i.Zero;

				for (int j = 0; j < 4; ++j)
				{
					int ei = 4 * quad_edges[tei + j];
					nbr_t[j] = (edges[ei + 2] == tID) ? edges[ei + 3] : edges[ei + 2];
				}
				return nbr_t;
			}
			else
				return InvalidQuad;
		}

		public IEnumerable<int> QuadQuadItr(int tID)
		{
			if (quad_refCount.isValid(tID))
			{
				int tei = 4 * tID;

				for (int j = 0; j < 4; ++j)
				{
					int ei = 4 * quad_edges[tei + j];
					int nbr_t = (edges[ei + 2] == tID) ? edges[ei + 3] : edges[ei + 2];
					if (nbr_t != DMesh3.InvalidID)
						yield return nbr_t;
				}
			}
			
		}



		public int GetQuadGroup(int tID)
		{
			return (quad_groups == null) ? -1
				: (quad_refCount.isValid(tID) ? quad_groups[tID] : 0);
		}


		public void SetQuadGroup(int tid, int group_id)
		{
			if (quad_groups != null)
			{
				quad_groups[tid] = group_id;
				max_group_id = Math.Max(max_group_id, group_id + 1);
			}
		}

		public int AllocateQuadGroup()
		{
			return ++max_group_id;
		}


		#endregion

		int EdgeStartVert(int eid)
		{
			return edges[4 * eid];
		}

		int EdgeEndVert(int eid)
		{
			return edges[4 * eid + 1];
		}

		int EdgePrimaryFace(int eid)
		{
			return edges[4 * eid + 2];
		}

		int EdgeSecondaryFace(int eid)
		{
			return edges[4 * eid + 3];
		}

		public void GetQuadVertices(int tID, ref Vector3d v0, ref Vector3d v1, ref Vector3d v2, ref Vector3d v3)
		{

			int ai = 3 * quads[4 * tID];
			v0.x = vertices[ai]; v0.y = vertices[ai + 1]; v0.z = vertices[ai + 2];

			int bi = 3 * quads[4 * tID + 1];
			v1.x = vertices[bi]; v1.y = vertices[bi + 1]; v1.z = vertices[bi + 2];

			int ci = 3 * quads[4 * tID + 2];
			v2.x = vertices[ci]; v2.y = vertices[ci + 1]; v2.z = vertices[ci + 2];

			int di = 3 * quads[4 * tID + 3];
			v3.x = vertices[di]; v3.y = vertices[di + 1]; v3.z = vertices[di + 2];

		}

		public void GetQuadVertexIndices(int tID, ref Index4i verts)
		{
			verts.a = quads[4 * tID + 0];
			verts.b = quads[4 * tID + 1];
			verts.c = quads[4 * tID + 2];
			verts.d	 = quads[4 * tID + 3];
		}

		public void GetQuadEdgeIndices(int fID, ref Index4i eids)
		{
			eids.a = quad_edges[4 * fID + 0];
			eids.b = quad_edges[4 * fID + 1];
			eids.c = quad_edges[4 * fID + 2];
			eids.d = quad_edges[4 * fID + 3];
		}

		public void GetQuadNeigboringFaceIndices(int fID, ref Index4i fids)
		{

			Index4i eids = default(Index4i);
			GetQuadEdgeIndices(fID, ref eids);

			fids.a = EdgeSecondaryFace(eids.a);
			fids.b = EdgeSecondaryFace(eids.b);
			fids.c = EdgeSecondaryFace(eids.c);
			fids.d = EdgeSecondaryFace(eids.d);

		}

		public Vector3d GetQuadVertex(int tid, int j)
		{
			int a = quads[4 * tid + j];
			return new Vector3d(vertices[3 * a], vertices[3 * a + 1], vertices[3 * a + 2]);
		}

		public Vector3d GetQuadNormal(int tID)
		{
			Vector3d v0 = Vector3d.Zero, v1 = Vector3d.Zero, v2 = Vector3d.Zero, v3 = Vector3d.Zero;
			GetQuadVertices(tID, ref v0, ref v1, ref v2, ref v3);
			return MathUtil.Normal(ref v0, ref v1, ref v2);
		}

		public double GetQuadArea(int tID)
		{
			Vector3d v0 = Vector3d.Zero, v1 = Vector3d.Zero, v2 = Vector3d.Zero, v3 = Vector3.zero;
			GetQuadVertices(tID, ref v0, ref v1, ref v2, ref v3);
			return MathUtil.Area(ref v0, ref v1, ref v2, ref v3);
		}

		/// <summary>
		///  get vertex indices of the endpoints of an edge
		/// </summary>
		/// <param name="eID"></param>
		/// <returns></returns>
		public Index2i GetEdgeV(int eID)
		{
			//debug_check_is_edge(eID);
			int i = 4 * eID;
			return new Index2i(edges[i], edges[i + 1]);
		}


		/// <summary>
		/// Get vertices of endpoints of the edge 
		/// </summary>
		/// <param name="eID"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public bool GetEdgeV(int eID, ref Vector3d a, ref Vector3d b)
		{

			int iv0 = 3 * edges[4 * eID];
			a.x = vertices[iv0]; a.y = vertices[iv0 + 1]; a.z = vertices[iv0 + 2];

			int iv1 = 3 * edges[4 * eID + 1];
			b.x = vertices[iv1]; b.y = vertices[iv1 + 1]; b.z = vertices[iv1 + 2];

			return true;

		}


		/// <summary>
		/// get indices of the two quads on either side of the edge
		/// </summary>
		/// <param name="eID"></param>
		/// <returns></returns>
		public Index2i GetEdgeQ(int eID)
		{
			int i = 4 * eID;
			return new Index2i(edges[i + 2], edges[i + 3]);
		}

		public Index4i GetEdge(int eID)
		{
			int i = 4 * eID;
			return new Index4i(edges[i], edges[i + 1], edges[i + 2], edges[i + 3]);
		}

		/// <summary>
		/// get indices of the two endpoints and faces of the edge
		/// </summary>
		/// <param name="eID"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="t0"></param>
		/// <param name="t1"></param>
		/// <returns></returns>
		public bool GetEdge(int eID, ref int a, ref int b, ref int t0, ref int t1)
		{
			int i = eID * 4;
			a = edges[i]; b = edges[i + 1]; t0 = edges[i + 2]; t1 = edges[i + 3];
			return true;
		}

		/// <summary>
		/// Compute a normal/tangent frame at vertex that is "stable" as long as
		/// the mesh topology doesn't change, meaning that one axis of the frame
		/// will be computed from projection of outgoing edge.
		/// Requires that vertex normals are available.
		/// by default, frame.Z is normal, and .X points along mesh edge
		/// if bFrameNormalY, then frame.Y is normal (X still points along mesh edge)
		/// </summary>
		public Frame3f GetVertexFrame(int vID, bool bFrameNormalY = false)
		{
			int vi = 3 * vID;
			Vector3d v = new Vector3d(vertices[vi], vertices[vi + 1], vertices[vi + 2]);
			Vector3d normal = new Vector3d(normals[vi], normals[vi + 1], normals[vi + 2]);
			int eid = vertex_edges.First(vID);

			int ovi = 3 * edge_other_v(eid, vID);
			Vector3d ov = new Vector3d(vertices[ovi], vertices[ovi + 1], vertices[ovi + 2]);
			Vector3d edge = (ov - v);
			edge.Normalize();

			Vector3d other = normal.Cross(edge);
			edge = other.Cross(normal);
			if (bFrameNormalY)
				return new Frame3f((Vector3f)v, (Vector3f)edge, (Vector3f)normal, (Vector3f)(-other));
			else
				return new Frame3f((Vector3f)v, (Vector3f)edge, (Vector3f)other, (Vector3f)normal);
		}

		#region mesh appending vertices

		/// <summary>
		/// Append new vertex at position, returns new vid
		/// </summary>
		public int AppendVertex(Vector3d v)
		{
			return AppendVertex(new NewVertexInfo()
			{
				v = v,
				bHaveC = false,
				bHaveUV = false,
				bHaveN = false
			});
		}

		/// <summary>
		/// Append new vertex at position and other fields, returns new vid
		/// </summary>
		public int AppendVertex(ref NewVertexInfo info)
		{
			int vid = vertices_refcount.allocate();
			int i = 3 * vid;
			vertices.insert(info.v[2], i + 2);
			vertices.insert(info.v[1], i + 1);
			vertices.insert(info.v[0], i);

			if (normals != null)
			{
				Vector3f n = (info.bHaveN) ? info.n : Vector3f.AxisY;
				normals.insert(n[2], i + 2);
				normals.insert(n[1], i + 1);
				normals.insert(n[0], i);
			}

			if (colors != null)
			{
				Vector3f c = (info.bHaveC) ? info.c : Vector3f.One;
				colors.insert(c[2], i + 2);
				colors.insert(c[1], i + 1);
				colors.insert(c[0], i);
			}

			if (uv != null)
			{
				Vector2f u = (info.bHaveUV) ? info.uv : Vector2f.Zero;
				int j = 2 * vid;
				uv.insert(u[1], j + 1);
				uv.insert(u[0], j);
			}

			allocate_edges_list(vid);

			return vid;
		}

		public int AppendVertex(NewVertexInfo info)
		{
			return AppendVertex(ref info);
		}

		/// <summary>
		/// copy vertex fromVID from existing source mesh, returns new vid
		/// </summary>
		public int AppendVertex(DMesh3 from, int fromVID)
		{
			int bi = 3 * fromVID;

			int vid = vertices_refcount.allocate();
			int i = 3 * vid;
			vertices.insert(from.vertices[bi + 2], i + 2);
			vertices.insert(from.vertices[bi + 1], i + 1);
			vertices.insert(from.vertices[bi], i);
			if (normals != null)
			{
				if (from.normals != null)
				{
					normals.insert(from.normals[bi + 2], i + 2);
					normals.insert(from.normals[bi + 1], i + 1);
					normals.insert(from.normals[bi], i);
				}
				else
				{
					normals.insert(0, i + 2);
					normals.insert(1, i + 1);       // y-up
					normals.insert(0, i);
				}
			}

			if (colors != null)
			{
				if (from.colors != null)
				{
					colors.insert(from.colors[bi + 2], i + 2);
					colors.insert(from.colors[bi + 1], i + 1);
					colors.insert(from.colors[bi], i);
				}
				else
				{
					colors.insert(1, i + 2);
					colors.insert(1, i + 1);       // white
					colors.insert(1, i);
				}
			}

			if (uv != null)
			{
				int j = 2 * vid;
				if (from.uv != null)
				{
					int bj = 2 * fromVID;
					uv.insert(from.uv[bj + 1], j + 1);
					uv.insert(from.uv[bj], j);
				}
				else
				{
					uv.insert(0, j + 1);
					uv.insert(0, j);
				}
			}

			allocate_edges_list(vid);

			return vid;
		}

		/// <summary>
		/// insert vertex at given index, assuming it is unused
		/// If bUnsafe, we use fast id allocation that does not update free list.
		/// You should only be using this between BeginUnsafeVerticesInsert() / EndUnsafeVerticesInsert() calls
		/// </summary>
		public MeshResult InsertVertex(int vid, ref NewVertexInfo info, bool bUnsafe = false)
		{
			if (vertices_refcount.isValid(vid))
				return MeshResult.Failed_VertexAlreadyExists;

			bool bOK = (bUnsafe) ? vertices_refcount.allocate_at_unsafe(vid) :
								   vertices_refcount.allocate_at(vid);
			if (bOK == false)
				return MeshResult.Failed_CannotAllocateVertex;

			int i = 3 * vid;
			vertices.insert(info.v[2], i + 2);
			vertices.insert(info.v[1], i + 1);
			vertices.insert(info.v[0], i);

			if (normals != null)
			{
				Vector3f n = (info.bHaveN) ? info.n : Vector3f.AxisY;
				normals.insert(n[2], i + 2);
				normals.insert(n[1], i + 1);
				normals.insert(n[0], i);
			}

			if (colors != null)
			{
				Vector3f c = (info.bHaveC) ? info.c : Vector3f.One;
				colors.insert(c[2], i + 2);
				colors.insert(c[1], i + 1);
				colors.insert(c[0], i);
			}

			if (uv != null)
			{
				Vector2f u = (info.bHaveUV) ? info.uv : Vector2f.Zero;
				int j = 2 * vid;
				uv.insert(u[1], j + 1);
				uv.insert(u[0], j);
			}

			allocate_edges_list(vid);

			return MeshResult.Ok;
		}

		public MeshResult InsertVertex(int vid, NewVertexInfo info)
		{
			return InsertVertex(vid, ref info);
		}

		#endregion

		#region appending faces

		/// <summary>
		///  Creates a quad from the vertex ids provided
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <param name="v3"></param>
		/// <param name="gid"></param>
		/// <returns></returns>
		public int AppendQuad(int v0, int v1, int v2, int v3, int gid = -1)
		{
			return AppendQuad(new Index4i(v0, v1, v2, v3), gid);
		}

		/// <summary>
		/// creates quad from provided vertex ids 
		/// </summary>
		/// <param name="qv"></param>
		/// <param name="gid"></param>
		/// <returns></returns>
		public int AppendQuad(Index4i qv, int gid = -1)
		{

			if (IsVertex(qv[0]) == false || IsVertex(qv[1]) == false || IsVertex(qv[2]) == false || IsVertex(qv[3]) == false)
			{
				Util.gDevAssert(false);
				return InvalidID;
			}

			// TODO: reject degenerate quad ... 
			if (qv[0] == qv[1] || qv[0] == qv[2] || qv[0] == qv[3])
			{
				Util.gDevAssert(false);
				return InvalidID;
			}

			// look up edges. if any already have two quads, this would 
			// create non-manifold geometry and so we do not allow it
			int e0 = find_edge(qv[0], qv[1]);
			int e1 = find_edge(qv[1], qv[2]);
			int e2 = find_edge(qv[2], qv[3]);
			int e3 = find_edge(qv[3], qv[0]);

			if ((e0 != InvalidID && IsBoundaryEdge(e0) == false)
				 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
 				 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
				 || (e2 != InvalidID && IsBoundaryEdge(e2) == false))
			{
				return NonManifoldID;
			}


			// now safe to insert triangle
			int tid = quad_refCount.allocate();
			int i = 4 * tid;

			quads.insert(qv[3], i + 3);
			quads.insert(qv[2], i + 2);
			quads.insert(qv[1], i + 1);
			quads.insert(qv[0], i + 0);

			if (quad_groups != null)
			{
				quad_groups.insert(gid, tid);
				max_group_id = Math.Max(max_group_id, gid + 1);
			}

			// increment ref counts and update/create edges
			vertices_refcount.increment(qv[0]);
			vertices_refcount.increment(qv[1]);
			vertices_refcount.increment(qv[2]);
			vertices_refcount.increment(qv[3]);

			add_quad_edge(tid, qv[0], qv[1], 0, e0);
			add_quad_edge(tid, qv[1], qv[2], 1, e1);
			add_quad_edge(tid, qv[2], qv[3], 2, e2);
			add_quad_edge(tid, qv[3], qv[0], 3, e3);

			return tid;
		}

		public void InsertVertexIndices(Index4i tv, int quadID)
		{
			int i = 4 * quadID;
			quads.insert(tv[3], i + 3);
			quads.insert(tv[2], i + 2);
			quads.insert(tv[1], i + 1);
			quads.insert(tv[0], i);
		}

		public void IncrementRefCounts(Index4i tv)
		{
			vertices_refcount.increment(tv[0]);
			vertices_refcount.increment(tv[1]);
			vertices_refcount.increment(tv[2]);
			vertices_refcount.increment(tv[3]);
		}

		public void AddQuadEdges(int quadID, Index4i tv, Index4i e)
		{
			add_quad_edge(quadID, tv[0], tv[1], 0, e[0]);
			add_quad_edge(quadID, tv[1], tv[2], 1, e[1]);
			add_quad_edge(quadID, tv[2], tv[3], 2, e[2]);
			add_quad_edge(quadID, tv[3], tv[0], 3, e[3]);
		}

		public void AddQuadEdges(int quadID, Index4i tv, Index4i fids, Index4i e)
		{
			add_quad_edge(quadID, fids[0], tv[0], tv[1], 0,  e[0]);
			add_quad_edge(quadID, fids[1], tv[1], tv[2], 1,  e[1]);
			add_quad_edge(quadID, fids[2], tv[2], tv[3], 2,  e[2]);
			add_quad_edge(quadID, fids[3], tv[3], tv[0], 3,  e[3]);
		}


		/// <summary>
		/// Insert quad given index, assuming it is unused.
		/// If bUnsafe, we use fast id allocation that does not update free list.
		/// You should only be using this between BeginUnsafeTrianglesInsert() / EndUnsafeTrianglesInsert() calls
		/// </summary>
		public MeshResult InsertQuad(int quadID, Index4i tv, int gid = -1, bool bUnsafe = false)
		{

			if (quad_refCount.isValid(quadID))
			{
				return MeshResult.Failed_TriangleAlreadyExists;
			}

			if (IsVertex(tv[0]) == false || IsVertex(tv[1]) == false || IsVertex(tv[2]) == false || !IsVertex(tv[3]))
			{
				Util.gDevAssert(false);
				return MeshResult.Failed_NotAVertex;
			}

			// [TODO] implement for invalid quad 
			if (tv[0] == tv[1] || tv[0] == tv[2] || tv[1] == tv[2])
			{
				Util.gDevAssert(false);
				return MeshResult.Failed_InvalidNeighbourhood;
			}

			int e0 = find_edge(tv[0], tv[1]);
			int e1 = find_edge(tv[1], tv[2]);
			int e2 = find_edge(tv[2], tv[3]);
			int e3 = find_edge(tv[3], tv[0]);

			// if any of the edges already exist and are already connected to 2 quads, reject
			if ((e0 != InvalidID && IsBoundaryEdge(e0) == false)
				 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
				 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
				 || (e2 != InvalidID && IsBoundaryEdge(e2) == false))
			{
				return MeshResult.Failed_WouldCreateNonmanifoldEdge;
			}

			bool bOK = (bUnsafe) ? quad_refCount.allocate_at_unsafe(quadID) :
								   quad_refCount.allocate_at(quadID);

			if (bOK == false)
				return MeshResult.Failed_CannotAllocateTriangle;


			// now safe to insert triangle
			InsertVertexIndices(tv, quadID);

			if (quad_groups != null)
			{
				quad_groups.insert(gid, quadID);
				max_group_id = Math.Max(max_group_id, gid + 1);
			}

			// increment ref counts and update/create edges
			IncrementRefCounts(tv);

			AddQuadEdges(quadID, tv, new Index4i(e0, e1, e2, e3));

			return MeshResult.Ok;


		}

		/// <summary>
		/// Insert quad given index, with face connections
		/// If bUnsafe, we use fast id allocation that does not update free list.
		/// You should only be using this between BeginUnsafeTrianglesInsert() / EndUnsafeTrianglesInsert() calls
		/// </summary>
		public MeshResult InsertQuad(int quadID, Index4i tv, Index4i fids, int gid = -1, bool bUnsafe = false)
		{

			if (quad_refCount.isValid(quadID))
			{
				return MeshResult.Failed_TriangleAlreadyExists;
			}

			if (IsVertex(tv[0]) == false || IsVertex(tv[1]) == false || IsVertex(tv[2]) == false || !IsVertex(tv[3]))
			{
				Util.gDevAssert(false);
				return MeshResult.Failed_NotAVertex;
			}

			// [TODO] implement for invalid quad 
			if (tv[0] == tv[1] || tv[0] == tv[2] || tv[1] == tv[2])
			{
				Util.gDevAssert(false);
				return MeshResult.Failed_InvalidNeighbourhood;
			}

			// look up edges. if any already have two triangles, this would 
			// create non-manifold geometry and so we do not allow it
			int e0 = find_edge(tv[0], tv[1]);
			int e1 = find_edge(tv[1], tv[2]);
			int e2 = find_edge(tv[2], tv[3]);
			int e3 = find_edge(tv[3], tv[0]);

			if ((e0 != InvalidID && IsBoundaryEdge(e0) == false)
				 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
				 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
				 || (e2 != InvalidID && IsBoundaryEdge(e2) == false))
			{
				return MeshResult.Failed_WouldCreateNonmanifoldEdge;
			}

			bool bOK = (bUnsafe) ? quad_refCount.allocate_at_unsafe(quadID) :
								   quad_refCount.allocate_at(quadID);

			if (bOK == false)
				return MeshResult.Failed_CannotAllocateTriangle;


			// now safe to insert triangle
			InsertVertexIndices(tv, quadID);


			if (quad_groups != null)
			{
				quad_groups.insert(gid, quadID);
				max_group_id = Math.Max(max_group_id, gid + 1);
			}


			// increment ref counts and update/create edges
			IncrementRefCounts(tv);
			AddQuadEdges(quadID, tv, fids, new Index4i(e0, e1, e2, e3));

			return MeshResult.Ok;

		}


		public int AddQuad(
			Index4i qv,		// indices of the verts we're adding, in clockwise order
			Index4i qids,   // indices of the neighboring faces, per edge
			int gid = -1
		) {

			if (IsVertex(qv[0]) == false || IsVertex(qv[1]) == false || IsVertex(qv[2]) == false || IsVertex(qv[3]) == false)
			{
				Util.gDevAssert(false);
				return InvalidID;
			}

			// TODO: reject degenerate quad ... 
			if (qv[0] == qv[1] || qv[0] == qv[2] || qv[0] == qv[3])
			{
				Util.gDevAssert(false);
				return InvalidID;
			}

			// look up edges. if any already have two quads, this would 
			// create non-manifold geometry and so we do not allow it
			int e0 = find_edge(qv[0], qv[1]);
			int e1 = find_edge(qv[1], qv[2]);
			int e2 = find_edge(qv[2], qv[3]);
			int e3 = find_edge(qv[3], qv[0]);

			if ((e0 != InvalidID && IsBoundaryEdge(e0) == false)
				 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
 				 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
				 || (e2 != InvalidID && IsBoundaryEdge(e2) == false))
			{
				return NonManifoldID;
			}


			// now safe to insert triangle
			int tid = quad_refCount.allocate();
			int i = 4 * tid;

			quads.insert(qv[3], i + 3);
			quads.insert(qv[2], i + 2);
			quads.insert(qv[1], i + 1);
			quads.insert(qv[0], i + 0);

			// add quad group if provided
			if (quad_groups != null)
			{
				quad_groups.insert(gid, tid);
				max_group_id = Math.Max(max_group_id, gid + 1);
			}


			// increment ref counts for each vertex
			vertices_refcount.increment(qv[0]);
			vertices_refcount.increment(qv[1]);
			vertices_refcount.increment(qv[2]);
			vertices_refcount.increment(qv[3]);


			// add the 4 edges, connected to the other faces
			add_quad_edge(tid, qids[0], qv[0], qv[1], 0, e0);
			add_quad_edge(tid, qids[1], qv[1], qv[2], 1, e1);
			add_quad_edge(tid, qids[2], qv[2], qv[3], 2, e2);
			add_quad_edge(tid, qids[3], qv[3], qv[0], 3, e3);

			return tid;

		}
			
		#endregion

		#region todo

	
		/// <summary>
		/// Compute triangle normal, area, and centroid all at once. Re-uses vertex
		/// lookups and computes normal & area simultaneously. *However* does not produce
		/// the same normal/area as separate calls, because of this.
		/// </summary>
		public void GetQuadInfo(int tID, out Vector3d normal, out double fArea, out Vector3d vCentroid)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region ENABLING TOPOLOGY

		public void EnableVertexNormals(Vector3f initial_normal)
		{
			if (HasVertexNormals)
				return;
			normals = new DVector<float>();
			int NV = MaxVertexID;
			normals.resize(3 * NV);
			for (int i = 0; i < NV; ++i)
			{
				int vi = 3 * i;
				normals[vi] = initial_normal.x;
				normals[vi + 1] = initial_normal.y;
				normals[vi + 2] = initial_normal.z;
			}
		}
		public void DiscardVertexNormals()
		{
			normals = null;
		}

		public void EnableVertexColors(Vector3f initial_color)
		{
			if (HasVertexColors)
				return;
			colors = new DVector<float>();
			int NV = MaxVertexID;
			colors.resize(3 * NV);
			for (int i = 0; i < NV; ++i)
			{
				int vi = 3 * i;
				colors[vi] = initial_color.x;
				colors[vi + 1] = initial_color.y;
				colors[vi + 2] = initial_color.z;
			}
		}
		public void DiscardVertexColors()
		{
			colors = null;
		}

		public void EnableVertexUVs(Vector2f initial_uv)
		{
			if (HasVertexUVs)
				return;
			uv = new DVector<float>();
			int NV = MaxVertexID;
			uv.resize(2 * NV);
			for (int i = 0; i < NV; ++i)
			{
				int vi = 2 * i;
				uv[vi] = initial_uv.x;
				uv[vi + 1] = initial_uv.y;
			}
		}
		public void DiscardVertexUVs()
		{
			uv = null;
		}

		public void EnableQuadGroups(int initial_group = 0)
		{
			if (HasQuadGroups)
				return;
			quad_groups = new DVector<int>();
			int NT = MaxQuadID;
			quad_groups.resize(NT);
			for (int i = 0; i < NT; ++i)
				quad_groups[i] = initial_group;
			max_group_id = 0;
		}

		public void DiscardQuadGroups()
		{
			quad_groups = null;
			max_group_id = 0;
		}

		#endregion

		#region helpers

		/// <summary>
		/// an edge is a boundary if its left face pointer is -1 (the right face pointer can never be invalid)
		/// </summary>
		/// <param name="eid"></param>
		/// <returns></returns>
		public bool IsBoundaryEdge(int eid)
		{
			return edges[4 * eid + 3] == InvalidID;
		}

		void allocate_edges_list(int vid)
		{
			if (vid < vertex_edges.Size)
				vertex_edges.Clear(vid);
			vertex_edges.AllocateAt(vid);
		}

		int find_edge(int vA, int vB)
		{
			// [RMS] edge vertices must be sorted (min,max),
			//   that means we only need one index-check in inner loop.
			//   commented out code is robust to incorrect ordering, but slower.
			int vO = Math.Max(vA, vB);
			int vI = Math.Min(vA, vB);

			foreach (int eid in vertex_edges.ValueItr(vI))
			{
				if (edges[4 * eid + 1] == vO)
					return eid;
			}
			return InvalidID;

			// this is slower, likely because it creates new func<> every time. can we do w/o that?
			//return vertex_edges.Find(vI, (eid) => { return edges[4 * eid + 1] == vO; }, InvalidID);
		}

		void add_quad_edge(int quadID, int vID0, int vID1, int j, int edgeID)
		{
			if (edgeID != InvalidID)
			{
				edges[4 * edgeID + 3] = quadID;
				quad_edges.insert(edgeID, 4 * quadID + j);
			}
			else
				quad_edges.insert(add_edge(vID0, vID1, quadID), 4 * quadID + j);
		}

		void add_quad_edge(int quadID, int otherQuadID, int vID0, int vID1, int j, int edgeID)
		{
			if (edgeID != InvalidID)
			{
				edges[4 * edgeID + 3] = quadID;
				quad_edges.insert(edgeID, 4 * quadID + j);
			}
			else {
				quad_edges.insert(add_edge(vID0, vID1, quadID, otherQuadID), 4 * quadID + j);
			}

		}

		int add_edge(int vA, int vB, int tA, int tB = InvalidID)
		{
			if (vB < vA)
			{
				int t = vB; vB = vA; vA = t;
			}

			int eid = edges_refcount.allocate();
			int i = 4 * eid;

			edges.insert(vA, i);
			edges.insert(vB, i + 1);
			edges.insert(tA, i + 2);
			edges.insert(tB, i + 3);

			vertex_edges.Insert(vA, eid);
			vertex_edges.Insert(vB, eid);

			return eid;

		}

		public int edge_other_v(int eID, int vID)
		{
			int i = 4 * eID;
			int ev0 = edges[i], ev1 = edges[i + 1];
			return (ev0 == vID) ? ev1 : ((ev1 == vID) ? ev0 : InvalidID);
		}

		#endregion

	}
}

