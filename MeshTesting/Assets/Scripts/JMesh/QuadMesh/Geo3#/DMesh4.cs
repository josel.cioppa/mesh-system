#define DMESH4_DEBUG 

using System;
using System.Collections.Generic;
using System.Linq;


namespace g3 {


	public struct QuadData {
		public Index4i vertexIndices;
		public Index4i edgeIndices;
	}
	public struct EdgeData {

		public int startVertID;
		public int endVertID;
		public int primaryFaceID;
		public int secondaryFaceID;

		public bool IsBoundaryEdge() {
			return secondaryFaceID == DMesh3.InvalidID;
		}

	}
	public struct VertexData {
		public Vector3d position, normal, color;
		public Vector2d uv;
	}



	public class DMesh4 : IMesh
	{

		#region custom data getters

		public VertexData GetVertexData(int vID) {
			VertexData data = new VertexData() { };
			int i = 3 * vID;
			data.position.Set(vertices[i], vertices[i + 1], vertices[i + 2]);
			data.normal.Set(normals[i], normals[i + 1], normals[i + 2]);
			data.color.Set(colors[i], colors[i + 1], colors[i + 2]);
			data.uv.Set(uv[i], uv[i + 1]);
			return data;
		}

		internal int DuplicateFace(int faceID)
		{
			Index4i verts = GetQuadVertices(faceID);
			return AppendIsolatedQuad(GetVertex(verts.a), GetVertex(verts.b), GetVertex(verts.c), GetVertex(verts.d));
		}

		public bool TryGetVertexData(int vID, ref VertexData data)
		{

			if (!IsVertex(vID))
				return false;

			int i = 3 * vID;
			data.position.Set(vertices[i], vertices[i + 1], vertices[i + 2]);
			data.normal.Set(normals[i], normals[i + 1], normals[i + 2]);
			data.color.Set(colors[i], colors[i + 1], colors[i + 2]);
			data.uv.Set(uv[i], uv[i + 1]);

			return true;

		}

		public QuadData GetQuadData(int qID) {

			QuadData result = new QuadData();

			result.vertexIndices = new Index4i()
			{
				a = quads[4 * qID + 0],
				b = quads[4 * qID + 1],
				c = quads[4 * qID + 2],
				d = quads[4 * qID + 3]
			};

			result.edgeIndices = new Index4i() {
				a = quad_edges[4 * qID + 0],
				b = quad_edges[4 * qID + 1],
				c = quad_edges[4 * qID + 2],
				d = quad_edges[4 * qID + 3]
			};

			return result;

		}

		public bool TryGetQuadData(int qID, ref QuadData result)
		{

			if (!IsQuad(qID)) return false;

			result.vertexIndices = new Index4i()
			{
				a = quads[4 * qID + 0],
				b = quads[4 * qID + 1],
				c = quads[4 * qID + 2],
				d = quads[4 * qID + 3]
			};

			result.edgeIndices = new Index4i()
			{
				a = quad_edges[4 * qID + 0],
				b = quad_edges[4 * qID + 1],
				c = quad_edges[4 * qID + 2],
				d = quad_edges[4 * qID + 3]


			};

			return true;

		}


		public EdgeData GetEdgeData(int eID) {



			EdgeData data = new EdgeData()
			{
				startVertID = edges[4 * eID + 0],
				endVertID = edges[4 * eID + 1],
				primaryFaceID = edges[4 * eID + 2],
				secondaryFaceID = edges[4 * eID + 3]
			};

			return data;

		}

		public bool TryGetEdgeData(int eID, ref EdgeData data)
		{

			if (!IsEdge(eID))
				return false;

			data.startVertID = edges[4 * eID + 0];
			data.endVertID = edges[4 * eID + 1];
			data.primaryFaceID = edges[4 * eID + 2];
			data.secondaryFaceID = edges[4 * eID + 3];

			return true;
		}


		#endregion

		#region fields

		public UnityEngine.Bounds meshBounds = new UnityEngine.Bounds();

		public const int InvalidID = -1;
		public const int NonManifoldID = -2;

		public RefCountVector vertices_refcount;
		public DVector<double> vertices;
		public SmallListSet vertex_edges;
		public DVector<float> normals;
		public DVector<float> colors;
		public DVector<float> uv;
		public static readonly Vector3d InvalidVertex = new Vector3d(Double.MaxValue, 0, 0);

		// [TODO] this is optional if we only want to use this class as an iterable mesh-with-nbrs

		public static readonly Index4i InvalidQuad = new Index4i(InvalidID, InvalidID, InvalidID, InvalidID);
		public RefCountVector quads_refcount;
		public DVector<int> quads;
		public DVector<int> quad_edges;
		public DVector<int> quad_groups;



		public static readonly Index2i InvalidEdge = new Index2i(InvalidID, InvalidID);
		public DVector<int> edges;
		public RefCountVector edges_refcount;

		int timestamp = 0;
		int shape_timestamp = 0;

		int max_group_id = 0;

		#endregion

		#region constructors

		public DMesh4(bool bWantNormals = true, bool bWantColors = false, bool bWantUVs = false, bool bWantTriGroups = false)
		{
			

			vertices = new DVector<double>();
			if (bWantNormals)
				normals = new DVector<float>();
			if (bWantColors)
				colors = new DVector<float>();
			if (bWantUVs)
				uv = new DVector<float>();

			vertex_edges = new SmallListSet();

			vertices_refcount = new RefCountVector();

			quads = new DVector<int>();
			quad_edges = new DVector<int>();
			quads_refcount = new RefCountVector();
			if (bWantTriGroups)
				quad_groups = new DVector<int>();
			max_group_id = 0;

			edges = new DVector<int>();
			edges_refcount = new RefCountVector();
		}

		public DMesh4(MeshComponents flags) :
			this((flags & MeshComponents.VertexNormals) != 0, (flags & MeshComponents.VertexColors) != 0,
				  (flags & MeshComponents.VertexUVs) != 0, (flags & MeshComponents.FaceGroups) != 0)
		{
		}

		// normals/colors/uvs will only be copied if they exist
		public DMesh4(DMesh4 copy, bool bCompact = false, bool bWantNormals = true, bool bWantColors = true, bool bWantUVs = true)
		{
			if (bCompact)
				CompactCopy(copy, bWantNormals, bWantColors, bWantUVs);
			else
				Copy(copy, bWantNormals, bWantColors, bWantUVs);
		}
		public DMesh4(DMesh4 copy, bool bCompact, MeshComponents flags) :
			this(copy, bCompact, (flags & MeshComponents.VertexNormals) != 0, (flags & MeshComponents.VertexColors) != 0,
				  (flags & MeshComponents.VertexUVs) != 0)
		{
		}




		public struct CompactInfo
		{
			public IIndexMap MapV;
		}

		public CompactInfo CompactCopy(DMesh4 copy, bool bNormals = true, bool bColors = true, bool bUVs = true)
		{

			if (copy.IsCompact)
			{
				Copy(copy, bNormals, bColors, bUVs);
				CompactInfo ci = new CompactInfo() { MapV = new IdentityIndexMap() };
				return ci;
			}

			vertices = new DVector<double>();
			vertex_edges = new SmallListSet();
			vertices_refcount = new RefCountVector();
			quads = new DVector<int>();
			quad_edges = new DVector<int>();
			quads_refcount = new RefCountVector();
			edges = new DVector<int>();
			edges_refcount = new RefCountVector();
			max_group_id = 0;

			normals = (bNormals && copy.normals != null) ? new DVector<float>() : null;
			colors = (bColors && copy.colors != null) ? new DVector<float>() : null;
			uv = (bUVs && copy.uv != null) ? new DVector<float>() : null;
			quad_groups = (copy.quad_groups != null) ? new DVector<int>() : null;

			// [TODO] if we knew some of these were dense we could copy directly...

			NewVertexInfo vinfo = new NewVertexInfo();
			int[] mapV = new int[copy.MaxVertexID];

			foreach (int vid in copy.vertices_refcount)
			{
				copy.GetVertex(vid, ref vinfo, bNormals, bColors, bUVs);
				mapV[vid] = AppendVertex(vinfo);
			}

			// [TODO] would be much faster to explicitly copy triangle & edge data structures!!
			foreach (int tid in copy.quads_refcount)
			{
				Index4i t = copy.GetQuad(tid);
				t.a = mapV[t.a]; t.b = mapV[t.b]; t.c = mapV[t.c]; t.d = mapV[t.d];

				int g = (copy.HasQuadGroups) ? copy.GetQuadGroup(tid) : InvalidID;
				AppendQuad(t, g);
				max_group_id = Math.Max(max_group_id, g + 1);
			}

			return new CompactInfo()
			{
				MapV = new IndexMap(mapV, this.MaxVertexID)
			};
		}


		public void Copy(DMesh4 copy, bool bNormals = true, bool bColors = true, bool bUVs = true)
		{
			vertices = new DVector<double>(copy.vertices);

			normals = (bNormals && copy.normals != null) ? new DVector<float>(copy.normals) : null;
			colors = (bColors && copy.colors != null) ? new DVector<float>(copy.colors) : null;
			uv = (bUVs && copy.uv != null) ? new DVector<float>(copy.uv) : null;

			vertices_refcount = new RefCountVector(copy.vertices_refcount);

			vertex_edges = new SmallListSet(copy.vertex_edges);

			quads = new DVector<int>(copy.quads);
			quad_edges = new DVector<int>(copy.quad_edges);
			quads_refcount = new RefCountVector(copy.quads_refcount);

			if (copy.quad_groups != null)
				quad_groups = new DVector<int>(copy.quad_groups);
			max_group_id = copy.max_group_id;

			edges = new DVector<int>(copy.edges);
			edges_refcount = new RefCountVector(copy.edges_refcount);
		}





		#endregion


		#region IMesh implementation 

		public int VertexCount
		{
			get { return vertices_refcount.count; }
		}
		public int QuadCount
		{
			get { return quads_refcount.count; }
		}
		public int EdgeCount
		{
			get { return edges_refcount.count; }
		}

		// these values are (max_used+1), ie so an iteration should be < MaxTriangleID, not <=
		public int MaxVertexID
		{
			get { return vertices_refcount.max_index; }
		}
		public int MaxQuadID
		{
			get { return quads_refcount.max_index; }
		}
		public int MaxEdgeID
		{
			get { return edges_refcount.max_index; }
		}
		public int MaxGroupID
		{
			get { return max_group_id; }
		}

		#endregion


		public bool HasVertexColors { get { return colors != null; } }
		public bool HasVertexNormals { get { return normals != null; } }
		public bool HasVertexUVs { get { return uv != null; } }
		public bool HasQuadGroups { get { return quad_groups != null; } }

		public MeshComponents Components
		{
			get
			{
				MeshComponents c = 0;
				if (normals != null) c |= MeshComponents.VertexNormals;
				if (colors != null) c |= MeshComponents.VertexColors;
				if (uv != null) c |= MeshComponents.VertexUVs;
				if (quad_groups != null) c |= MeshComponents.FaceGroups;
				return c;
			}
		}

		// info

		public bool IsVertex(int vID)
		{
			return vertices_refcount.isValid(vID);
		}
		public bool IsQuad(int tID)
		{
			return quads_refcount.isValid(tID);
		}
		public bool IsEdge(int eID)
		{
			return edges_refcount.isValid(eID);
		}

		public Vector3d GetVertex(int vID)
		{
			int i = 3 * vID;
			return new Vector3d(vertices[i], vertices[i + 1], vertices[i + 2]);
		}

		public Vector3f GetVertexf(int vID)
		{
			int i = 3 * vID;
			return new Vector3f((float)vertices[i], (float)vertices[i + 1], (float)vertices[i + 2]);
		}

		public void SetVertex(int vID, NewVertexInfo vInfo)
		{
			int i = 3 * vID;
			vertices[i] = vInfo.v.x; vertices[i + 1] = vInfo.v.y; vertices[i + 2] = vInfo.v.z;
			if (HasVertexNormals) { normals[i] = vInfo.n.x; normals[i + 1] = vInfo.n.y; normals[i + 2] = vInfo.n.z; }
			if (HasVertexColors) { colors[i] = vInfo.c.x; colors[i + 1] = vInfo.c.y; colors[i + 2] = vInfo.c.z; }

			if (HasVertexUVs)
			{
				uv[i] = vInfo.uv.x; uv[i + 1] = vInfo.uv.y;
			}

		}

		public void SetVertex(int vID, Vector3d vNewPos)
		{
			int i = 3 * vID;
			vertices[i] = vNewPos.x; vertices[i + 1] = vNewPos.y; vertices[i + 2] = vNewPos.z;
		}

		public Vector3f GetVertexNormal(int vID)
		{
			if (normals == null)
			{
				return Vector3f.AxisY;
			}
			else
			{
				int i = 3 * vID;
				return new Vector3f(normals[i], normals[i + 1], normals[i + 2]);
			}
		}

		public void SetVertexNormal(int vID, Vector3f vNewNormal)
		{
			if (HasVertexNormals)
			{
				int i = 3 * vID;
				normals[i] = vNewNormal.x; normals[i + 1] = vNewNormal.y; normals[i + 2] = vNewNormal.z;
			}
		}

		public Vector3f GetVertexColor(int vID)
		{
			if (colors == null)
			{
				return Vector3f.One;
			}
			else
			{
				int i = 3 * vID;
				return new Vector3f(colors[i], colors[i + 1], colors[i + 2]);
			}
		}

		public void SetVertexColor(int vID, Vector3f vNewColor)
		{
			if (HasVertexColors)
			{
				int i = 3 * vID;
				colors[i] = vNewColor.x; colors[i + 1] = vNewColor.y; colors[i + 2] = vNewColor.z;
			}
		}

		public Vector2f GetVertexUV(int vID)
		{
			if (uv == null)
			{
				return Vector2f.Zero;
			}
			else
			{
				int i = 2 * vID;
				return new Vector2f(uv[i], uv[i + 1]);
			}
		}

		public void SetVertexUV(int vID, Vector2f vNewUV)
		{
			if (HasVertexUVs)
			{
				int i = 2 * vID;
				uv[i] = vNewUV.x; uv[i + 1] = vNewUV.y;
			}
		}

		public bool GetVertex(int vID, ref NewVertexInfo vinfo, bool bWantNormals, bool bWantColors, bool bWantUVs)
		{
			if (vertices_refcount.isValid(vID) == false)
				return false;

			vinfo.v.Set(vertices[3 * vID], vertices[3 * vID + 1], vertices[3 * vID + 2]);
			vinfo.bHaveN = vinfo.bHaveUV = vinfo.bHaveC = false;

			if (HasVertexNormals && bWantNormals)
			{
				vinfo.bHaveN = true;
				vinfo.n.Set(normals[3 * vID], normals[3 * vID + 1], normals[3 * vID + 2]);
			}
			if (HasVertexColors && bWantColors)
			{
				vinfo.bHaveC = true;
				vinfo.c.Set(colors[3 * vID], colors[3 * vID + 1], colors[3 * vID + 2]);
			}
			if (HasVertexUVs && bWantUVs)
			{
				vinfo.bHaveUV = true;
				vinfo.uv.Set(uv[2 * vID], uv[2 * vID + 1]);
			}
			return true;
		}


		public int GetVtxEdgeCount(int vID)
		{
			return vertices_refcount.isValid(vID) ? vertex_edges.Count(vID) : -1;
		}

		public int GetMaxVtxEdgeCount()
		{
			int max = 0;
			foreach (int vid in vertices_refcount)
				max = Math.Max(max, vertex_edges.Count(vid));
			return max;
		}

		public NewVertexInfo GetVertexAll(int i)
		{
			NewVertexInfo vi = new NewVertexInfo();
			vi.v = GetVertex(i);
			if (HasVertexNormals)
			{
				vi.bHaveN = true;
				vi.n = GetVertexNormal(i);
			}
			else
				vi.bHaveN = false;
			if (HasVertexColors)
			{
				vi.bHaveC = true;
				vi.c = GetVertexColor(i);
			}
			else
				vi.bHaveC = false;
			if (HasVertexUVs)
			{
				vi.bHaveUV = true;
				vi.uv = GetVertexUV(i);
			}
			else
				vi.bHaveUV = false;
			return vi;
		}


		/// <summary>
		/// Compute a normal/tangent frame at vertex that is "stable" as long as
		/// the mesh topology doesn't change, meaning that one axis of the frame
		/// will be computed from projection of outgoing edge.
		/// Requires that vertex normals are available.
		/// by default, frame.Z is normal, and .X points along mesh edge
		/// if bFrameNormalY, then frame.Y is normal (X still points along mesh edge)
		/// </summary>
		public Frame3f GetVertexFrame(int vID, bool bFrameNormalY = false)
		{

			int vi = 3 * vID;
			Vector3d v = new Vector3d(vertices[vi], vertices[vi + 1], vertices[vi + 2]);
			Vector3d normal = new Vector3d(normals[vi], normals[vi + 1], normals[vi + 2]);
			int eid = vertex_edges.First(vID);
			int ovi = 3 * edge_other_v(eid, vID);
			Vector3d ov = new Vector3d(vertices[ovi], vertices[ovi + 1], vertices[ovi + 2]);
			Vector3d edge = (ov - v);
			edge.Normalize();

			Vector3d other = normal.Cross(edge);
			edge = other.Cross(normal);
			if (bFrameNormalY)
				return new Frame3f((Vector3f)v, (Vector3f)edge, (Vector3f)normal, (Vector3f)(-other));
			else
				return new Frame3f((Vector3f)v, (Vector3f)edge, (Vector3f)other, (Vector3f)normal);

		}

		public Index4i GetQuad(int tID)
		{
			int i = 4 * tID;
			return new Index4i(quads[i], quads[i + 1], quads[i + 2], quads[i + 3]);
		}

		public Index4i GetQuadEdges(int tID)
		{
			int i = 4 * tID;
			return new Index4i(quad_edges[i], quad_edges[i + 1], quad_edges[i + 2], quad_edges[i + 3]);
		}

		public int GetQuadEdge(int tid, int j)
		{
			return quad_edges[4 * tid + j];
		}

		public Index4i GetQuadNeighbourQuads(int tID)
		{
			if (quads_refcount.isValid(tID))
			{
				int tei = 4 * tID;
				Index4i nbr_t = Index4i.Zero;

				for (int j = 0; j < 4; ++j)
				{
					int ei = 4 * quad_edges[tei + j];
					nbr_t[j] = (edges[ei + 2] == tID) ? edges[ei + 3] : edges[ei + 2];
				}
				return nbr_t;
			}
			else
				return InvalidQuad;
		}

		public IEnumerable<int> QuadQuadsItr(int tID)
		{
			if (quads_refcount.isValid(tID))
			{
				int tei = 4 * tID;
				for (int j = 0; j < 4; ++j)
				{
					int ei = 4 * quad_edges[tei + j];
					int nbr_t = (edges[ei + 2] == tID) ? edges[ei + 3] : edges[ei + 2];
					if (nbr_t != DMesh4.InvalidID)
						yield return nbr_t;
				}
			}
		}



		public int GetQuadGroup(int tID)
		{
			return (quad_groups == null) ? -1
					: (quads_refcount.isValid(tID) ? quad_groups[tID] : 0);
		}

		public void SetQuadGroup(int tid, int group_id)
		{
			if (quad_groups != null)
			{
				quad_groups[tid] = group_id;
				max_group_id = Math.Max(max_group_id, group_id + 1);
			}
		}

		public int AllocateQuadGroup()
		{
			return ++max_group_id;
		}


		public Index4i GetQuadVertices(int tID) {
			Index4i res = new Index4i();
			GetQuadVertices(tID, ref res);
			return res;
		}

		public void GetQuadVertices(int tID, ref Index4i indices) {
			indices.a = quads[4 * tID + 0];
			indices.b = quads[4 * tID + 1];
			indices.c = quads[4 * tID + 2];
			indices.d = quads[4 * tID + 3];
		}

		public void GetQuadVertices(int tID, ref Vector3d v0, ref Vector3d v1, ref Vector3d v2, ref Vector3d v3)
		{

			int ai = 3 * quads[4 * tID];
			v0.x = vertices[ai]; v0.y = vertices[ai + 1]; v0.z = vertices[ai + 2];

			int bi = 3 * quads[4 * tID + 1];
			v1.x = vertices[bi]; v1.y = vertices[bi + 1]; v1.z = vertices[bi + 2];

			int ci = 3 * quads[4 * tID + 2];
			v2.x = vertices[ci]; v2.y = vertices[ci + 1]; v2.z = vertices[ci + 2];

			int di = 3 * quads[4 * tID + 3];
			v3.x = vertices[ci]; v3.y = vertices[ci + 1]; v3.z = vertices[ci + 2];

		}

		public Vector3d GetQuadVertex(int tid, int j)
		{
			int a = quads[4 * tid + j];
			return new Vector3d(vertices[3 * a], vertices[3 * a + 1], vertices[3 * a + 2]);
		}

		public Vector3d GetPointInQuad(int tid, Vector2d coords) {

			int ai = 3 * quads[4 * tid],
				bi = 3 * quads[4 * tid + 1],
				ci = 3 * quads[4 * tid + 3];

			// return va + coords.x * (vb - va) + coords.y * (vc - va)
			return new Vector3d(
				vertices[ai] + coords.x * (vertices[bi] - vertices[ai]) + coords.y * (vertices[ci] - vertices[ai]),
				vertices[ai + 1] + coords.x * (vertices[bi + 1] - vertices[ai + 1]) + coords.y * (vertices[ci + 1] - vertices[ai + 1]),
				vertices[ai + 2] + coords.x * (vertices[bi + 2] - vertices[ai + 2]) + coords.y * (vertices[ci + 2] - vertices[ai + 2])
			);
		}



		public Vector3d GetQuadNormal(int tID)
		{
			Vector3d v0 = Vector3d.Zero, v1 = Vector3d.Zero, v2 = Vector3d.Zero, v3 = Vector3d.Zero;
			GetQuadVertices(tID, ref v0, ref v1, ref v2, ref v3);
			return MathUtil.Normal(ref v0, ref v1, ref v2);
		}

		public double GetQuadArea(int tID)
		{
			Vector3d v0 = Vector3d.Zero, v1 = Vector3d.Zero, v2 = Vector3d.Zero, v3 = Vector3d.Zero;
			GetQuadVertices(tID, ref v0, ref v1, ref v2, ref v3);
			return MathUtil.Area(ref v0, ref v1, ref v2, ref v3);
		}

		/// <summary>
		/// Compute triangle normal, area, and centroid all at once. Re-uses vertex
		/// lookups and computes normal & area simultaneously. *However* does not produce
		/// the same normal/area as separate calls, because of this.
		/// </summary>
		public void GetQuadInfo(int tID, out Vector3d normal, out double fArea, out Vector3d vCentroid)
		{
			Vector3d v0 = Vector3d.Zero, v1 = Vector3d.Zero, v2 = Vector3d.Zero, v3 = Vector3d.Zero;
			GetQuadVertices(tID, ref v0, ref v1, ref v2, ref v3);
			vCentroid = (1.0 / 4.0) * (v0 + v1 + v2 + v3);
			normal = MathUtil.FastNormalArea(ref v0, ref v1, ref v2, out fArea);
		}





		/// <summary>
		/// construct bounding box of triangle as efficiently as possible
		/// </summary>
		public AxisAlignedBox3d GetQuadBounds(int tID)
		{
			int vi = 3 * quads[4 * tID];
			double x = vertices[vi], y = vertices[vi + 1], z = vertices[vi + 2];
			double minx = x, maxx = x, miny = y, maxy = y, minz = z, maxz = z;

			for (int i = 1; i < 4; ++i)
			{
				vi = 3 * quads[4 * tID + i];
				x = vertices[vi]; y = vertices[vi + 1]; z = vertices[vi + 2];
				if (x < minx) minx = x; else if (x > maxx) maxx = x;
				if (y < miny) miny = y; else if (y > maxy) maxy = y;
				if (z < minz) minz = z; else if (z > maxz) maxz = z;
			}

			return new AxisAlignedBox3d(minx, miny, minz, maxx, maxy, maxz);

		}


		/// <summary>
		/// Construct stable frame at triangle centroid, where frame.Z is face normal,
		/// and frame.X is aligned with edge nEdge of triangle.
		/// </summary>
		public Frame3f GetQuadFrame(int tID, int nEdge = 0)
		{
			int ti = 4 * tID;
			int a = 3 * quads[ti + (nEdge % 4)];
			int b = 3 * quads[ti + ((nEdge + 1) % 4)];
			int c = 3 * quads[ti + ((nEdge + 2) % 4)];
			int d = 3 * quads[ti + ((nEdge + 3) % 4)];


			Vector3d v1 = new Vector3d(vertices[a], vertices[a + 1], vertices[a + 2]);
			Vector3d v2 = new Vector3d(vertices[b], vertices[b + 1], vertices[b + 2]);
			Vector3d v3 = new Vector3d(vertices[c], vertices[c + 1], vertices[c + 2]);
			Vector3d v4 = new Vector3d(vertices[d], vertices[d + 1], vertices[d + 2]);


			Vector3d edge1 = v2 - v1; edge1.Normalize();
			Vector3d edge2 = v3 - v2; edge2.Normalize();
			Vector3d normal = edge1.Cross(edge2);
			normal.Normalize();

			Vector3d other = normal.Cross(edge1);

			Vector3f center = (Vector3f)(v1 + v2 + v3 + v4) / 4;
			return new Frame3f(center, (Vector3f)edge1, (Vector3f)other, (Vector3f)normal);

		}



		public Index2i GetEdgeV(int eID)
		{
			int i = 4 * eID;
			return new Index2i(edges[i], edges[i + 1]);
		}

		public bool GetEdgeV(int eID, ref Vector3d a, ref Vector3d b)
		{

			int iv0 = 3 * edges[4 * eID];
			a.x = vertices[iv0]; a.y = vertices[iv0 + 1]; a.z = vertices[iv0 + 2];

			int iv1 = 3 * edges[4 * eID + 1];
			b.x = vertices[iv1]; b.y = vertices[iv1 + 1]; b.z = vertices[iv1 + 2];

			return true;

		}

		public Index2i GetEdgeQ(int eID)
		{
			int i = 4 * eID;
			return new Index2i(edges[i + 2], edges[i + 3]);
		}

		/// <summary>
		/// return [v0,v1,t0,t1], or Index4i.Max if eid is invalid
		/// </summary>
		public Index4i GetEdge(int eID)
		{
			int i = 4 * eID;
			return new Index4i(edges[i], edges[i + 1], edges[i + 2], edges[i + 3]);
		}


		public bool GetEdge(int eID, ref int a, ref int b, ref int t0, ref int t1)
		{
			int i = eID * 4;
			a = edges[i]; b = edges[i + 1]; t0 = edges[i + 2]; t1 = edges[i + 3];
			return true;
		}


		/// <summary>
		/// Get the average of the quad normal of the give edge
		/// </summary>
		/// <returns>The edge normal.</returns>
		/// <param name="eID">E identifier.</param>
		public Vector3d GetEdgeNormal(int eID)
		{

			if (edges_refcount.isValid(eID))
			{
				int ei = 4 * eID;
				Vector3d n = GetQuadNormal(edges[ei + 2]);

				if (edges[ei + 3] != InvalidID)
				{
					n += GetQuadNormal(edges[ei + 3]);
					n.Normalize();
				}

				return n;
			}

			return Vector3d.Zero;

		}

		/// <summary>
		/// Gets the interpolated edge point along the given edge
		/// </summary>
		/// <returns>The edge point.</returns>
		/// <param name="eID">E identifier.</param>
		/// <param name="t">T.</param>
		public Vector3d GetEdgePoint(int eID, double t)
		{
			if (edges_refcount.isValid(eID))
			{
				int ei = 4 * eID;
				int iv0 = 3 * edges[ei];
				int iv1 = 3 * edges[ei + 1];
				double mt = 1.0 - t;

				return new Vector3d(
					mt * vertices[iv0] + t * vertices[iv1],
					mt * vertices[iv0 + 1] + t * vertices[iv1 + 1],
					mt * vertices[iv0 + 2] + t * vertices[iv1 + 2]);
			}
			return Vector3d.Zero;
		}

		/// <summary>
		/// Gets the interpolated edge point along the given edge
		/// </summary>
		/// <returns>The edge point.</returns>
		/// <param name="eID">E identifier.</param>
		/// <param name="t">T.</param>
		public Vector3d GetEdgePoint(int eID, int qID, double t)
		{
			if (edges_refcount.isValid(eID))
			{

				int iv0 = 3 * startVertex(eID, qID);
				int iv1 = 3 * endVertex(eID, qID);
				double mt = 1.0 - t;

				return new Vector3d(
					mt * vertices[iv0] + t * vertices[iv1],
					mt * vertices[iv0 + 1] + t * vertices[iv1 + 1],
					mt * vertices[iv0 + 2] + t * vertices[iv1 + 2]);
			}

			return Vector3d.Zero;

		}


		/// <summary>
		/// Append new vertex at position, returns new vid
		/// </summary>
		public int AppendVertex(Vector3d v)
		{
			return AppendVertex(new NewVertexInfo()
			{
				v = v,
				bHaveC = false,
				bHaveUV = false,
				bHaveN = false
			});
		}

		/// <summary>
		/// Append new vertex at position and other fields, returns new vid. Also creates empty
		/// space for tracking the vertex_edges
		/// </summary>
		public int AppendVertex(ref NewVertexInfo info)
		{

			int vid = vertices_refcount.allocate();

#if DMESH4_DEBUG
			UnityEngine.Debug.Assert(IsVertex(vid), string.Format("vertex {0} is not valid", vid));
			UnityEngine.Debug.Assert(vertices_refcount.isValid(vid));
#endif

			int i = 3 * vid;
			vertices.insert(info.v[2], i + 2);
			vertices.insert(info.v[1], i + 1);
			vertices.insert(info.v[0], i);

			meshBounds.Encapsulate(new UnityEngine.Vector3((float) info.v[0], (float) info.v[1],  (float) info.v[2]));

			if (normals != null)
			{
				Vector3f n = (info.bHaveN) ? info.n : Vector3f.AxisY;
				normals.insert(n[2], i + 2);
				normals.insert(n[1], i + 1);
				normals.insert(n[0], i);
			}

			if (colors != null)
			{
				Vector3f c = (info.bHaveC) ? info.c : Vector3f.One;
				colors.insert(c[2], i + 2);
				colors.insert(c[1], i + 1);
				colors.insert(c[0], i);
			}

			if (uv != null)
			{
				Vector2f u = (info.bHaveUV) ? info.uv : Vector2f.Zero;
				int j = 2 * vid;
				uv.insert(u[1], j + 1);
				uv.insert(u[0], j);
			}

			allocate_edges_list(vid);

			return vid;

		}

		void allocate_edges_list(int vid)
		{
			if (vid < vertex_edges.Size)
				vertex_edges.Clear(vid);

			vertex_edges.AllocateAt(vid);

		}

		public int AppendVertex(NewVertexInfo info)
		{
			return AppendVertex(ref info);
		}

		/// <summary>
		/// copy vertex fromVID from existing source mesh, returns new vid
		/// </summary>
		public int AppendVertex(DMesh4 from, int fromVID)
		{

			int bi = 3 * fromVID;
			int vid = vertices_refcount.allocate();

			int i = 3 * vid;
			vertices.insert(from.vertices[bi + 2], i + 2);
			vertices.insert(from.vertices[bi + 1], i + 1);
			vertices.insert(from.vertices[bi], i);


			UnityEngine.Vector3 newVert = new UnityEngine.Vector3((float) from.vertices[bi],(float) from.vertices[bi + 1], (float) from.vertices[bi + 2]); 
			meshBounds.Encapsulate(newVert);


			if (normals != null)
			{
				if (from.normals != null)
				{
					normals.insert(from.normals[bi + 2], i + 2);
					normals.insert(from.normals[bi + 1], i + 1);
					normals.insert(from.normals[bi], i);
				}
				else
				{
					normals.insert(0, i + 2);
					normals.insert(1, i + 1);       // y-up
					normals.insert(0, i);
				}
			}

			if (colors != null)
			{
				if (from.colors != null)
				{
					colors.insert(from.colors[bi + 2], i + 2);
					colors.insert(from.colors[bi + 1], i + 1);
					colors.insert(from.colors[bi], i);
				}
				else
				{
					colors.insert(1, i + 2);
					colors.insert(1, i + 1);       // white
					colors.insert(1, i);
				}
			}

			if (uv != null)
			{
				int j = 2 * vid;
				if (from.uv != null)
				{
					int bj = 2 * fromVID;
					uv.insert(from.uv[bj + 1], j + 1);
					uv.insert(from.uv[bj], j);
				}
				else
				{
					uv.insert(0, j + 1);
					uv.insert(0, j);
				}
			}

			allocate_edges_list(vid);

			return vid;

		}

		/// <summary>
		/// insert vertex at given index, assuming that the index has already been allocated. 
		/// If bUnsafe, we use fast id allocation that does not update free list.
		/// You should only be using this between BeginUnsafeVerticesInsert() / EndUnsafeVerticesInsert() calls
		/// </summary>
		public MeshResult InsertVertex(int vid, ref NewVertexInfo info, bool bUnsafe = false)
		{

#if DMESH4_DEBUG
			UnityEngine.Debug.Assert(IsVertex(vid));
#endif

			int i = 3 * vid;
			vertices.insert(info.v[2], i + 2);
			vertices.insert(info.v[1], i + 1);
			vertices.insert(info.v[0], i);

			meshBounds.Encapsulate(new UnityEngine.Vector3((float)info.v[0], (float)info.v[1], (float)info.v[2]));

			if (normals != null)
			{
				Vector3f n = (info.bHaveN) ? info.n : Vector3f.AxisY;
				normals.insert(n[2], i + 2);
				normals.insert(n[1], i + 1);
				normals.insert(n[0], i);
			}

			if (colors != null)
			{
				Vector3f c = (info.bHaveC) ? info.c : Vector3f.One;
				colors.insert(c[2], i + 2);
				colors.insert(c[1], i + 1);
				colors.insert(c[0], i);
			}

			if (uv != null)
			{
				Vector2f u = (info.bHaveUV) ? info.uv : Vector2f.Zero;
				int j = 2 * vid;
				uv.insert(u[1], j + 1);
				uv.insert(u[0], j);
			}

			allocate_edges_list(vid);
			return MeshResult.Ok;

		}

		public MeshResult InsertVertex(int vid, NewVertexInfo info)
		{
			return InsertVertex(vid, ref info);
		}

		public int find_edge(int vA, int vB)
		{

			int vO = Math.Max(vA, vB);
			int vI = Math.Min(vA, vB);

			foreach (int eid in vertex_edges.ValueItr(vI))
			{
				if (edges[4 * eid + 1] == vO)
					return eid;
			}

			return InvalidID;

		}

		public virtual void BeginUnsafeVerticesInsert()
		{
			// do nothing...
		}

		public virtual void EndUnsafeVerticesInsert()
		{
			vertices_refcount.rebuild_free_list();
		}

		/// <summary>
		/// Creates a quad from 4 new vertices. ASSUMES that there will be no connected edges, only the primary face
		/// </summary>
		/// <returns>The new quad.</returns>
		/// <param name="v1">V1.</param>
		/// <param name="v2">V2.</param>
		/// <param name="v3">V3.</param>
		/// <param name="v4">V4.</param>
		public int AppendIsolatedQuad(Vector3d v1, Vector3d v2, Vector3d v3, Vector3d v4) {

			Index4i vt = Index4i.Zero;

			vt.a = AppendVertex(v1);
			vt.b = AppendVertex(v2);
			vt.c = AppendVertex(v3);
			vt.d = AppendVertex(v4);

			int tid = quads_refcount.allocate();

			if (tid == DMesh4.InvalidID) {
				return DMesh4.InvalidID;
			}

			if (MeshResult.Ok != InsertQuad(tid, vt, InvalidQuad))
			{
				return DMesh4.InvalidID;
			}

			return tid;
		}

		public void VerifyMeshEdges()
		{
			foreach(var edgeData in Edges())
			{
				UnityEngine.Debug.Assert(edgeData.a != -1, "first vert is null");
				UnityEngine.Debug.Assert(edgeData.b != -1, "second vert is null");
				UnityEngine.Debug.Assert(edgeData.c != -1, "primary face is null");
				UnityEngine.Debug.Assert(edgeData.c != -1, "secondary face is null");
			}
		}

		public int AppendQuad(int v0, int v1, int v2, int v3, int gid = -1)
		{
			return AppendQuad(new Index4i(v0, v1, v2, v3), gid);
		}

		/// <summary>
		/// Given a quad and an edge on the quad, return the quadID of the other quad touching that edge
		/// </summary>
		/// <param name="qID"></param>
		/// <param name="eID"></param>
		/// <returns></returns>
		public int AdjacentQuadID(int qID, int eID)
		{

			int primary = primaryFace(eID);
			int secondary = secondaryFace(eID);
						
			if (qID == primary)
			{
				return secondary;
			}
			
			return  primary;	

		}

		
		public bool SwapFaces(int eID, int oldQID, int newQID)
		{

			int primary = primaryFace(eID);
			int secondary = secondaryFace(eID);

			if (oldQID == primary)
			{
				SetPrimaryFace(eID, newQID);
				return true;
			} 

			if (oldQID == secondary)
			{
				SetSecondaryFace(eID, newQID);
				return true;
			}

			return false;

		}

		public int GetOppositeEdge(int qID, int eID)
		{

			int start, end;
			Index4i edges = GetQuadEdges(qID);
			Index2i vs = GetEdgeV(eID);

			for (int i = 0; i < 4; i++)
			{

				start = startVertex(edges[i], qID);
				end = endVertex(edges[i], qID);

				if (start != vs[0] && start != vs[1] && end != vs[0] && end != vs[1])
				{
					return edges[i];
				}

			}

			UnityEngine.Debug.Log("returning invalid opposite edge");
			return DMesh4.InvalidID;

		}

		public int AppendQuad(Index4i tv, int gid = -1)
		{

#if DMESH4_DEBUG

			for (int x = 0; x < 4; x++) {
				UnityEngine.Debug.Assert(IsVertex(tv[x]));
			}

			bool duplicate = (tv[0] == tv[1] || tv[0] == tv[2] || tv[0] == tv[3] || tv[1] == tv[2] || tv[1] == tv[3] || tv[2] == tv[3]);
			UnityEngine.Debug.Assert(!duplicate);

#endif


			// look up edges. if any already have two triangles, this would 
			// create non-manifold geometry and so we do not allow it
			int e0 = find_edge(tv[0], tv[1]);
			int e1 = find_edge(tv[1], tv[2]);
			int e2 = find_edge(tv[2], tv[3]);
			int e3 = find_edge(tv[3], tv[0]);


#if DMESH4_DEBUG

			//bool nonManifold = (e0 != InvalidID && IsBoundaryEdge(e0) == false)
			//	 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
			//	 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
			//	|| (e2 != InvalidID && IsBoundaryEdge(e2) == false);

			//UnityEngine.Debug.Assert(!nonManifold, "mesh must be manifold");

#endif

			// now safe to insert triangle
			int tid = quads_refcount.allocate();

			if (tid == DMesh4.InvalidID) {
				return DMesh4.InvalidID;
			}

			if (MeshResult.Ok != InsertQuad(tid, tv, new Index4i(e0, e1, e2, e3), gid)) {
				return DMesh4.InvalidID;

			}

			return tid;

		}

		public int InsertQuadRaw(Index4i vertexIDS, int groupID =- -1)
		{
			int qid = quads_refcount.allocate();

			if (qid == DMesh4.InvalidID)
			{
				return DMesh4.InvalidID;
			}

			int i = 4 * qid;
			quads.insert(vertexIDS[3], i + 3);
			quads.insert(vertexIDS[2], i + 2);
			quads.insert(vertexIDS[1], i + 1);
			quads.insert(vertexIDS[0], i);

			if (quad_groups != null)
			{
				quad_groups.insert(groupID, qid);
				max_group_id = Math.Max(max_group_id, groupID + 1);
			}

			return qid;

		}

		public MeshResult InsertQuad(int quadID, Index4i vertexIDS, Index4i edgeIDS, int groupID = -1, bool bUnsafe = false) {


			// insert vert ids into quad array
			int i = 4 * quadID;
			quads.insert(vertexIDS[3], i + 3);
			quads.insert(vertexIDS[2], i + 2);
			quads.insert(vertexIDS[1], i + 1);
			quads.insert(vertexIDS[0], i);


			// append quad group, if provided
			if (quad_groups != null)
			{
				quad_groups.insert(groupID, quadID);
				max_group_id = Math.Max(max_group_id, groupID + 1);
			}

			// increment ref counts for verts
			vertices_refcount.increment(vertexIDS[0]);
			vertices_refcount.increment(vertexIDS[1]);
			vertices_refcount.increment(vertexIDS[2]);
			vertices_refcount.increment(vertexIDS[3]);

			// create edges (if not already valid). Otherwise, set the new quad to be the secondary face of the edge
			add_quad_edge(quadID, vertexIDS[0], vertexIDS[1], 0, edgeIDS[0]);
			add_quad_edge(quadID, vertexIDS[1], vertexIDS[2], 1, edgeIDS[1]);
			add_quad_edge(quadID, vertexIDS[2], vertexIDS[3], 2, edgeIDS[2]);
			add_quad_edge(quadID, vertexIDS[3], vertexIDS[0], 3, edgeIDS[3]);

			return MeshResult.Ok;

		}


		/// <summary>
		/// Insert triangle at given index, assuming it has already been allocated
		/// If bUnsafe, we use fast id allocation that does not update free list.
		/// You should only be using this between BeginUnsafeTrianglesInsert() / EndUnsafeTrianglesInsert() calls
		/// Inserting the quad does not create new edges if there already is one between any two verts. 
		/// </summary>
		public MeshResult InsertQuad(int tid, Index4i tv, int gid = -1, bool bUnsafe = false)
		{

			// first we see if edges already exist between the vert ids provided. 
			int e0 = find_edge(tv[0], tv[1]);
			int e1 = find_edge(tv[1], tv[2]);
			int e2 = find_edge(tv[2], tv[3]);
			int e3 = find_edge(tv[3], tv[0]);

			// if adding this face would create 3 or more faces connected to one of the edges, we bail
			if ((e0 != InvalidID && IsBoundaryEdge(e0) == false)
				 || (e1 != InvalidID && IsBoundaryEdge(e1) == false)
				 || (e3 != InvalidID && IsBoundaryEdge(e3) == false)
				 || (e2 != InvalidID && IsBoundaryEdge(e2) == false))
			{
				return MeshResult.Failed_WouldCreateNonmanifoldEdge;
			}


			// insert the quad, now that we know the ids of the edges existing between the verts
			return InsertQuad(tid, tv, new Index4i(e0, e1, e2, e3), gid, bUnsafe);


		}


		public virtual void BeginUnsafeQuadsInsert()
		{
			// do nothing...
		}
		public virtual void EndUnsafeQuadsInsert()
		{
			quads_refcount.rebuild_free_list();
		}



		public void EnableVertexNormals(Vector3f initial_normal)
		{
			if (HasVertexNormals)
				return;
			normals = new DVector<float>();
			int NV = MaxVertexID;
			normals.resize(3 * NV);

			for (int i = 0; i < NV; ++i)
			{
				int vi = 3 * i;
				normals[vi] = initial_normal.x;
				normals[vi + 1] = initial_normal.y;
				normals[vi + 2] = initial_normal.z;
			}

		}
		public void DiscardVertexNormals()
		{
			normals = null;
		}

		public void EnableVertexColors(Vector3f initial_color)
		{
			if (HasVertexColors)
				return;
			colors = new DVector<float>();
			int NV = MaxVertexID;
			colors.resize(3 * NV);
			for (int i = 0; i < NV; ++i)
			{
				int vi = 3 * i;
				colors[vi] = initial_color.x;
				colors[vi + 1] = initial_color.y;
				colors[vi + 2] = initial_color.z;
			}
		}
		public void DiscardVertexColors()
		{
			colors = null;
		}

		public void EnableVertexUVs(Vector2f initial_uv)
		{
			if (HasVertexUVs)
				return;
			uv = new DVector<float>();
			int NV = MaxVertexID;
			uv.resize(2 * NV);
			for (int i = 0; i < NV; ++i)
			{
				int vi = 2 * i;
				uv[vi] = initial_uv.x;
				uv[vi + 1] = initial_uv.y;
			}
		}
		public void DiscardVertexUVs()
		{
			uv = null;
		}

		public void EnableQuadGroups(int initial_group = 0)
		{
			if (HasQuadGroups)
				return;
			quad_groups = new DVector<int>();
			int NT = MaxQuadID;
			quad_groups.resize(NT);
			for (int i = 0; i < NT; ++i)
				quad_groups[i] = initial_group;
			max_group_id = 0;
		}

		public void DiscardQuadGroups()
		{
			quad_groups = null;
			max_group_id = 0;
		}

		// iterators

		public IEnumerable<int> VertexIndices()
		{
			foreach (int vid in vertices_refcount)
				yield return vid;
		}

		public IEnumerable<int> QuadIndices()
		{
			foreach (int tid in quads_refcount)
				yield return tid;
		}

		public IEnumerable<int> EdgeIndices()
		{
			foreach (int eid in edges_refcount)
				yield return eid;
		}

		/// <summary>
		/// Allows going foreach(int eid in BoundaryEdgeIndices()) {  }
		/// </summary>
		public IEnumerable<int> BoundaryEdgeIndices()
		{
			foreach (int eid in edges_refcount)
			{
				if (IsBoundaryEdge(eid))
					yield return eid;
			}
		}

		/// <summary>
		/// Allows going foreach(int eid in BoundaryEdgeIndices()) {  }
		/// </summary>
		public IEnumerable<int> InteriorEdgeIndices()
		{
			foreach (int eid in edges_refcount)
			{
				if (!IsBoundaryEdge(eid))
					yield return eid;
			}
		}


		/// <summary>
		/// Enumerate vertices
		/// </summary>
		public IEnumerable<Vector3d> Vertices()
		{
			foreach (int vid in vertices_refcount)
			{
				int i = 3 * vid;
				yield return new Vector3d(vertices[i], vertices[i + 1], vertices[i + 2]);
			}
		}

		/// <summary>
		/// Enumerate triangles
		/// </summary>
		public IEnumerable<Index4i> Quads()
		{
			foreach (int tid in quads)
			{
				int i = 4 * tid;
				yield return new Index4i(quads[i], quads[i + 1], quads[i + 2], quads[i + 3]);
			}
		}

		/// <summary>
		/// Enumerage edges. return value is [v0,v1,t0,t1], where t1 will be InvalidID if this is a boundary edge
		/// </summary>
		public IEnumerable<Index4i> Edges()
		{
			foreach (int eid in edges_refcount)
			{
				int i = 4 * eid;
				yield return new Index4i(edges[i], edges[i + 1], edges[i + 2], edges[i + 3]);
			}
		}

		/// <summary>
		/// Find edgeid for edge [a,b]
		/// </summary>
		public int FindEdge(int vA, int vB)
		{
			return find_edge(vA, vB);
		}




		/// <summary>
		/// Find edgeid for edge [a,b] from triangle that contains the edge.
		/// This is faster than FindEdge() because it is constant-time
		/// </summary>
		public int FindEdgeFromQuad(int vA, int vB, int qID)
		{
			return find_edge_from_quad(vA, vB, qID);
		}

		/// <summary>
		/// If edge has vertices [a,b], and is connected two triangles [a,b,c] and [a,b,d],
		/// this returns [c,d], or [c,InvalidID] for a boundary edge
		/// </summary>
		public Index2i GetEdgeOpposingV(int eID)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Find triangle made up of any permutation of vertices [a,b,c, d]
		/// </summary>
		public int FindQuad(int a, int b, int c, int d)
		{

			throw new NotImplementedException();
		}

		/// <summary>
		/// Enumerate "other" vertices of edges connected to vertex (ie vertex one-ring)
		/// </summary>
		public IEnumerable<int> VtxVerticesItr(int vID)
		{
			if (vertices_refcount.isValid(vID))
			{
				foreach (int eid in vertex_edges.ValueItr(vID))
					yield return edge_other_v(eid, vID);
			}
		}

		/// <summary>
		/// Enumerate edge ids connected to vertex (ie edge one-ring)
		/// </summary>
		public IEnumerable<int> VtxEdgesItr(int vID)
		{
			if (vertices_refcount.isValid(vID))
			{
				return vertex_edges.ValueItr(vID);
			}
			return Enumerable.Empty<int>();
		}

		// <summary>
		/// Returns count of boundary edges at vertex, and 
		/// the first two boundary edges if found. 
		/// If return is > 2, call VtxAllBoundaryEdges
		/// </summary>
		public int VtxBoundaryEdges(int vID, ref int e0, ref int e1)
		{
			if (vertices_refcount.isValid(vID))
			{

				int count = 0;

				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int ei = 4 * eid;

					if (edges[ei + 3] == InvalidID)
					{
						if (count == 0)
							e0 = eid;

						else if (count == 1)
							e1 = eid;

						count++;

					}
				}
				return count;
			}
			UnityEngine.Debug.Assert(false);
			return -1;
		}

		/// <summary>
		/// Find edge ids of boundary edges connected to vertex.
		/// e needs to be large enough (ie call VtxBoundaryEdges, or as large as max one-ring)
		/// returns count, ie number of elements of e that were filled
		/// </summary>
		public int VtxAllBoundaryEdges(int vID, int[] e)
		{
			if (vertices_refcount.isValid(vID))
			{
				int count = 0;
				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int ei = 4 * eid;
					if (edges[ei + 3] == InvalidID)
						e[count++] = eid;
				}
				return count;
			}
			UnityEngine.Debug.Assert(false);
			return -1;
		}

		/// <summary>
		/// Get quad one-ring at vertex. 
		/// bUseOrientation is more efficient but returns incorrect result if vertex is a bowtie
		/// </summary>
		public MeshResult GetVtxQuads(int vID, List<int> vQuads, bool bUseOrientation)
		{


			if (bUseOrientation)
			{
				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int vOther = edge_other_v(eid, vID);
					int i = 4 * eid;
					int et0 = edges[i + 2];
					if (quad_has_sequential_v(et0, vID, vOther))
						vQuads.Add(et0);

					int et1 = edges[i + 3];

					if (et1 != InvalidID && quad_has_sequential_v(et1, vID, vOther))
						vQuads.Add(et1);
				}
			}
			else
			{
				// brute-force method
				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int i = 4 * eid;
					int t0 = edges[i + 2];
					if (vQuads.Contains(t0) == false)
						vQuads.Add(t0);
					int t1 = edges[i + 3];
					if (t1 != InvalidID && vQuads.Contains(t1) == false)
						vQuads.Add(t1);
				}
			}
			return MeshResult.Ok;
		}

		/// <summary>
		/// return # of triangles attached to vID, or -1 if invalid vertex
		/// if bBruteForce = true, explicitly checks, which creates a list and is expensive
		/// default is false, uses orientation, no memory allocation
		/// </summary>
		public int GetVtxQuadCount(int vID, bool bBruteForce = false)
		{
			if (bBruteForce)
			{
				List<int> vQuads = new List<int>();
				if (GetVtxQuads(vID, vQuads, false) != MeshResult.Ok)
					return -1;
				return vQuads.Count;
			}

			if (!IsVertex(vID))
				return -1;

			int N = 0;

			foreach (int eid in vertex_edges.ValueItr(vID))
			{
				int vOther = edge_other_v(eid, vID);
				int i = 4 * eid;
				int et0 = edges[i + 2];
				if (quad_has_sequential_v(et0, vID, vOther))
					N++;
				int et1 = edges[i + 3];
				if (et1 != InvalidID && quad_has_sequential_v(et1, vID, vOther))
					N++;
			}
			return N;
		}


		/// <summary>
		/// iterate over triangle IDs of vertex one-ring
		/// </summary>
		public IEnumerable<int> VtxQuadsItr(int vID)
		{
			if (IsVertex(vID))
			{
				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int vOther = edge_other_v(eid, vID);
					int i = 4 * eid;
					int et0 = edges[i + 2];
					if (quad_has_sequential_v(et0, vID, vOther))
						yield return et0;
					int et1 = edges[i + 3];
					if (et1 != InvalidID && quad_has_sequential_v(et1, vID, vOther))
						yield return et1;
				}
			}
		}

		/// <summary>
		///  from edge and vert, returns other vert, two opposing verts, and two quads
		/// </summary>
		public void GetQuadNbrhood(int eID, int vID, ref int vOther, ref int oppV1, ref int oppV2, ref int t1, ref int t2)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fastest possible one-ring centroid. This is used inside many other algorithms
		/// so it helps to have it be maximally efficient
		/// </summary>
		public void VtxOneRingCentroid(int vID, ref Vector3d centroid)
		{

			centroid = Vector3d.Zero;

			if (vertices_refcount.isValid(vID))
			{

				int n = 0;

				foreach (int eid in vertex_edges.ValueItr(vID))
				{
					int other_idx = 3 * edge_other_v(eid, vID);
					centroid.x += vertices[other_idx];
					centroid.y += vertices[other_idx + 1];
					centroid.z += vertices[other_idx + 2];
					n++;
				}
				if (n > 0)
				{
					double d = 1.0 / n;
					centroid.x *= d; centroid.y *= d; centroid.z *= d;
				}
			}
		}


		public bool IsBoundaryEdge(int eid)
		{
			return edges[4 * eid + 3] == InvalidID;
		}

		public bool quad_has_sequential_v(int tID, int vA, int vB)
		{
			int i = 4 * tID;
			int v0 = quads[i], v1 = quads[i + 1], v2 = quads[i + 2], v3 = quads[i + 3];
			if (v0 == vA && v1 == vB) return true;
			if (v1 == vA && v2 == vB) return true;
			if (v2 == vA && v3 == vB) return true;
			if (v3 == vA && v0 == vB) return true;
			return false;
		}

		/// <summary>
		/// returns the index of the edge on the quad sharing the two vert ids (or InvalidID)
		/// </summary>
		/// <returns>The edge from quad.</returns>
		/// <param name="vA">V a.</param>
		/// <param name="vB">V b.</param>
		/// <param name="tID">T identifier.</param>
		int find_edge_from_quad(int vA, int vB, int tID)
		{

			int i = 4 * tID;
			int t0 = quads[i],
				t1 = quads[i + 1];

			if (IndexUtil.same_pair_unordered(vA, vB, t0, t1))
				return quad_edges[i];

			int t2 = quads[i + 2];

			if (IndexUtil.same_pair_unordered(vA, vB, t1, t2))
				return quad_edges[i + 1];

			int t3 = quads[i + 3];

			if (IndexUtil.same_pair_unordered(vA, vB, t2, t3))
				return quad_edges[i + 2];

			if (IndexUtil.same_pair_unordered(vA, vB, t3, t0))
				return quad_edges[i + 3];

			return InvalidID;
		}

		public bool quad_has_vert(int tID, int vID)
		{
			int i = 4 * tID;
			return quads[i] == vID
				|| quads[i + 1] == vID
				|| quads[i + 2] == vID
				|| quads[i + 3] == vID;
		}

		public bool quad_is_boundary(int tID)
		{
			int i = 4 * tID;
			return IsBoundaryEdge(quad_edges[i])
				|| IsBoundaryEdge(quad_edges[i + 1])
				|| IsBoundaryEdge(quad_edges[i + 2])
				|| IsBoundaryEdge(quad_edges[i + 3]);
			;
		}

		public int startVertex(int eid, int qID = -1) {

			if (qID == -1)
				return edges[4 * eid];

			Index4i quad = GetQuad(qID);

			int a = edges[4 * eid], b = edges[4 * eid + 1];

			for (int i = 0; i < 4; i++)
			{

				if (quad[i] == a && quad[(i + 1) % 4] == b)
				{
					return a;
				}

				if (quad[i] == b && quad[(i + 1) % 4] == a)
				{
					return b;
				}

			}
			UnityEngine.Debug.Log("startVert");
			return -1;

		}

		public int endVertex(int eid, int qID = -1)
		{
	
			if (qID == -1)
			{
				return edges[4 * eid + 1];
			}
			
			Index4i quad = GetQuad(qID);
			int a = edges[4 * eid], b = edges[4 * eid + 1];

			for (int i = 0; i < 4; i++)
			{

				if (quad[i] == a && quad[(i + 1) % 4] == b)
				{
					return b;
				}

				if (quad[i] == b && quad[(i + 1) % 4] == a)
				{
					return a;
				}

			}
			UnityEngine.Debug.Log("endVert");
			return -1;

		}

		public int primaryFace(int eid) {
			return edges[4 * eid + 2];
		}

		public void SetPrimaryFace(int eid, int qid)
		{
			edges[4 * eid + 2] = qid;
		}

		public int secondaryFace(int eid)
		{
			return edges[4 * eid + 3];
		}

		public void SetSecondaryFace(int eid, int qid)
		{
			edges[4 * eid + 3] = qid;
		}

		public int edge_other_v(int eID, int vID)
		{
			int i = 4 * eID;
			int ev0 = edges[i], ev1 = edges[i + 1];
			return (ev0 == vID) ? ev1 : ((ev1 == vID) ? ev0 : InvalidID);
		}

		// check if the vertex is part of the edge
		public bool edge_has_vert(int eid, int vid)
		{
			int i = 4 * eid;
			return (edges[i] == vid) || (edges[i + 1] == vid);
		}


		// determines if the edge is shared by the quad
		public bool edge_has_quad(int eid, int tid)
		{
			int i = 4 * eid;
			return (edges[i + 2] == tid) || (edges[i + 3] == tid);
		}


		// gets the opposing quad from the edge (not tid)
		public int edge_other_quad(int eID, int tid)
		{
			int i = 4 * eID;
			int et0 = edges[i + 2], et1 = edges[i + 3];
			return (et0 == tid) ? et1 : ((et1 == tid) ? et0 : InvalidID);
		}

		/// <summary>
		/// Adds a new adge. Note that for any given edge e = (v1, v2, q1, q2)
		/// we ensure that v1 < v2
		/// </summary>
		/// <returns>The edge.</returns>
		/// <param name="vA">V a.</param>
		/// <param name="vB">V b.</param>
		/// <param name="tA">T a.</param>
		/// <param name="tB">T b.</param>
		public int add_edge(int vA, int vB, int tA, int tB = InvalidID)
		{
			// make sure vA < vB, so that the edge ids are ordered
			if (vB < vA)
			{
				int t = vB; vB = vA; vA = t;
			}

			int eid = edges_refcount.allocate();

			int i = 4 * eid;
			edges.insert(vA, i);
			edges.insert(vB, i + 1);
			edges.insert(tA, i + 2);
			edges.insert(tB, i + 3);

			vertex_edges.Insert(vA, eid);
			vertex_edges.Insert(vB, eid);

			return eid;

		}

		// helper fn for above, just makes code cleaner
		void add_quad_edge(int quadID, int vID0, int vID1, int j, int edgeID)
		{
			int otherQuadID = DMesh4.InvalidID;
			add_quad_edge(quadID, otherQuadID, vID0, vID1, j, edgeID);
		}

		void set_primary_face(int eid, int qID)
		{
			edges[4 * eid + 2] = qID;
		}

		void set_secondary_face(int eid, int qID)
		{
			edges[4 * eid + 3] = qID;		
		}


		/// <summary>
		///
		/// </summary>
		/// <param name="quadID"></param>
		/// <param name="otherQuadID"></param>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="j"></param>
		/// <param name="existingEdgeID"></param>
		void add_quad_edge(int quadID, int otherQuadID, int v0, int v1, int j, int existingEdgeID)
		{

			int newQuadEdgeID = 4 * quadID + j;

			// no existing edge already exists between the two verts, create it first
			if (existingEdgeID == InvalidID)
			{
				int newEdgeID = add_edge(v0, v1, quadID, otherQuadID);
				quad_edges.insert(newEdgeID, newQuadEdgeID);
			}
			else
			{
				set_secondary_face(existingEdgeID, quadID);
				quad_edges.insert(existingEdgeID, newQuadEdgeID);
			}

		}

		/// <summary>
		/// Adds a new edge
		/// </summary>
		/// <param name="faceIDS">the index of the primary and secondary faces of the new edge</param>
		/// <param name="vertIDs">indices of the start/end vertexs of the edge</param>
		/// <param name="edgeIndex">Which edge {0,1,2,3} of the new face we're adding.</param>
		/// <param name="existingEdgeID">Index of the edge already existing between the two verts. -1 if no such edge exists </param>
		void AddQuadEdge(Index2i faceIDS, Index2i vertIDs, int edgeIndex, int existingEdgeID)
		{
			add_quad_edge(faceIDS.a, faceIDS.b, vertIDs.a, vertIDs.b, edgeIndex, existingEdgeID);
		}

		// check if the two quads are neighbors
		public bool quad_has_neighbour_quad(int tCheck, int tNbr)
		{

			int i = 4 * tCheck;

			return edge_has_quad(quad_edges[i], tNbr)
				|| edge_has_quad(quad_edges[i + 1], tNbr)
				|| edge_has_quad(quad_edges[i + 2], tNbr)
				|| edge_has_quad(quad_edges[i + 3], tNbr)
				;
		}

		public bool IsBoundaryVertex(int vID)
		{
			foreach (int e in vertex_edges.ValueItr(vID))
			{
				if (secondaryFace(e) == InvalidID)
					return true;
			}

			return false;

		}

		public bool IsBoundaryQuad(int tID)
		{
			int i = 4 * tID;
			return IsBoundaryEdge(quad_edges[i]) || IsBoundaryEdge(quad_edges[i + 1]) || IsBoundaryEdge(quad_edges[i + 3]) || IsBoundaryEdge(quad_edges[i + 2]);
		}


		/// <summary>
		/// Returns true if the two quads connected to edge have different group IDs
		/// </summary>
		public bool IsGroupBoundaryEdge(int eID)
		{


			int et1 = secondaryFace(eID);

			if (et1 == InvalidID)
				return false;

			int g1 = quad_groups[et1];
			int et0 = primaryFace(eID);

			int g0 = quad_groups[et0];
			return g1 != g0;

		}

		/// <summary>
		/// returns true if vertex has more than one quad group in its quad nbrhood
		/// </summary>
		public bool IsGroupBoundaryVertex(int vID)
		{


			int group_id = int.MinValue;

			foreach (int eID in vertex_edges.ValueItr(vID))
			{
				int et0 = primaryFace(eID);
				int g0 = quad_groups[et0];

				if (group_id != g0)
				{
					if (group_id == int.MinValue)
						group_id = g0;
					else
						return true;        // saw multiple group IDs
				}
				int et1 = secondaryFace(eID);

				if (et1 != InvalidID)
				{
					int g1 = quad_edges[et1];
					if (group_id != g1)
						return true;        // saw multiple group IDs
				}
			}
			return false;
		}

		#region useless triangle IMesh stuff

		public Index3i GetTriangle(int i)
		{
			throw new NotImplementedException();
		}

		public int GetTriangleGroup(int i)
		{
			throw new NotImplementedException();
		}

		public bool IsTriangle(int tID)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<int> TriangleIndices()
		{
			throw new NotImplementedException();
		}

		public int TriangleCount
		{
			get
			{
				return 0;
			}
		}

		public int MaxTriangleID { get { return DMesh4.InvalidID; } }
		public bool HasTriangleGroups { get { return false; } }

		#endregion

		#region compact data

		/// <summary> returns true if vertices, edges, and triangles are all "dense" (Count == MaxID) </summary>
		public bool IsCompact
		{
			get { return vertices_refcount.is_dense && edges_refcount.is_dense && quads_refcount.is_dense; }
		}

		/// <summary> Returns true if vertex count == max vertex id </summary>
		public bool IsCompactV
		{
			get { return vertices_refcount.is_dense; }
		}

		/// <summary> returns true if triangle count == max triangle id </summary>
		public bool IsCompactT
		{
			get { return quads_refcount.is_dense; }
		}

		/// <summary> returns measure of compactness in range [0,1], where 1 is fully compacted </summary>
		public double CompactMetric
		{
			get { return ((double)VertexCount / (double)MaxVertexID + (double)QuadCount / (double)MaxQuadID) * 0.5; }
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="faceID"></param>
		/// <param name="t"></param>
		/// <param name="startingEdge"> should be 0, 1, 2 or 3 </param>
		/// <returns></returns>
		public MeshResult SplitFace(int faceID, float t, int startingEdge)
		{

			int eID1 = GetQuadEdge(faceID, startingEdge);
			int eID2 = GetQuadEdge(faceID, (startingEdge + 2) % 4);
			int v0 = startVertex(eID1, faceID), v1 = endVertex(eID1, faceID), v2 = startVertex(eID2, faceID), v3 = endVertex(eID2, faceID);
			var newPoint1 = GetEdgePoint(eID1, t);
			var newPoint2 = GetEdgePoint(eID2, 1 - t);

			int newV1 = AppendVertex(newPoint1);
			int newV2 = AppendVertex(newPoint2);
			AppendQuad(v0, newV1, newV2, v3);
			AppendQuad(newV1, v1, v2, newV2);

			RemoveQuad(faceID);

			return MeshResult.Ok;

		}

		/// <summary>
		/// Remove vertex vID, and all connected triangles if bRemoveAllTriangles = true
		/// (if false, them throws exception if there are still any triangles!)
		/// if bPreserveManifold, checks that we will not create a bowtie vertex first
		/// </summary>
		public MeshResult RemoveVertex(int vID, bool bRemoveAllQuads = true, bool bPreserveManifold = false)
		{

			if (vertices_refcount.isValid(vID) == false)
			{
				return MeshResult.Failed_NotAVertex;
			}

			if (bRemoveAllQuads)
			{

				// if any one-ring vtx is a boundary vtx and one of its outer-ring edges is an
				// interior edge then we will create a bowtie if we remove that triangle
				if (bPreserveManifold)
				{
					foreach (int tid in VtxQuadsItr(vID))
					{

						Index4i quad = GetQuad(tid);
						int j = IndexUtil.find_quad_index(vID, ref quad);
						int oa = quad[(j + 1) % 4], ob = quad[(j + 2) % 4], oc = quad[(j + 3) % 4];
						int eid = find_edge(oa, ob);

						if (IsBoundaryEdge(eid))
						{
							continue;
						}

						if (IsBoundaryVertex(oa) || IsBoundaryVertex(ob))
						{
							return MeshResult.Failed_WouldCreateBowtie;
						}

					}
				}

				List<int> tris = new List<int>();
				GetVtxQuads(vID, tris, true);

				foreach (int tID in tris)
				{
					MeshResult result = RemoveQuad(tID, false, bPreserveManifold);
					if (result != MeshResult.Ok)
						return result;
				}

			}

			if (vertices_refcount.refCount(vID) != 1)
				throw new NotImplementedException("DMesh3.RemoveVertex: vertex is still referenced");

			vertices_refcount.decrement(vID);
			vertex_edges.Clear(vID);
			return MeshResult.Ok;

		}

		



		#region REMOVING TOPOLOGY

		/// <summary>
		/// Remove a tID from the mesh. Also removes any unreferenced edges after tri is removed.
		/// If bRemoveIsolatedVertices is false, then if you remove all tris from a vert, that vert is also removed.
		/// If bPreserveManifold, we check that you will not create a bowtie vertex (and return false).
		///   If this check is not done, you have to make sure you don't create a bowtie, because other
		///   code assumes we don't have bowties, and will not handle it properly
		/// </summary>
		public MeshResult RemoveQuad(int quadToRemoveID, bool bRemoveIsolatedVertices = true, bool bPreserveManifold = false)
		{

			if (!quads_refcount.isValid(quadToRemoveID))
			{
				return MeshResult.Failed_NotATriangle;
			}

			Index4i tv = GetQuad(quadToRemoveID);
			Index4i te = GetQuadEdges(quadToRemoveID);


			// Remove triangle from its edges. if edge has no triangles left,
			// then it is removed.
			for (int j = 0; j < 4; ++j)
			{
				// index of edge
				int edgeID = te[j];

				replace_edge_quad(edgeID, quadToRemoveID, InvalidID);

				if (primaryFace(edgeID) == InvalidID)
				{
					vertex_edges.Remove(startVertex(edgeID), edgeID);
					vertex_edges.Remove(endVertex(edgeID), edgeID);
					edges_refcount.decrement(edgeID);
				}

			}

			// free this triangle
			quads_refcount.decrement(quadToRemoveID);

			// Decrement vertex refcounts. If any hit 1 and we got remove-isolated flag,
			// we need to remove that vertex
			for (int j = 0; j < 4; ++j)
			{

				int vid = tv[j];
				vertices_refcount.decrement(vid);

				if (bRemoveIsolatedVertices && vertices_refcount.refCount(vid) == 1)
				{
					vertices_refcount.decrement(vid);
					vertex_edges.Clear(vid);
				}

			}

			return MeshResult.Ok;

		}



		int replace_edge_quad(int edgeID, int oldFaceID, int newFaceID)
		{

			int i = 4 * edgeID;
			int primaryFaceID = edges[i + 2], secondaryFaceID = edges[i + 3];

			if (primaryFaceID == oldFaceID)
			{
				if (newFaceID == InvalidID)
				{		
					edges[i + 2] = secondaryFaceID;
					edges[i + 3] = InvalidID;
				}
				else
				{
					edges[i + 2] = newFaceID;
				}

				return 0;

			}
			else if (secondaryFaceID == oldFaceID)
			{

				edges[i + 3] = newFaceID;
				return 1;
			}
			else
				return -1;
		}

		#endregion

	}



}

