﻿using UnityEngine;
using g3;
using System.Collections.Generic;

public class QuadMeshTest : MonoBehaviour {

	public JQuadMeshFilter filter;
	public ComputeShader shader;
	public ActiveGameObject activeGO;

	public void CreateQuad(DMesh4 m, int resolution, Vector3d center, Vector3d right, Vector3d up, Dictionary<Vector3d, int> posInd, bool normalized = false)
	{

		float step = 1.0f / (float) resolution;
		int[,] verts = new int[resolution + 1, resolution + 1];

		for (int i = 0; i <= resolution; i++)
		{
			for (int j = 0; j <= resolution; j++)
			{
				float a = i * step - 0.5f;
				float b = j * step - 0.5f;
				Vector3d pos =  center + a * right + b * up;

				if (normalized)
				{
					pos.Normalize();
				}

				if (posInd.ContainsKey(pos))
				{
					verts[i, j] = posInd[pos];
				} else
				{
					verts[i, j] = m.AppendVertex(pos);
					posInd.Add(pos, verts[i, j]);
				}

			}
		}

		for (int i = 0; i < resolution; i++)
		{
			for (int j = 0; j < resolution; j++)
			{
				Index4i quadVerts = new Index4i(verts[i, j], verts[i, j + 1], verts[i + 1, j + 1], verts[i + 1, j]);
				m.AppendQuad(quadVerts);
			}
		}

	}
	public void CreateCube(DMesh4 qMesh, int res)
	{
		Dictionary<Vector3d, int> positionIndices = new Dictionary<Vector3d, int>();

		g3.Vector3d up = Vector3.up;
		g3.Vector3d right = Vector3.right, left = Vector3.left;
		g3.Vector3d forward = Vector3.forward, back = Vector3.back;

		CreateQuad(qMesh, res, 0.5f * right, forward, up, positionIndices);
		CreateQuad(qMesh, res, -0.5f * right, -forward, up, positionIndices);
		CreateQuad(qMesh, res, 0.5f * forward, -right, up, positionIndices);
		CreateQuad(qMesh, res, -0.5f * forward, right, up, positionIndices);
		CreateQuad(qMesh, res, 0.5f * up, right, forward, positionIndices);
		CreateQuad(qMesh, res, -0.5f * up, -right, forward, positionIndices);

	

	}
	public void CreateSphere(DMesh4 qMesh, int res)
	{
		Dictionary<Vector3d, int> positionIndices = new Dictionary<Vector3d, int>();
		CreateQuad(qMesh, res, new Vector3d(0.5, 0, 0), new Vector3d(0, 0, 1), new Vector3(0, 1, 0),   positionIndices, true);
		CreateQuad(qMesh, res, new Vector3d(-0.5, 0, 0), new Vector3d(0, 0, -1), new Vector3(0, 1, 0), positionIndices, true);
		CreateQuad(qMesh, res, new Vector3d(0, 0.5, 0), new Vector3d(1, 0, 0), new Vector3(0, 0, 1),   positionIndices, true);
		CreateQuad(qMesh, res, new Vector3d(0, -0.5, 0), new Vector3d(-1, 0, 0), new Vector3(0, 0, 1), positionIndices, true);
		CreateQuad(qMesh, res, new Vector3d(0, 0, -0.5), new Vector3d(1, 0, 0), new Vector3(0, 1, 0),  positionIndices, true);
		CreateQuad(qMesh, res, new Vector3d(0, 0, 0.5), new Vector3d(-1, 0, 0), new Vector3(0, 1, 0),  positionIndices, true);
	}

	public int resolution = 2;

	void Awake()
	{

		DMesh4 qMesh = new DMesh4();
		CreateCube(qMesh, resolution);
		JQuadMesh jQMesh = new JQuadMesh(qMesh);
		jQMesh.gpuMesh.updateMeshShader = shader;
		filter.SetMesh(jQMesh);

		var octree = filter.gameObject.AddComponent<MeshOctree>();
		octree.filter = filter;
		jQMesh.octree = octree;

		var meshTools = filter.gameObject.GetComponents<MeshTool>();

		foreach(var tool in meshTools)
		{
			tool.SetMesh(jQMesh);
		}

		activeGO.SetActiveObject(filter.gameObject);

		jQMesh.ActivateBoundaryQuads();

	}

}
