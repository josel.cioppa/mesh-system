﻿using UnityEngine;
using System.Collections.Generic;
using System;

public struct QuadUpdate
{
	public int a, b, c, d;
	public int index;

	public static QuadUpdate FromIndices(g3.Index4i vIDS, int fID)
	{
		return new QuadUpdate()
		{
			a = vIDS.a,
			b = vIDS.b,
			c = vIDS.c,
			d = vIDS.d,
			index = fID
		};
	}
}

public struct EdgeUpdate
{

	public int v1, v2, f1, f2;
	public int index;

	// NOTE[josel] we could get this from somethign like dMesh.GetEdge(eID)
	public static EdgeUpdate FromEdgeData(g3.Index4i edgeData, int eID)
	{
		return new EdgeUpdate()
		{
			v1 = edgeData.a,
			v2 = edgeData.b,
			f1 = edgeData.c,
			f2 = edgeData.d,
			index = eID
		};
	}

}


public class GPUQuadMesh
{

	public struct Vertex
	{
		public Vector3 position;
		public Vector3 normal;
		public Vector3 color;
		public int flags;
	}

	public struct Quad
	{
		public int a;
		public int b;
		public int c;
		public int d;
		public int flags;
	}

	public struct Edge
	{

		// the two vert indices of the edge (unordered???)
		public int vidA;
		public int vidB;

		// the two connected face indices of the edge (primary/secondary) 
		public int fidA;
		public int fidB;

		// edge specific flags
		public int flags;

	}

	public struct QuadFlags
	{
		public static int ACTIVE_FLAG   = 0x00000001;	// should the quad be rendered
		public static int SELECTED_FLAG = 0x00000002;   // should it be highlighted
		public static int BOUNDARY_FLAG = 0x00000004;   // quad is a boundary quad (i.e has some neighboring quad which is null)
	}

	public static int FLOATS_PER_VERTEX = 9;
	public static int INTS_PER_QUAD = 5;
	public static int INTS_PER_EDGE = 5;

	public int maxVertices = 500000;
	public int maxQuads = 500000;

	public int numQuads;
	public int numVertices;

	public int maxVertIndex = 0;
	public int maxQuadIndex = 0;

	public int QuadCount
	{
		get
		{
			return numQuads;
		}
	}

	public ComputeBuffer quadData;
	public ComputeBuffer vertexData;
	public ComputeShader updateMeshShader;
	public static int MAX_NEW_QUAD_APPEND = 1000;
	static int MAX_VERT_UPDATES = 1000;
	ComputeBuffer quadUpdateBuffer = null;
	ComputeBuffer vertUpdateBuffer = null;
	Dictionary<int, bool> kernelsInitialize = new Dictionary<int, bool>();
	ComputeBuffer quadUpdateIDS = null;

	public GPUQuadMesh()
	{

		vertexData = new ComputeBuffer(maxVertices, FLOATS_PER_VERTEX * sizeof(float) + sizeof(int));
		quadData = new ComputeBuffer(maxQuads, INTS_PER_QUAD * sizeof(int));


		quadUpdateBuffer = new ComputeBuffer(MAX_NEW_QUAD_APPEND, 4 * sizeof(int));
		vertUpdateBuffer = new ComputeBuffer(MAX_VERT_UPDATES, 9 * sizeof(float) + sizeof(int));

	}

	public GPUQuadMesh(g3.DMesh4 dmesh)
	{

		var quads = dmesh.quads;
		Vertex[] vertices = new Vertex[dmesh.VertexCount];
		Quad[] triangleDatum = new Quad[quads.Length / 4];

		numVertices = vertices.Length;
		numQuads = quads.Length / 4;

		for (int i = 0; i < numVertices; i++)
		{
			vertices[i] = new Vertex()
			{
				position = new Vector3((float)dmesh.vertices[3 * i], (float)dmesh.vertices[3 * i + 1], (float)dmesh.vertices[3 * i + 2]),
				normal = new Vector3((float)dmesh.normals[3 * i], (float)dmesh.normals[3 * i + 1], (float)dmesh.normals[3 * i + 2]),
				color = new Vector3(1.0f, 1.0f, 1.0f),
				flags = 0x00000001
			};
		}

		for (int i = 0; i < numQuads; i++)
		{
			triangleDatum[i] = new Quad()
			{
				a = quads[0 + 4 * i],
				b = quads[1 + 4 * i],
				c = quads[2 + 4 * i],
				d = quads[3 + 4 * i],
				flags = QuadFlags.ACTIVE_FLAG
			};
		}

		SetMeshData(vertices, triangleDatum);

	}

	private void SetMeshData(Vertex[] vertices, Quad[] quadDatum)
	{

		quadData = new ComputeBuffer(maxQuads, sizeof(int) * INTS_PER_QUAD);
		quadData.SetData(quadDatum, 0, 0, quadDatum.Length);

		vertexData = new ComputeBuffer(maxVertices, FLOATS_PER_VERTEX * sizeof(float) + sizeof(int));
		vertexData.SetData(vertices, 0, 0, vertices.Length);

		quadUpdateBuffer = new ComputeBuffer(MAX_NEW_QUAD_APPEND, 5 * sizeof(int));
		vertUpdateBuffer = new ComputeBuffer(MAX_VERT_UPDATES, 9 * sizeof(float) + sizeof(int));
		quadUpdateIDS = new ComputeBuffer(MAX_NEW_QUAD_APPEND, sizeof(int));

	}

	/// <summary>
	/// This takes a list of vertex updates, and updates the GPU vertex data. Assumes these are NOT new vertices, so doesn't increase number of vertices in the list
	/// </summary>
	/// <param name="newVertices"></param>
	internal void PushVertexUpdates(VertexUpdate[] newVertices)
	{

		if (newVertices.Length >= MAX_VERT_UPDATES)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateVertices");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "vertexData", vertexData);
			updateMeshShader.SetBuffer(kernel, "vertexUpdates", vertUpdateBuffer);
			kernelsInitialize.Add(kernel, true);
		}

		vertUpdateBuffer.SetData(newVertices, 0, 0, newVertices.Length);
		updateMeshShader.Dispatch(kernel, newVertices.Length, 1, 1);

	}


	/// <summary>
	/// updates vertex data on GPU and increases number of vertex count
	/// </summary>
	/// <param name="newVertices"></param>
	internal void PushNewVertices(VertexUpdate[] newVertices)
	{
		PushVertexUpdates(newVertices);
		numVertices += newVertices.Length;
	}


	internal void HighlightQuads(int[] quadIDS)
	{

		if (quadIDS.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateQuadFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdateIDS", quadUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", true);
		updateMeshShader.SetInt("flagToUpdate", 0x00000002);
		quadUpdateIDS.SetData(quadIDS, 0, 0, quadIDS.Length);
		updateMeshShader.Dispatch(kernel, quadIDS.Length, 1, 1);

	}

	internal void SetBoundaryQuads(int[] quadIDS)
	{

		if (quadIDS.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateQuadFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdateIDS", quadUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", true);
		updateMeshShader.SetInt("flagToUpdate", 0x00000004);
		quadUpdateIDS.SetData(quadIDS, 0, 0, quadIDS.Length);
		updateMeshShader.Dispatch(kernel, quadIDS.Length, 1, 1);

	}
	internal void DisableBoundaryQuads(int[] quadIDS)
	{

		if (quadIDS.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateQuadFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdateIDS", quadUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", false);
		updateMeshShader.SetInt("flagToUpdate", 0x00000004);
		quadUpdateIDS.SetData(quadIDS, 0, 0, quadIDS.Length);
		updateMeshShader.Dispatch(kernel, quadIDS.Length, 1, 1);

	}

	internal void ClearQuadHighlight(int[] quadIDS)
	{
		if (quadIDS.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("exceeded max vertex update limit");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateQuadFlags");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdateIDS", quadUpdateIDS);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		updateMeshShader.SetBool("enableFlag", false);
		updateMeshShader.SetInt("flagToUpdate", 0x00000002);
		quadUpdateIDS.SetData(quadIDS, 0, 0, quadIDS.Length);
		updateMeshShader.Dispatch(kernel, quadIDS.Length, 1, 1);

	}

	/// <summary>
	/// Updates triangles on GPU. Does NOT increase number of triangles, assumes we're merely updating an active triangle
	/// </summary>
	/// <param name="newQuads"></param>
	internal void PushQuadUpdates(QuadUpdate[] newQuads)
	{

		if (newQuads.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("Exceeded max quad append count");
			return;
		}

		int kernel = updateMeshShader.FindKernel("UpdateQuads");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdates", quadUpdateBuffer);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		quadUpdateBuffer.SetData(newQuads, 0, 0, newQuads.Length);
		updateMeshShader.Dispatch(kernel, newQuads.Length, 1, 1);

	}


	/// <summary>
	/// Updates triangle data, and increases number of triangles counter
	/// </summary>
	/// <param name="newQuads"></param>
	internal void PushNewQuads(QuadUpdate[] newQuads)
	{
		PushQuadUpdates(newQuads);
		numQuads += newQuads.Length;
	}


	internal void PushRemoveQuads(QuadUpdate[] quadsToRemove)
	{
		if (quadsToRemove.Length >= MAX_NEW_QUAD_APPEND)
		{
			Debug.LogError("Exceeded max triangle append count");
			return;
		}

		int kernel = updateMeshShader.FindKernel("RemoveQuads");

		if (!kernelsInitialize.ContainsKey(kernel))
		{
			updateMeshShader.SetBuffer(kernel, "quadUpdates", quadUpdateBuffer);
			updateMeshShader.SetBuffer(kernel, "quadData", quadData);
			kernelsInitialize.Add(kernel, true);
		}

		quadUpdateBuffer.SetData(quadsToRemove, 0, 0, quadsToRemove.Length);
		updateMeshShader.Dispatch(kernel, quadsToRemove.Length, 1, 1);
	}


	public void Destroy()
	{

		// free buffers holding mesh data
		if (quadData != null) quadData.Release();
		if (vertexData != null) vertexData.Release();

		// free buffers used to push updates to the GPU
		if (quadUpdateBuffer != null) quadUpdateBuffer.Release();
		if (vertUpdateBuffer != null) vertUpdateBuffer.Release();

	}

	internal void ClearEdgeHighlights(int[] highlightedEdges)
	{
		throw new NotImplementedException();
	}

	internal void HighlightEdges(int[] highlightedEdges)
	{
		throw new NotImplementedException();
	}
}