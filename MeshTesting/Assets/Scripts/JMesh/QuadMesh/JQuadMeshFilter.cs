﻿using UnityEngine;
using g3;
using System.Threading;


public class JQuadMeshFilter : MonoBehaviour
{

	public JQuadMesh mesh;

	public void SetShaderVariables(Material material)
	{
		material.SetBuffer("quadData", mesh.gpuMesh.quadData);
		material.SetBuffer("vertexData", mesh.gpuMesh.vertexData);
	}

	public void SetMesh(JQuadMesh jmesh)
	{
		this.mesh = jmesh;
	}

	public void OnDestroy()
	{
		mesh.gpuMesh.Destroy();
	}

}
