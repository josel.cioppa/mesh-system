﻿using System;
using System.Collections.Generic;
using g3;
using UnityEngine;

public class JQuadMeshEditor {

	public DQuadMesh qMesh;
	public JQuadMesh jMesh;

	public JQuadMeshEditor(DQuadMesh qm, JQuadMesh jm)
	{
		qMesh = qm;
		jMesh = jm;
	}
	
	int AddFace(Index4i vids)
	{

		int fid = qMesh.AppendQuad(vids);

		if (fid == DMesh3.InvalidID)
		{

		}

		Index4i quadVerts = default(Index4i);
		VertexUpdate[] vertUpdates = new VertexUpdate[4];

		qMesh.GetQuadVertexIndices(fid, ref quadVerts);
		QuadUpdate[] quadUpdates = new QuadUpdate[1] { QuadUpdate.FromIndices(quadVerts, fid) };
		jMesh.gpuMesh.PushNewQuads(quadUpdates);

		vertUpdates[0] = VertexUpdate.FromVertexInfo(vids[0], qMesh.GetVertexAll(vids[0]));
		vertUpdates[1] = VertexUpdate.FromVertexInfo(vids[1], qMesh.GetVertexAll(vids[1]));
		vertUpdates[2] = VertexUpdate.FromVertexInfo(vids[2], qMesh.GetVertexAll(vids[2]));
		vertUpdates[3] = VertexUpdate.FromVertexInfo(vids[3], qMesh.GetVertexAll(vids[3]));

		jMesh.gpuMesh.PushNewVertices(vertUpdates);

		return fid;

	}

	// add a face from vertex ids and adjacent face ids. 
	int AddFace(Index4i vids, Index4i fids)
	{

		int fid = qMesh.AddQuad(vids, fids);

		if (fid == DMesh3.InvalidID)
		{

		}

		Index4i quadVerts = default(Index4i);
		VertexUpdate[] vertUpdates = new VertexUpdate[4];

		qMesh.GetQuadVertexIndices(fid, ref quadVerts);
		QuadUpdate[] quadUpdates = new QuadUpdate[1] { QuadUpdate.FromIndices(quadVerts, fid) };
		jMesh.gpuMesh.PushNewQuads(quadUpdates);

		// push vertex updates, should we do this, or assume its already been done on create?
		vertUpdates[0] = VertexUpdate.FromVertexInfo(vids[0], qMesh.GetVertexAll(vids[0]));
		vertUpdates[1] = VertexUpdate.FromVertexInfo(vids[1], qMesh.GetVertexAll(vids[1]));
		vertUpdates[2] = VertexUpdate.FromVertexInfo(vids[2], qMesh.GetVertexAll(vids[2]));
		vertUpdates[3] = VertexUpdate.FromVertexInfo(vids[3], qMesh.GetVertexAll(vids[3]));
		jMesh.gpuMesh.PushNewVertices(vertUpdates);


		return fid;

	}

	public void CreateGrid(int nX, int nY, Vector2 gridScale, int gid = -1)
	{
		int[,] vids = new int[nX + 1, nY + 1];
		int[,] fids = new int[nX, nY];

		VertexUpdate[] vUpdates = new VertexUpdate[(nX + 1) * (nY + 1)];
		QuadUpdate[] qUpdate = new QuadUpdate[nX * nY];
		Vector3d vert = default(Vector3d);
		
		for (int i = 0; i <= nX; i++)
		{
			for (int j = 0; j <= nY; j++)
			{			
				vert.Set((double)(i * gridScale.x), 0, (double) (j * gridScale.y));
				int id = qMesh.AppendVertex(vert);

				if (id == -1)
				{

				}

				vids[i, j] = id;
				vUpdates[i + j * (nX + 1)] = VertexUpdate.FromVertexInfo(id, qMesh.GetVertexAll(id));
			}
		}

		// allocate the face indices
		for (int i = 0; i < nX; i++)
		{
			for (int j = 0; j < nY; j++)
			{

				int id = qMesh.quad_refCount.allocate();

				if (id == -1)
				{
					return;
				}


				fids[i, j] = id;

			}
		}

		// allocate the face indices
		for (int i = 0; i < nX; i++)
		{
			for (int j = 0; j < nY; j++)
			{

				Index4i vid = new Index4i(
					vids[i,j],
					vids[i, j+1],
					vids[i+1, j+1],
					vids[i+1, j]
				);


				Index4i fid = new Index4i(
					i - 1 >= 0 ?  fids[i - 1, j] : -1,
					j + 1 < nY ?  fids[i, j+1] : -1,
					i + 1 < nX && j + 1 < nY ? fids[i+1, j+1] : -1,
					j - 1 >= 0 ? fids[i, j - 1] : -1
				);


				MeshResult result = qMesh.InsertQuad(fids[i, j], vid, fid, gid);

				if (result != MeshResult.Ok)
				{

				} else
				{
					qMesh.quad_refCount.increment(fids[i,j]);
					qUpdate[i + j * nX] = QuadUpdate.FromIndices(vid, fids[i, j]);
				}

			}
		}

		jMesh.gpuMesh.PushNewVertices(vUpdates);
		jMesh.gpuMesh.PushNewQuads(qUpdate);

	}


	int ExtrudeFace(int qID)
	{
		return -1;
	}

	int InsetFace(int qID)
	{
		return -1;
	}

}

public class JQuadMesh
{

	public GPUQuadMesh gpuMesh;
	public DMesh4 dMesh;
	public MeshOctree octree;

	public int QuadCount
	{
		get
		{
			if (gpuMesh == null) return 0;
			return gpuMesh.QuadCount;
		}

	}

	public int MaxQuadID
	{
		get
		{
			return dMesh == null ? -1 : dMesh.MaxQuadID;
		}
	}

	public int MaxVertexID
	{
		get
		{
			return dMesh == null ? -1 : dMesh.MaxVertexID;
		}
	}

	public JQuadMesh(g3.DMesh4 dmesh)
	{
		dMesh = dmesh;
		gpuMesh = new GPUQuadMesh(dmesh);
	}

	public JQuadMesh(g3.DMesh4 dmesh, GPUQuadMesh gmesh)
	{
		dMesh = dmesh;
		gpuMesh = gmesh;	
	}

	public void PushNewVertices(VertexUpdate[] newVertices, bool rebuild = false)
	{
		gpuMesh.PushNewVertices(newVertices);		
	}

	internal void PushVertexUpdates(VertexUpdate[] m_vertexUpdates, bool rebuildSpatial = false)
	{
		gpuMesh.PushVertexUpdates(m_vertexUpdates);
	}

	public void PushNewQuads(QuadUpdate[] newTriangles, bool rebuildSpatial = false)
	{
		gpuMesh.PushNewQuads(newTriangles);
	}

	internal void PushQuadUpdates(QuadUpdate[] newTriangles, bool rebuildSpatial = false)
	{
		gpuMesh.PushQuadUpdates(newTriangles);
		
	}

	internal void PushRemoveQuads(QuadUpdate[] trianglesToRemove, bool rebuildSpatial)
	{
		gpuMesh.PushRemoveQuads(trianglesToRemove);
	}

	internal void HighlightQuads(int[] tids)
	{
		gpuMesh.HighlightQuads(tids);
	}

	internal void ClearQuadHighlights(int[] tids)
	{
		gpuMesh.ClearQuadHighlight(tids);
	}

	internal MeshResult RemoveFace(int faceID)
	{
		QuadUpdate[] removedQuads = new QuadUpdate[1] {
			QuadUpdate.FromIndices(dMesh.GetQuadEdges(faceID), faceID)
		};

		var result = dMesh.RemoveQuad(faceID);

		if (result == MeshResult.Ok)
		{
			gpuMesh.PushRemoveQuads(removedQuads);
		}

		return result;
	}

	internal MeshResult SplitFace(int faceID, float t, int startingEdgeID, int endingEdgeID, ref int oldVertID)
	{

		t = 0.5f;

		int eID1 = startingEdgeID;
		int eID2 = endingEdgeID;

		int v0 = dMesh.startVertex(eID1, faceID), v1 = dMesh.endVertex(eID1, faceID), 
			v2 = dMesh.startVertex(eID2, faceID), v3 = dMesh.endVertex(eID2, faceID);

		QuadUpdate[] removedQuads = new QuadUpdate[1] {
			QuadUpdate.FromIndices(dMesh.GetQuad(faceID), faceID)
		};

		var newPoint1 = dMesh.GetEdgePoint(eID1, t);
		var newPoint2 = dMesh.GetEdgePoint(eID2, 1 - t);

		int newV1;

		if (oldVertID == -1)
		{
			// NOTE(josel) : this is a hack, it should be newpoint1 but I did this to make it work
			newV1 = dMesh.AppendVertex(dMesh.GetEdgePoint(eID1, 1 - t));
		}
		else
		{
			newV1 = oldVertID;
		}


		if (newV1 < 0)
		{
			Debug.LogError("failed to append vert");
			return MeshResult.Failed_BrokenTopology;
		}

		int newV2 = dMesh.AppendVertex(newPoint2);

		if (newV2 < 0)
		{
			Debug.LogError("failed to append vert");
			return MeshResult.Failed_BrokenTopology;
		}

		oldVertID = newV2;
	
		// create the quad
		int q1;			
		q1 = dMesh.AppendQuad(v0, newV1, newV2, v3);

		if (q1 < 0)
		{
			Debug.LogError("failed to append quad");
			return MeshResult.Failed_BrokenTopology;
		}

		int q2;
		q2 = dMesh.AppendQuad(newV1, v1, v2, newV2);
		
		if (q2 < 0)
		{
			Debug.LogError("failed to append quad");
			return MeshResult.Failed_BrokenTopology;
		}

		if (dMesh.RemoveQuad(faceID) != MeshResult.Ok)
		{
			Debug.LogError("failed to remove quad");
			return MeshResult.Failed_BrokenTopology;
		}

		VertexUpdate[] newVerts = new VertexUpdate[2] {
			VertexUpdate.FromVertexInfo(newV1, dMesh.GetVertexAll(newV1)),
			VertexUpdate.FromVertexInfo(newV2, dMesh.GetVertexAll(newV2))
		};

		QuadUpdate[] newQuads = new QuadUpdate[2] {
			QuadUpdate.FromIndices(dMesh.GetQuad(q1), q1),
			QuadUpdate.FromIndices(dMesh.GetQuad(q2), q2)
		};

		PushNewQuads(newQuads);
		PushNewVertices(newVerts);
		PushRemoveQuads(removedQuads, false);
			
		return MeshResult.Ok;

	}

	internal void ActivateBoundaryQuads()
	{

		var boundaryQuads = new List<int>();

		foreach(var qID in dMesh.QuadIndices())
		{
			if (dMesh.IsBoundaryQuad(qID))
			{
				boundaryQuads.Add(qID);
			}
		}

		if (boundaryQuads.Count == 0)
		{
			return;
		}

		gpuMesh.SetBoundaryQuads(boundaryQuads.ToArray());

	}

	internal void ClearEdgeHighlights(int[] highlightedEdges)
	{
		gpuMesh.ClearEdgeHighlights(highlightedEdges);
	}

	internal void HighlightEdges(int[] highlightedEdges)
	{
		gpuMesh.HighlightEdges(highlightedEdges);
	}



}
