﻿using UnityEngine;
using UnityEngine.Rendering;

public class JQuadMeshRenderer : MonoBehaviour
{

	public Material quadMat;
	public Material wireMat;
	public Material pointMat;
	public bool m_renderQuads, m_renderWireframe, m_renderPoints;
	public JQuadMeshFilter meshFilter;

	public CommandBuffer renderingBuffer;
	public Camera cam;

	public void Start()
	{
		renderingBuffer = new CommandBuffer();
		cam.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, renderingBuffer);
	}
	
	private void OnRenderObject()
	{

		int trisToRender = meshFilter.mesh.MaxQuadID + 1;
		renderingBuffer.Clear();

		meshFilter.SetShaderVariables(quadMat);
		renderingBuffer.DrawProcedural(transform.localToWorldMatrix, quadMat, 0, MeshTopology.Points, trisToRender);

		//meshFilter.SetShaderVariables(wireMat);
		//renderingBuffer.DrawProcedural(transform.localToWorldMatrix, wireMat, 0, MeshTopology.Points, trisToRender);
	

	}

}

