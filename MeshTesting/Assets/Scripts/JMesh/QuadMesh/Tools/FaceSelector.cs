﻿using System.Collections.Generic;

public class FaceSelector : MeshTool {

	public PlayerData player;
	public HashSet<int> currentlySelectedFaces = new HashSet<int>();
	public List<int> selectedFaces = new List<int>();

	public void OnStartSelectingFaces()
	{
		if (selectedFaces.Count > 0)
		{
			mesh.ClearQuadHighlights(selectedFaces.ToArray());
		}

		selectedFaces.Clear();
		currentlySelectedFaces.Clear();
	}


	public void OnSelectFaces()
	{

		int qID = mesh.octree.ClosestQuadID(transform.InverseTransformPoint(player.rightHand.position));

		if (!currentlySelectedFaces.Contains(qID))
		{
			UpdateSelectedFaces(qID);
		}

	}

	public void OnFinishSelectingFaces()
	{
		
	}

	private void UpdateSelectedFaces(int qID)
	{
		currentlySelectedFaces.Add(qID);
		selectedFaces.Add(qID);
		mesh.HighlightQuads(selectedFaces.ToArray());
	}

}
