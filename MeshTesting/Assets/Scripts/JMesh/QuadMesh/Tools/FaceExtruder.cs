﻿using UnityEngine;


public abstract class MeshTool : MonoBehaviour
{
	public JQuadMesh mesh;

	public void SetMesh(JQuadMesh m)
	{
		this.mesh = m;
	}
}

public class FaceExtruder : MeshTool {

	public PlayerData playerData;
	int qID;
	g3.Index4i newQuadVerts;
	Vector3 originalHandPos;
	g3.NewVertexInfo[] originalVertexData = new g3.NewVertexInfo[4];
	VertexUpdate[] vertUpdates = new VertexUpdate[4];

	Vector3 faceNormal;

	public void StartExtruding()
	{

		originalHandPos = transform.InverseTransformPoint(playerData.rightHand.position);
		int closestQuad =  mesh.octree.ClosestQuadID(transform.InverseTransformPoint(playerData.rightHand.position));

		qID = -1;
		newQuadVerts = g3.DMesh4.InvalidQuad;

		if (closestQuad >= 0)
		{

			faceNormal = (Vector3) mesh.dMesh.GetQuadNormal(closestQuad);

			qID = ExecuteExtrudeFace(closestQuad);

			newQuadVerts = mesh.dMesh.GetQuadVertices(qID);

			originalVertexData[0] = mesh.dMesh.GetVertexAll(newQuadVerts.a);
			originalVertexData[1] = mesh.dMesh.GetVertexAll(newQuadVerts.b);
			originalVertexData[2] = mesh.dMesh.GetVertexAll(newQuadVerts.c);
			originalVertexData[3] = mesh.dMesh.GetVertexAll(newQuadVerts.d);

		}

	}

	public void ExtrudeFace()
	{

		// direction vector from where we clicked to where we are now
		var dir = transform.InverseTransformPoint(playerData.rightHand.position) - originalHandPos;

		// orient in the direction of the face normal
		dir = dir.magnitude * faceNormal;

		int id = newQuadVerts.a;
		var vertInfo = originalVertexData[0];
		mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
		vertUpdates[0].position = (Vector3)vertInfo.v + dir * 0.1f;
		vertUpdates[0].normal = vertInfo.n;
		vertUpdates[0].color = vertInfo.c;
		vertUpdates[0].vID = id;

		id = newQuadVerts.b;
		vertInfo = originalVertexData[1];
		mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
		vertUpdates[1].position = (Vector3)vertInfo.v + dir * 0.1f;
		vertUpdates[1].normal = vertInfo.n;
		vertUpdates[1].color = vertInfo.c;
		vertUpdates[1].vID = id;

		id = newQuadVerts.c;
		vertInfo = originalVertexData[2];
		mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
		vertUpdates[2].position = (Vector3)vertInfo.v + dir * 0.1f;
		vertUpdates[2].normal = vertInfo.n;
		vertUpdates[2].color = vertInfo.c;
		vertUpdates[2].vID = id;

		id = newQuadVerts.d;
		vertInfo = originalVertexData[3];
		mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
		vertUpdates[3].position = (Vector3)vertInfo.v + dir * 0.1f;
		vertUpdates[3].normal = vertInfo.n;
		vertUpdates[3].color = vertInfo.c;
		vertUpdates[3].vID = id;

		mesh.PushVertexUpdates(vertUpdates);

	}

	private int ExecuteExtrudeFace(int faceID)
	{

		int newQuad = mesh.dMesh.DuplicateFace(faceID);

		g3.MeshResult removedQuad =  mesh.dMesh.RemoveQuad(faceID);

		if (removedQuad != g3.MeshResult.Ok)
		{
			Debug.LogError("something went wrong in removing a quad");
			return g3.DMesh4.InvalidID;
		}

		mesh.PushRemoveQuads(new QuadUpdate[]
		{
			new QuadUpdate() {  index = faceID }
		}, false);
	
		g3.Index4i oldVerts = mesh.dMesh.GetQuadVertices(faceID);
		g3.Index4i newVerts = mesh.dMesh.GetQuadVertices(newQuad);

		int f1 = mesh.dMesh.AppendQuad(oldVerts.a, newVerts.a, newVerts.d, oldVerts.d);
		int f2 = mesh.dMesh.AppendQuad(oldVerts.d, newVerts.d, newVerts.c, oldVerts.c);
		int f3 = mesh.dMesh.AppendQuad(oldVerts.c, newVerts.c, newVerts.b, oldVerts.b);
		int f4 = mesh.dMesh.AppendQuad(oldVerts.b, newVerts.b, newVerts.a, oldVerts.a);


		VertexUpdate[] newVertUpdates = new VertexUpdate[4]
		{
			VertexUpdate.FromVertexInfo(newVerts.a, mesh.dMesh.GetVertexAll(newVerts.a)),
			VertexUpdate.FromVertexInfo(newVerts.b, mesh.dMesh.GetVertexAll(newVerts.b)),
			VertexUpdate.FromVertexInfo(newVerts.c, mesh.dMesh.GetVertexAll(newVerts.c)),
			VertexUpdate.FromVertexInfo(newVerts.d, mesh.dMesh.GetVertexAll(newVerts.d))
		};
		QuadUpdate[] newQuadUpdates = new QuadUpdate[5]
		{
			QuadUpdate.FromIndices(newVerts, newQuad),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuadVertices(f1), f1),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuadVertices(f2), f2),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuadVertices(f3), f3),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuadVertices(f4), f4)
		};

		mesh.PushNewVertices(newVertUpdates);
		mesh.PushNewQuads(newQuadUpdates);

		return newQuad;

	}

	private int[] ExecuteExtrudeFaces(int[] faceIDS)
	{

		int[] newFaceIDS = new int[faceIDS.Length];

		for (int i = 0; i < faceIDS.Length; i++)
		{
			newFaceIDS[i] = ExecuteExtrudeFace(faceIDS[i]);
		}

		return newFaceIDS;

	}

	public void ExtrudeAllFaces(int[] faceIDS, g3.NewVertexInfo[] originalVerts)
	{

		var dir = transform.InverseTransformPoint(playerData.rightHand.position) - originalHandPos;

		for (int i = 0; i < faceIDS.Length; i++)
		{

			int index = 4 * i;
			g3.Index4i quadVerts = mesh.dMesh.GetQuadVertices(faceIDS[i]);

			int id = quadVerts.a;
			var vertInfo = originalVerts[index];
			mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
			vertUpdates[0].position = (Vector3)vertInfo.v + dir * 0.1f;
			vertUpdates[0].normal = vertInfo.n;
			vertUpdates[0].color = vertInfo.c;
			vertUpdates[0].vID = id;

			id = quadVerts.b;
			vertInfo = originalVerts[index + 1];
			mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
			vertUpdates[1].position = (Vector3)vertInfo.v + dir * 0.1f;
			vertUpdates[1].normal = vertInfo.n;
			vertUpdates[1].color = vertInfo.c;
			vertUpdates[1].vID = id;

			id = quadVerts.c;
			vertInfo = originalVerts[index + 2];
			mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
			vertUpdates[2].position = (Vector3)vertInfo.v + dir * 0.1f;
			vertUpdates[2].normal = vertInfo.n;
			vertUpdates[2].color = vertInfo.c;
			vertUpdates[2].vID = id;

			id = quadVerts.d;
			vertInfo = originalVerts[index + 3];
			mesh.dMesh.SetVertex(id, vertInfo.v + dir * 0.25f);
			vertUpdates[3].position = (Vector3)vertInfo.v + dir * 0.1f;
			vertUpdates[3].normal = vertInfo.n;
			vertUpdates[3].color = vertInfo.c;
			vertUpdates[3].vID = id;

			mesh.PushVertexUpdates(vertUpdates);
		}
	}

	public void FinishExtruding()
	{
		mesh.octree.Rebuild();
	}

}
