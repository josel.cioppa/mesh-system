﻿using System.Collections.Generic;
using UnityEngine;
using g3;
using System.Linq;

public class EdgeCutter : MeshTool {

	List<Vector3> points = null;
	public PlayerData playerData;
	EdgeLoopData data;
	public LineRenderer lRenderer;
	Vector3 originalHandPos;
	int lastSelectedFace = -1;
	int lastEdgeID = -1;

	float distAlongEdge;
	Vector3 originalPosition;
	Vector3 originalNormal;
	bool executingEdgeCut = false;

	public void Awake()
	{
			
	}

	public void Update()
	{

		if (executingEdgeCut == false)
		{

			int qid = mesh.octree.ClosestQuadID(transform.InverseTransformPoint(playerData.rightHand.position));

			if (qid < 0)
			{
				return;
			}

			var _up = playerData.rightHand.up;
			int edgeID = 0;

			if (_up.y < 0.1f)
			{
				edgeID = 1;
			}

			if (lastSelectedFace == qid && lastEdgeID == edgeID)
			{
				return;
			}

			data = GetEdgeLoopFull(qid, mesh.dMesh.GetQuadEdge(qid, edgeID));

			lRenderer.SetColors(Color.magenta, Color.magenta);
			lRenderer.SetWidth(0.0025f, 0.0025f);

			lastEdgeID = edgeID;
			lastSelectedFace = qid;

		}

		DisplayEdgeCut();	

	}



	public void BeginEdgeCut()
	{
		executingEdgeCut = true;
	}

	private void DisplayEdgeCut()
	{

		if (data.edgePoints == null || data.edgePoints.Count <= 0)
		{
			return;
		}

		distAlongEdge = Vector3.Dot(originalNormal, playerData.rightHand.position - originalPosition);
		distAlongEdge += 1.0f;
		distAlongEdge *= 0.5f;
		distAlongEdge = Mathf.Clamp01(distAlongEdge);

		var pointsToRender = data.edgePoints.Select(verts => Vector3.Lerp((Vector3) mesh.dMesh.GetVertex(verts.Key), (Vector3) mesh.dMesh.GetVertex(verts.Value), distAlongEdge)  ).ToArray();
		lRenderer.SetVertexCount(data.edgePoints.Count);
		lRenderer.SetPositions(pointsToRender);
	}

	public void ExecuteEdgeLoopCut()
	{

		var quads = data.quadLoop;
		var edges = data.edgeLoop;

		int oldVertID = -1;

		for (int i = 0; i < quads.Count; i++)
		{
			int quadID = quads[i];
			int edgeID2 = edges[i];
			int edgeID = mesh.dMesh.GetOppositeEdge(quadID, edgeID2);

			if (mesh.SplitFace(quads[i], distAlongEdge, edgeID, edgeID2, ref oldVertID) != MeshResult.Ok)
			{
				Debug.LogError("failed to split face");
			}	
		}

		mesh.octree.Rebuild();
		executingEdgeCut = false;
		mesh.ActivateBoundaryQuads();

	}

	public struct FaceConstructionData
	{
		public int oldQID;
		public int v0, v1, v2;
	}

	public struct EdgeConstructionData
	{
		public int leftFaceID, rightFaceID;
	}

	public struct QuadEdgeData
	{
		public int e0, e1, e2, e3, e4;
	}

	public void CutEdgeLoop()
	{

		var quads = data.quadLoop;
		var edges = data.edgeLoop;

		List<FaceConstructionData> newFaceData = new List<FaceConstructionData>();

		VertexUpdate[] updates = new VertexUpdate[quads.Count];

		// add all of the vertices along the edge loop
		for (int i = 0; i < quads.Count; i++)
		{

			var edgeID = edges[i];
			var quadID = quads[i];
			int newVert = mesh.dMesh.AppendVertex(mesh.dMesh.GetEdgePoint(edgeID, quadID, distAlongEdge));

			updates[i] = VertexUpdate.FromVertexInfo(newVert, mesh.dMesh.GetVertexAll(newVert));

			newFaceData.Add(new FaceConstructionData()
			{
				v0 = mesh.dMesh.startVertex(edgeID, quadID),
				v1 = newVert,
				v2 = mesh.dMesh.endVertex(edgeID, quadID),
				oldQID = quadID
			});

		}

		mesh.gpuMesh.PushNewVertices(updates);


		int N = newFaceData.Count;
		QuadUpdate[] quadUpdates = new QuadUpdate[2 * N];

		List<EdgeConstructionData> edgeConstructData = new List<EdgeConstructionData>();

		// insert the two new quads, but no edge data
		for (int i = 0; i < newFaceData.Count; i++)
		{

			int curr = i;
			int next = (i + 1) % N;

			int rQ = mesh.dMesh.InsertQuadRaw(new Index4i(
				newFaceData[curr].v0, 
				newFaceData[curr].v1, 
				newFaceData[next].v1,
				newFaceData[next].v0
			));

			quadUpdates[2 * i] = QuadUpdate.FromIndices(mesh.dMesh.GetQuad(rQ), rQ);

			mesh.dMesh.vertices_refcount.increment(newFaceData[curr].v0);
			mesh.dMesh.vertices_refcount.increment(newFaceData[curr].v1);
			mesh.dMesh.vertices_refcount.increment(newFaceData[next].v1);
			mesh.dMesh.vertices_refcount.increment(newFaceData[next].v0);

			int lQ = mesh.dMesh.InsertQuadRaw(new Index4i(
				newFaceData[curr].v1,
				newFaceData[curr].v2,
				newFaceData[next].v2,
				newFaceData[next].v1
			));

			quadUpdates[2 * i + 1] = QuadUpdate.FromIndices(mesh.dMesh.GetQuad(lQ), lQ);

			mesh.dMesh.vertices_refcount.increment(newFaceData[curr].v1);
			mesh.dMesh.vertices_refcount.increment(newFaceData[curr].v2);
			mesh.dMesh.vertices_refcount.increment(newFaceData[next].v2);
			mesh.dMesh.vertices_refcount.increment(newFaceData[next].v1);

			edgeConstructData.Add(new EdgeConstructionData()
			{
				leftFaceID = lQ,
				rightFaceID = rQ
			});

		}

		mesh.gpuMesh.PushNewQuads(quadUpdates);

		List<QuadEdgeData> qeData = new List<QuadEdgeData>();

		// here we set up the edge data
		N = edgeConstructData.Count;

		for (int i = 0; i < N; i++)
		{

			int curr = i;
			int next = (i + 1) % N;
			int prev = ((i - 1) + N) % N;

			int e0 = mesh.dMesh.add_edge(newFaceData[curr].v0, newFaceData[curr].v1, edgeConstructData[curr].rightFaceID, edgeConstructData[prev].rightFaceID);
			int e1 = mesh.dMesh.add_edge(newFaceData[curr].v1, newFaceData[curr].v2, edgeConstructData[curr].leftFaceID, edgeConstructData[prev].leftFaceID);
			int e2 = mesh.dMesh.add_edge(newFaceData[curr].v1, newFaceData[next].v1, edgeConstructData[curr].rightFaceID, edgeConstructData[curr].leftFaceID);

			mesh.dMesh.SwapFaces(
				mesh.dMesh.find_edge(newFaceData[curr].v0, newFaceData[next].v0),
				newFaceData[curr].oldQID,
				edgeConstructData[curr].rightFaceID
			);

			mesh.dMesh.SwapFaces(
				mesh.dMesh.find_edge(newFaceData[curr].v2, newFaceData[next].v2),
				newFaceData[curr].oldQID,
				edgeConstructData[curr].leftFaceID
			);

			qeData.Add(new QuadEdgeData() {
				e0 = e0,
				e1 = e1, 
				e2 = mesh.dMesh.find_edge(newFaceData[curr].v2, newFaceData[next].v2),
				e3 = e2,
				e4 = mesh.dMesh.find_edge(newFaceData[curr].v0, newFaceData[next].v0)
			});

		}

		// set quad edges for all the newly created quads

		N = qeData.Count;

		for (int i = 0; i < N; i++)
		{

			int curr = i;
			int next = (i + 1) % N;
			int prev = ((i - 1) + N) % N;

			// right face of quad
			int qID = edgeConstructData[curr].rightFaceID;
			mesh.dMesh.quad_edges.insert(qeData[curr].e0, 4 * qID + 0);
			mesh.dMesh.quad_edges.insert(qeData[curr].e3, 4 * qID + 1);
			mesh.dMesh.quad_edges.insert(qeData[next].e0, 4 * qID + 2);
			mesh.dMesh.quad_edges.insert(qeData[curr].e4, 4 * qID + 3);

			// left face of quad
			qID = edgeConstructData[curr].leftFaceID;
			mesh.dMesh.quad_edges.insert(qeData[curr].e1, 4 * qID + 0);
			mesh.dMesh.quad_edges.insert(qeData[curr].e2, 4 * qID + 1);
			mesh.dMesh.quad_edges.insert(qeData[next].e1, 4 * qID + 2);
			mesh.dMesh.quad_edges.insert(qeData[curr].e3, 4 * qID + 3);

		}

		QuadUpdate[] removeQuads = new QuadUpdate[quads.Count];

		// remove all of the initial quads
		for (int i = 0; i < quads.Count; i++)
		{

			var quadToRemoveID = quads[i];
			Index4i tv = mesh.dMesh.GetQuad(quadToRemoveID);
			Index4i te = mesh.dMesh.GetQuadEdges(quadToRemoveID);

			for (int j = 0; j < 4; ++j)
			{
			
				// index of edge
				int edgeID = te[j];

				if (mesh.dMesh.primaryFace(edgeID) == g3.DMesh4.InvalidID)
				{
					mesh.dMesh.vertex_edges.Remove(mesh.dMesh.startVertex(edgeID), edgeID);
					mesh.dMesh.vertex_edges.Remove(mesh.dMesh.endVertex(edgeID), edgeID);
					mesh.dMesh.edges_refcount.decrement(edgeID);
				}

			}

			// free this triangle
			mesh.dMesh.quads_refcount.decrement(quadToRemoveID);

			// Decrement vertex refcounts. If any hit 1 and we got remove-isolated flag,
			// we need to remove that vertex
			for (int j = 0; j < 4; ++j)
			{
				int vid = tv[j];
				mesh.dMesh.vertices_refcount.decrement(vid);	
			}

			removeQuads[i] = QuadUpdate.FromIndices(mesh.dMesh.GetQuad(quadToRemoveID), quadToRemoveID);

		}

		mesh.gpuMesh.PushRemoveQuads(removeQuads);

		mesh.octree.Rebuild();
		executingEdgeCut = false;
		mesh.ActivateBoundaryQuads();

	}

	public struct EdgeLoopData
	{
		public List<int> quadLoop;
		public List<int> edgeLoop;
		public List<KeyValuePair<int, int>> edgePoints;
	}

	public EdgeLoopData GetEdgeLoopFull(int initialQuadID, int initialEdge)
	{

		EdgeLoopData data = new EdgeLoopData();
		data.quadLoop = new List<int>();
		data.edgeLoop = new List<int>();
		data.edgePoints = new List<KeyValuePair<int, int>>();

		var mesh = this.mesh.dMesh;

		data.quadLoop.Add(initialQuadID);
		data.edgeLoop.Add(initialEdge);

		var entry = new KeyValuePair<int, int>(mesh.startVertex(initialEdge, initialQuadID), mesh.endVertex(initialEdge, initialQuadID));
		data.edgePoints.Add(entry);

		originalPosition = transform.TransformPoint((Vector3) mesh.GetEdgePoint(initialEdge, 0.5f));
		originalNormal = (originalPosition -  transform.TransformPoint((Vector3) mesh.GetVertex(mesh.startVertex(initialEdge, initialQuadID)))).normalized;

		int counter = 0;
		int searchLimit = 100;
		int nextQuadID = -1;

		nextQuadID = mesh.AdjacentQuadID(initialQuadID, initialEdge);

		if (nextQuadID == -1)
		{
			return data;
		}

		int lastEdgeID = initialEdge;
		int currentEdgeID;

		while (nextQuadID != initialQuadID && counter < searchLimit)
		{

			currentEdgeID = mesh.GetOppositeEdge(nextQuadID, lastEdgeID);

			// unable to get the next edge of the quad
			if (currentEdgeID == -1)
			{
				return data;
			}

			data.quadLoop.Add(nextQuadID);
			data.edgeLoop.Add(currentEdgeID);
			data.edgePoints.Add(new KeyValuePair<int, int>(mesh.startVertex(currentEdgeID, nextQuadID), mesh.endVertex(currentEdgeID, nextQuadID)));

			nextQuadID = mesh.AdjacentQuadID(nextQuadID, currentEdgeID);

			if (nextQuadID == -1)
			{
				return data;
			}

			counter++;
			lastEdgeID = currentEdgeID;

		}


		data.edgePoints.Add(new KeyValuePair<int, int>(mesh.startVertex(initialEdge, initialQuadID), mesh.endVertex(initialEdge, initialQuadID)));

		return data;

	}



}
