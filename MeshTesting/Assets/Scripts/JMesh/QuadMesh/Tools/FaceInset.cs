﻿public class FaceInset : MeshTool {

	public PlayerData player;

	public void InsetFace()
	{
		int qID = mesh.octree.ClosestQuadID(transform.InverseTransformPoint(player.rightHand.position));

		if (qID > 0)
		{
			InsetFace(qID);
		}
	}
	
	public void OnFinishInsetting()
	{
		mesh.octree.Rebuild();
	}

	public int InsetFace(int qID, float insetAmount = 0.5f)
	{

		var centerPoint = mesh.dMesh.GetPointInQuad(qID, 0.5f * g3.Vector2d.One);
		var vertIDS = mesh.dMesh.GetQuad(qID);

		int v0 = mesh.dMesh.AppendVertex(centerPoint + insetAmount * (mesh.dMesh.GetVertex(vertIDS.a) - centerPoint));
		int v1 = mesh.dMesh.AppendVertex(centerPoint + insetAmount * (mesh.dMesh.GetVertex(vertIDS.b) - centerPoint));
		int v2 = mesh.dMesh.AppendVertex(centerPoint + insetAmount * (mesh.dMesh.GetVertex(vertIDS.c) - centerPoint));
		int v3 = mesh.dMesh.AppendVertex(centerPoint + insetAmount * (mesh.dMesh.GetVertex(vertIDS.d) - centerPoint));

		int q0 = mesh.dMesh.AppendQuad(v0, v1, v2, v3);
		int q1 = mesh.dMesh.AppendQuad(v0, vertIDS.a, vertIDS.b, v1);
		int q2 = mesh.dMesh.AppendQuad(v1, vertIDS.b, vertIDS.c, v2);
		int q3 = mesh.dMesh.AppendQuad(v2, vertIDS.c, vertIDS.d, v3);
		int q4 = mesh.dMesh.AppendQuad(v3, vertIDS.d, vertIDS.a, v0);

		mesh.PushNewQuads(new QuadUpdate[]
		{
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(q0), q0),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(q1), q1),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(q2), q2),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(q3), q3),
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(q4), q4)
		});

		mesh.PushNewVertices(new VertexUpdate[]
		{
			VertexUpdate.FromVertexInfo(v0, mesh.dMesh.GetVertexAll(v0)),
			VertexUpdate.FromVertexInfo(v1, mesh.dMesh.GetVertexAll(v1)),
			VertexUpdate.FromVertexInfo(v2, mesh.dMesh.GetVertexAll(v2)),
			VertexUpdate.FromVertexInfo(v3, mesh.dMesh.GetVertexAll(v3))
		});

		mesh.dMesh.RemoveQuad(qID);

		mesh.PushRemoveQuads(new QuadUpdate[]
		{
			QuadUpdate.FromIndices(mesh.dMesh.GetQuad(qID), qID)
		}, false);

		return q0;

	}

}
