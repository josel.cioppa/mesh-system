﻿using UnityEngine;
using g3;
using System.Threading;


public class JMeshFilter : MonoBehaviour {

    public JMesh mesh;
    public MeshTopology meshTopology = MeshTopology.Triangles;

    public void SetShaderVariables(Material material) {
        material.SetBuffer("triangleData",  mesh.gpuMesh.triangleData);
        material.SetBuffer("vertexData", mesh.gpuMesh.vertexData);
    } 

	public void SetMesh(JMesh jmesh)
	{
		this.mesh = jmesh;
	}

    public void OnDestroy()
    {
        mesh.gpuMesh.Destroy();
    }

}
