﻿using g3;
using UnityEngine;

public class JMesh {

    public GPUMesh gpuMesh;
    public DMesh3 dMesh;
    public DMeshAABBTree3 spatial;
    public MeshEditor editor;

	public int TriangleCount
	{
		get
		{
			if (gpuMesh == null) return 0;
			return gpuMesh.TriangleCount;
		}

	}

	public int MaxTriangleID
	{
		get
		{
			return dMesh == null ? -1 : dMesh.MaxTriangleID;
		}
	}

	public int MaxVertexID
	{
		get
		{
			return dMesh == null ? -1 : dMesh.MaxVertexID;
		}
	}

    public JMesh(g3.DMesh3 dmesh)
    {
        dMesh = dmesh;
		gpuMesh = new GPUMesh(dmesh);
        spatial = new DMeshAABBTree3(dmesh);
        spatial.Build();
        editor = new MeshEditor(dMesh);
	}

	public JMesh(g3.DMesh3 dmesh, GPUMesh gmesh)
	{
		dMesh = dmesh;
		gpuMesh = gmesh;
		spatial = new DMeshAABBTree3(dmesh);
		spatial.Build();
		editor = new g3.MeshEditor(dMesh);
	}
		
	public void PushNewVertices(VertexUpdate[] newVertices, bool rebuild = false)
	{
		gpuMesh.PushNewVertices(newVertices);
		if (rebuild)
		{
			spatial.Build();
		}	
	}

	
	internal void PushVertexUpdates(VertexUpdate[] m_vertexUpdates, bool rebuildSpatial = false)
	{
		gpuMesh.PushVertexUpdates(m_vertexUpdates);
		if (rebuildSpatial)
		{
			this.spatial.Build();
		}
	}


	/// <summary>
	/// Adds a list of new triangles to the GPU mesh, and optionally rebuilds the spatial tree
	/// </summary>
	/// <param name="newTriangles"></param>
	/// <param name="rebuildSpatial"></param>
	public void PushNewTriangles(TriangleUpdate[] newTriangles, bool rebuildSpatial = false)
	{
		gpuMesh.PushNewTriangles(newTriangles);
		if (rebuildSpatial)
		{
			spatial.Build();
		}
	}

	internal void PushTriangleUpdates(TriangleUpdate[] newTriangles, bool rebuildSpatial = false)
	{
		gpuMesh.PushTriangleUpdates(newTriangles);
		if (rebuildSpatial)
		{
			spatial.Build();
		}
	}

	internal void PushRemoveTriangles(TriangleUpdate[] trianglesToRemove, bool rebuildSpatial)
	{
		gpuMesh.PushRemoveTriangles(trianglesToRemove);
		if (rebuildSpatial)
		{
			spatial.Build();
		}
	}

	internal void RebuildSpatialTree()
	{
		spatial.Build();
	}

	internal void SubvidideTriangle(int tID)
	{

		var triangle = dMesh.GetTriangle(tID);

		int va = triangle.a, vb = triangle.b, vc = triangle.c;

		Vector3d 
			a = dMesh.GetVertex(triangle.a),
			b = dMesh.GetVertex(triangle.b),
			c = dMesh.GetVertex(triangle.c);


		// add 3 new verts and push changes to GPU
		int vA = dMesh.AppendVertex(a + 0.5f * (b - a));
		int vB = dMesh.AppendVertex(b + 0.5f * (c - b));
		int vC = dMesh.AppendVertex(c + 0.5f * (a - c));

		PushNewVertices(new VertexUpdate[] {
			VertexUpdate.FromVertexInfo(vA, dMesh.GetVertexAll(vA)),
			VertexUpdate.FromVertexInfo(vB, dMesh.GetVertexAll(vB)),
			VertexUpdate.FromVertexInfo(vC, dMesh.GetVertexAll(vC))
		});


		// add the 4 new triangles and push changes to GPU

		int t1 = dMesh.AppendTriangle(va, vA, vC);
		int t2 = dMesh.AppendTriangle(vC, vA, vB);
		int t3 = dMesh.AppendTriangle(vA, vb, vB);
		int t4 = dMesh.AppendTriangle(vC, vB, vc);


		PushNewTriangles(new TriangleUpdate[]
		{
			new TriangleUpdate() { a = va, b = vA, c = vC, index = t1},
			new TriangleUpdate() { a = vC, b = vA, c = vB, index = t2},
			new TriangleUpdate() { a = vA, b = vb, c = vB, index = t3},
			new TriangleUpdate() { a = vC, b = vB, c = vc, index = t4}
		});


		// remove original larger triangle
		dMesh.RemoveTriangle(tID);

		PushRemoveTriangles(new TriangleUpdate[]
		{
			new TriangleUpdate() { a = 0, b = 0, c = 0, index = tID}
		}, false);

	}

	bool TriangleValid(int tID)
	{
		return tID != DMesh3.InvalidID && tID != DMesh3.NonManifoldID;
	}

	internal void SubdivideTriangles(int[] tIDs)
	{

		// add 3 verts per triangle
		VertexUpdate[] vertUpdates = new VertexUpdate[3 * tIDs.Length];

		// add 4 tris per triangle
		TriangleUpdate[] triUpdates = new TriangleUpdate[4 * tIDs.Length];

		// always remove the triangle after subdividing
		TriangleUpdate[] triRemoves = new TriangleUpdate[tIDs.Length];
		
		for (int i = 0; i < tIDs.Length; i++)
		{

			int tID = tIDs[i];
			
			var triangle = dMesh.GetTriangle(tID);
			int va = triangle.a, vb = triangle.b, vc = triangle.c;

			Vector3d
				a = dMesh.GetVertex(triangle.a),
				b = dMesh.GetVertex(triangle.b),
				c = dMesh.GetVertex(triangle.c);


			// add 3 new verts and push changes to GPU
			int vA = dMesh.AppendVertex(a + 0.5f * (b - a));
			int vB = dMesh.AppendVertex(b + 0.5f * (c - b));
			int vC = dMesh.AppendVertex(c + 0.5f * (a - c));


			vertUpdates[3 * i]     = VertexUpdate.FromVertexInfo(vA, dMesh.GetVertexAll(vA));
			vertUpdates[3 * i + 1] = VertexUpdate.FromVertexInfo(vB, dMesh.GetVertexAll(vB));
			vertUpdates[3 * i + 2] = VertexUpdate.FromVertexInfo(vC, dMesh.GetVertexAll(vC));


			// add the 4 new triangles and push changes to GPU

			int t1 = dMesh.AppendTriangle(va, vA, vC);
			if (!TriangleValid(t1))
			{
				return;
			}

			int t2 = dMesh.AppendTriangle(vC, vA, vB);
			if (!TriangleValid(t2))
			{
				return;
			}

			int t3 = dMesh.AppendTriangle(vA, vb, vB);
			if (!TriangleValid(t3)) {

			}

			int t4 = dMesh.AppendTriangle(vC, vB, vc);
			if (!TriangleValid(t4))
			{
				return;
			}

			triUpdates[4 * i + 0] = new TriangleUpdate() { a = va, b = vA, c = vC, index = t1 };
			triUpdates[4 * i + 1] = new TriangleUpdate() { a = vC, b = vA, c = vB, index = t2 };
			triUpdates[4 * i + 2] = new TriangleUpdate() { a = vA, b = vb, c = vB, index = t3 };
			triUpdates[4 * i + 3] = new TriangleUpdate() { a = vC, b = vB, c = vc, index = t4 };
			
			// remove original larger triangle
			MeshResult result = dMesh.RemoveTriangle(tID);

			if (result != MeshResult.Ok)
			{
				return;
			}

			triRemoves[i] = new TriangleUpdate() { a = 0, b = 0, c = 0, index = tID };
			
		}

		PushNewVertices(vertUpdates, false);
		PushNewTriangles(triUpdates, false);
		PushRemoveTriangles(triRemoves, false);

	}



	// extrudes a triangle face, returns the id of the new face triangle for moving around afterwards
	// NOTE(josel): this has problems with non closed 3d surfaces, like the quad primitive
	internal int ExtrudeFace(int tID)
	{

		int newTID = -1;

		VertexUpdate[] newVerts = new VertexUpdate[3];
		TriangleUpdate[] newTris = new TriangleUpdate[7];

		var triangle = dMesh.GetTriangle(tID);
		Vector3d N = dMesh.GetTriNormal(tID);

		// remove initial triangle
		MeshResult result = dMesh.RemoveTriangle(tID);

		if (result != MeshResult.Ok)
		{
			Debug.LogError(string.Format("failed to remove triangle {0} in InsetTriangle", tID));
			return -1;
		}

		PushRemoveTriangles(new TriangleUpdate[] { new TriangleUpdate() { a = triangle.a, b = triangle.b, c = triangle.c, index = tID } }, false);

		// first, we duplicate the three existing vertices, extruding away from the normals of each vert
		NewVertexInfo newVInfo = dMesh.GetVertexAll(triangle.a);
		newVInfo.v = newVInfo.v + 0.2f * N;
		int newVID = dMesh.AppendVertex(ref newVInfo);
		newVerts[0] = VertexUpdate.FromVertexInfo(newVID, newVInfo);


		newVInfo = dMesh.GetVertexAll(triangle.b);
		newVInfo.v = newVInfo.v + 0.2f * N;
		newVID = dMesh.AppendVertex(ref newVInfo);
		newVerts[1] = VertexUpdate.FromVertexInfo(newVID, newVInfo);


		newVInfo = dMesh.GetVertexAll(triangle.c);
		newVInfo.v = newVInfo.v + 0.2f * N;
		newVID = dMesh.AppendVertex(ref newVInfo);
		newVerts[2] = VertexUpdate.FromVertexInfo(newVID, newVInfo);

		// new face
		int I0 = newVerts[0].vID,
			I1 = newVerts[1].vID,
			I2 = newVerts[2].vID,
			i0 = triangle.a,
			i1 = triangle.b,
			i2 = triangle.c;


		newTID = dMesh.AppendTriangle(I0, I1, I2);

		if (newTID == DMesh3.InvalidID || newTID == DMesh3.NonManifoldID)
		{
			Debug.LogError(string.Format("failed to append new triangle in ExtrudeFace"));
			return -1;		
		}


		newTris[0] = new TriangleUpdate() { a = I0, b = I1, c =I2, index = newTID };

		AddQuad(i0, i2, I0, I2, ref newTris, 1);
		AddQuad(i2, i1, I2, I1, ref newTris, 3);
		AddQuad(i1, i0, I1, I0, ref newTris, 5);

		PushNewVertices(newVerts);
		PushNewTriangles(newTris);

		MeshNormals.QuickCompute(dMesh);

		return newTID;

	}

	/*
	 *  a ---------  b
	 *  | \      /  /
	 *  |  A - B   /
 		|  |  /   /
		|  c /  /
		| /   /
		|   /
		| /
		C   
	 * 
	 * 
	 */ 

	internal int InsetTriangle(int tID)
	{

		int newTID = -1;

		var triangle = dMesh.GetTriangle(tID);
		int va = triangle.a, vb = triangle.b, vc = triangle.c;

		// three corners of the triangle
		Vector3d
			a = dMesh.GetVertex(triangle.a),
			b = dMesh.GetVertex(triangle.b),
			c = dMesh.GetVertex(triangle.c);

		// triangle center
		Vector3d triCenter = dMesh.GetTriBaryPoint(tID, 0.3333f, 0.3333f, 0.3333f);

		float newCornerOffset = 0.25f;

		// remove initial triangle
		MeshResult result = dMesh.RemoveTriangle(tID);
		
		if (result != MeshResult.Ok)
		{
			Debug.LogError(string.Format("failed to remove triangle {0} in InsetTriangle", tID));
			return -1;
		}

		PushRemoveTriangles(new TriangleUpdate[] { new TriangleUpdate() { a = triangle.a, b = triangle.b, c = triangle.c, index = tID } }, false);

		// add 3 new verts and push changes to GPU
		int I0 = dMesh.AppendVertex(a + newCornerOffset * (triCenter - a));
		int I1 = dMesh.AppendVertex(b + newCornerOffset * (triCenter - b));
		int I2 = dMesh.AppendVertex(c + newCornerOffset * (triCenter - c));
		int i0 = triangle.a,
			i1 = triangle.b,
			i2 = triangle.c;

		PushNewVertices(new VertexUpdate[] {
			VertexUpdate.FromVertexInfo(I0, dMesh.GetVertexAll(I0)),
			VertexUpdate.FromVertexInfo(I1, dMesh.GetVertexAll(I1)),
			VertexUpdate.FromVertexInfo(I2, dMesh.GetVertexAll(I2))
		});

		TriangleUpdate[] newTris = new TriangleUpdate[7];

		// add new center triangle
		newTID = dMesh.AppendTriangle(I0, I1, I2);

		if (newTID == DMesh3.InvalidID || newTID == DMesh3.NonManifoldID)
		{
			Debug.LogError(string.Format("failed to append triangle in InsetTriangle"));
			return -1;
		}

		newTris[0] = new TriangleUpdate() { a = I0, b = I1, c = I2, index = newTID };

		AddQuad(i0, i1, I0, I1, ref newTris, 1);
		AddQuad(i1, i2, I1, I2, ref newTris, 3);
		AddQuad(i2, i0, I2, I0, ref newTris, 5);

		PushNewTriangles(newTris);

	
		MeshNormals.QuickCompute(dMesh);


		return newTID;

	}

	/*
	 *  Adds a quad from already existing vertex indices:
	 * 
	 *   I ---- J
	 *   |	  / |
  	 *   |  /	|
	 *   |/		|	    
	 *   i -----j
	 * 
	 */
	private void AddQuad(int i, int j, int I, int J, ref TriangleUpdate[] newTris, int index)
	{

		int d1 = dMesh.AppendTriangle(i, I, J);

		if (d1 == DMesh3.NonManifoldID || d1 == DMesh3.InvalidID)
		{
			Debug.LogError(string.Format("failed to insert first triangle in AddQuad"));
			return;
		}

		newTris[index] = new TriangleUpdate() { a = i, b = I, c = J, index = d1 };

		int d2 = dMesh.AppendTriangle(i, J, j);

		if (d2 == DMesh3.NonManifoldID || d2 == DMesh3.InvalidID)
		{
			Debug.LogError(string.Format("failed to insert second triangle in AddQuad"));
			return;
		}

		newTris[index + 1] = new TriangleUpdate() { a = i, b = J, c = j, index = d2 };

	}

	internal void HighlightTriangles(int[] tids)
	{
		gpuMesh.HighlightTriangles(tids);
	}

	internal void ClearTriangleHighlight(int[] tids)
	{
		gpuMesh.ClearTriangleHighlight(tids);
	}
}
