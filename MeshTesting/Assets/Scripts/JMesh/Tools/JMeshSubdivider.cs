﻿using UnityEngine;

public class JMeshSubdivider : MonoBehaviour {

	public JMesh mesh;
	public PlayerData playerData;

	public float m_searchRadius = 1.0f;
	public Vector3 m_handStartPos;
	int m_closestTriID;
	private bool m_active;

	public void SetActive(bool b)
	{
		m_active = b;
	}

	public void SetMesh(JMesh mesh)
	{
		this.mesh = mesh;
	}

	public void OnTriggerPressed()
	{

		if (!m_active)
		{
			return;
		}

		m_handStartPos = transform.InverseTransformPoint(playerData.rightHand.transform.position);
		m_closestTriID = mesh.spatial.FindNearestTriangle(m_handStartPos, m_searchRadius);

		if (m_closestTriID == g3.DMesh3.InvalidID)
		{
			return;
		}

		mesh.SubvidideTriangle(m_closestTriID);
	}

	public void OnTriggerReleased()
	{
		if (!m_active)
		{
			return;
		}

		mesh.RebuildSpatialTree();

	}

}
