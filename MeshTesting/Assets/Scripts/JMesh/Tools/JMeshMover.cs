﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class JMeshMover : MonoBehaviour {

    JMesh mesh;
    public PlayerData playerData;
    Matrix4x4 originalHandTransform, moveTransform, meshToWorld;
    Vector3 originalHandPos = default(Vector3);
    public Material moveMat;
    public MoveData data;
	public bool m_moving = false;
	List<int> vIDs = new List<int>();


	public struct MoveData
    {
        public Vector3 originalToolPos;
        public Matrix4x4 moveTransform;
        public Matrix4x4 meshToWorld;
        public float radius;
        public float bandWidth;
    }

    public void SetMesh(JMesh mesh)
    {
        this.mesh = mesh;
    }

	bool active = false;

	public void SetActive(bool act)
	{
		this.active = act;
	}

	public void OnPressed()
    {

        originalHandTransform = playerData.rightHand.localToWorldMatrix;
        originalHandPos = playerData.rightHand.position;
		m_moving = true;

		var localHandPos = transform.InverseTransformPoint(originalHandPos);

		// get list of vIDs within range of hand
		vIDs.Clear();

		foreach(var vertID in mesh.dMesh.VertexIndices())
		{

			var vert = mesh.dMesh.GetVertex(vertID);

			if (Vector3.Distance((Vector3)vert, localHandPos) < 0.2f)
			{
				vIDs.Add(vertID);
			}

		}

		m_moving = true;
    }

	public void Update()
	{

		meshToWorld = transform.localToWorldMatrix;

		if (m_moving)
		{

			var toolTransform = playerData.rightHand.localToWorldMatrix;
			var toolPosition = playerData.rightHand.position;
			moveTransform = toolTransform * originalHandTransform.inverse;

			data = new MoveData()
			{
				originalToolPos = this.originalHandPos,
				moveTransform = this.moveTransform,
				meshToWorld = this.meshToWorld,
				radius = 0.2f,
				bandWidth = 0.1f
			};

		}

		moveMat.SetVector("originalToolPos", originalHandPos);
		moveMat.SetMatrix("meshToWorld", meshToWorld);
		moveMat.SetMatrix("moveTransform", moveTransform);
		moveMat.SetFloat("radius", 0.2f);
		moveMat.SetFloat("bandWidth", 0.1f);

	}

    public void OnRelease()
    {

		// nothing to move
		if (vIDs.Count <= 0)
		{
			return;
		}

		StartCoroutine(MoveMesh(data, () =>
		{
			moveTransform = Matrix4x4.identity;
			originalHandPos = Vector3.zero;
			m_moving = false;
		}));

	}
 
    private IEnumerator MoveMesh(MoveData data, System.Action callback)
    {

        bool done = false;

		VertexUpdate[] vertexUpdates = new VertexUpdate[vIDs.Count];

        Thread t = new Thread(() => {

			for (int i = 0; i < vIDs.Count; i++)
			{

				int index = vIDs[i];
				var vertexWorldPos = data.meshToWorld.MultiplyPoint(mesh.dMesh.GetVertexAsVector3(index));
				float distance = Vector3.Distance(vertexWorldPos, data.originalToolPos);
				float falloff = Mathf.Clamp((data.radius - distance) / data.bandWidth, -1.0f, 1.0f);
				falloff = 0.5f * (falloff + 1.0f);	
				Vector3 direction = data.moveTransform.MultiplyPoint(vertexWorldPos) - vertexWorldPos;
				vertexWorldPos = vertexWorldPos + falloff * direction;
				mesh.dMesh.SetVertexFromVector3(index, data.meshToWorld.inverse.MultiplyPoint(vertexWorldPos));

				vertexUpdates[i] = VertexUpdate.FromVertexInfo(index, mesh.dMesh.GetVertexAll(index));

			}

            done = true;

        });

        t.Start();

        yield return new WaitUntil(() => done);
	
		mesh.PushVertexUpdates(vertexUpdates, false);
		
        callback.Invoke();

    }

}
