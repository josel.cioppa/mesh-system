﻿using UnityEngine;
using g3;
using System.Collections.Generic;

public class JMeshOneRingMover : MonoBehaviour
{

    public JMesh mesh;
    public PlayerData playerData;
    public float m_searchRadius = 1.0f;
    public Vector3 m_handStartPos, m_endHandPos;
    int  m_closestVertID;
    Dictionary<int, NewVertexInfo> m_neighboringVertices = new Dictionary<int, NewVertexInfo>();
    private bool m_active;
    public bool mUpdateUnityMesh = true;
	VertexUpdate[] m_vertexUpdates = null;
	List<int> onRingTriIDS = new List<int>();


    public void SetActive(bool b)
    {
        m_active = b;
    }

    public void SetMesh(JMesh mesh)
    {
        this.mesh = mesh;
    }


	public void OnTriggerPressed()
    {

        if (!m_active)
        {
            return;
        }

        m_handStartPos = transform.InverseTransformPoint(playerData.rightHand.transform.position);
        m_closestVertID = mesh.spatial.FindNearestVertex(m_handStartPos, m_searchRadius);



		// find and highlight the 1-ring around the closest vertex
		onRingTriIDS.Clear();

		foreach(int id in mesh.dMesh.VtxTrianglesItr(m_closestVertID))
		{
			onRingTriIDS.Add(id);
		}
		mesh.HighlightTriangles(onRingTriIDS.ToArray());




		// iterate over the neighboring verts and cache their IDs
		List<int> newIndices = new List<int>();
        m_neighboringVertices.Clear();

		foreach (int id in mesh.dMesh.VtxVerticesItr(m_closestVertID))
        {
            m_neighboringVertices.Add(id, mesh.dMesh.GetVertexAll(id));
			newIndices.Add(id);
		}


		// create the list of vertex updates and populate their vID (but no other data, we'll do that on hold())
		m_vertexUpdates = new VertexUpdate[m_neighboringVertices.Count];
		for (int i = 0; i < m_vertexUpdates.Length; i++)
		{
			m_vertexUpdates[i].vID = newIndices[i];
		}

				
          

    }

    public void OnTriggerHeld()
    {

        if (!m_active)
        {
            return;
        }

        if ( m_closestVertID == DMesh3.InvalidID)
        {
            return;
        }

        var dir = transform.InverseTransformPoint(playerData.rightHand.transform.position) - m_handStartPos;

		for (int i = 0; i < m_vertexUpdates.Length; i++)
		{
			int id = m_vertexUpdates[i].vID;
			mesh.dMesh.SetVertex(id, m_neighboringVertices[id].v + dir * 0.5f);
			m_vertexUpdates[i].position = (Vector3)m_neighboringVertices[id].v + dir * 0.5f;
			m_vertexUpdates[i].normal = m_neighboringVertices[id].n;
			m_vertexUpdates[i].color = m_neighboringVertices[id].c;
			m_vertexUpdates[i].vID = id;
		}
	
		if (mUpdateUnityMesh)
		{
			mesh.PushVertexUpdates(m_vertexUpdates);
		}

    }

    public void OnTriggerReleased()
    {
        if (!m_active)
        {
            return;
        }

		mesh.ClearTriangleHighlight(onRingTriIDS.ToArray());
		mesh.RebuildSpatialTree();

	}



}
