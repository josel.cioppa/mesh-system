﻿using UnityEngine;
using g3;
using System.Collections.Generic;

public class JMeshFaceExtrude : MonoBehaviour
{

	public JMesh mesh;
	public PlayerData playerData;
	public float m_searchRadius = 1.0f;
	public Vector3 m_handStartPos, m_endHandPos;
	int m_closestVertID;
	private bool m_active;
	public bool mUpdateUnityMesh = true;
	int m_closestTriID;

	Index3i triangle;
	VertexUpdate[] vertUpdates = new VertexUpdate[3];
	NewVertexInfo[] originalVertData = new NewVertexInfo[3];

	public void SetActive(bool b)
	{
		m_active = b;
	}

	public void SetMesh(JMesh mesh)
	{
		this.mesh = mesh;
	}


	public void OnTriggerPressed()
	{

		if (!m_active)
		{
			return;
		}

		m_handStartPos = transform.InverseTransformPoint(playerData.rightHand.transform.position);
		m_closestTriID = mesh.spatial.FindNearestTriangle(m_handStartPos, m_searchRadius);

		if (m_closestTriID == g3.DMesh3.InvalidID)
		{
			return;
		}

		int newFaceID = mesh.ExtrudeFace(m_closestTriID);
		triangle = mesh.dMesh.GetTriangle(newFaceID);

		// cache original vertex data of the triangle so we can move it later
		originalVertData[0] = mesh.dMesh.GetVertexAll(triangle.a);
		originalVertData[1] = mesh.dMesh.GetVertexAll(triangle.b);
		originalVertData[2] = mesh.dMesh.GetVertexAll(triangle.c);


	}

	public void OnTriggerHeld()
	{

		if (!m_active)
		{
			return;
		}

		var dir = transform.InverseTransformPoint(playerData.rightHand.transform.position) - m_handStartPos;

		int id = triangle.a;
		var vertexInfo = originalVertData[0];
		mesh.dMesh.SetVertex(id, vertexInfo.v + dir * 0.1f);
		vertUpdates[0].position = (Vector3)vertexInfo.v + dir * 0.1f;
		vertUpdates[0].normal = vertexInfo.n;
		vertUpdates[0].color = vertexInfo.c;
		vertUpdates[0].vID = id;

		id = triangle.b;
		vertexInfo = originalVertData[1];
		mesh.dMesh.SetVertex(id, vertexInfo.v + dir * 0.1f);
		vertUpdates[1].position = (Vector3)vertexInfo.v + dir * 0.1f;
		vertUpdates[1].normal = vertexInfo.n;
		vertUpdates[1].color = vertexInfo.c;
		vertUpdates[1].vID = id;

		id = triangle.c;
		vertexInfo = originalVertData[2];
		mesh.dMesh.SetVertex(id, vertexInfo.v + dir * 0.1f);
		vertUpdates[2].position = (Vector3)vertexInfo.v + dir * 0.1f;
		vertUpdates[2].normal = vertexInfo.n;
		vertUpdates[2].color = vertexInfo.c;
		vertUpdates[2].vID = id;

		mesh.PushVertexUpdates(vertUpdates);

	}

	public void OnRelease()
	{
		if (!m_active)
		{
			return;
		}

		mesh.RebuildSpatialTree();

	}


}
