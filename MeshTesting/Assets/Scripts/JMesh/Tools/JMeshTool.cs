﻿public interface JMeshTool
{

	bool isActive { get; set; }
	JMesh m_mesh { get; set; }

	void SetMesh(JMesh mesh);
	void SetActive(bool active);
	void StartUsingTool();
	void UseTool();
	void FinishUsingTool();

}