﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandController : MonoBehaviour {

	public XRNode node;

	private void Start()
	{
		if (node == XRNode.RightHand)
		{
			VRInputManager.SetControllerActive(true);
			VRInputManager.SetControllerTransform(this.transform);
		}
	}

	private void Update()
	{
		transform.localPosition = InputTracking.GetLocalPosition(node);
		transform.localRotation = InputTracking.GetLocalRotation(node);
	}

	public void OnTriggerPressed()
	{
		VRInputManager.SetIsControllerButtonPressed(true);
	}

	public void OnTriggerReleased()
	{
		VRInputManager.SetIsControllerButtonPressed(false);
	}
	

}
