﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using g3;

[RequireComponent(typeof(JMeshGenerator))]
public class PrimitiveCreator : MonoBehaviour {

    public enum PrimitiveShape
    {
        Sphere,
        Cube,
        Quad,
		Ribbon,
        Cylinder
    }

	public PrimitiveShape currentShape;
	public Transform spawnPoint;
	public ActiveGameObject activeObject;
	public Transform rightHandTransform;
    public Transform primitivePrefab;

    JMeshGenerator meshGenerator;

    public void Awake()
    {
        meshGenerator = GetComponent<JMeshGenerator>();
    }

	public void SetPrimitiveType(int shape)
	{
		currentShape = (PrimitiveShape) shape;
	}

	public void CreateSphere()
	{
		CreatePrimitive(PrimitiveShape.Sphere);
	}

	public void CreateCube()
	{
		CreatePrimitive(PrimitiveShape.Cube);
	}

	public void CreateQuad()
	{
		CreatePrimitive(PrimitiveShape.Quad);
	}

	public void CreateCylinder()
	{
		CreatePrimitive(PrimitiveShape.Cylinder);
	}

	public void CreateShape()
	{

			
	}

    private void CreatePrimitive(PrimitiveShape shape)
    {

		// instantiate the GPU mehs prefab
        var obj = GameObject.Instantiate(primitivePrefab, spawnPoint.position, spawnPoint.rotation).gameObject;

        JMesh jmesh = null;


		// create jmesh based on what kind of shape we've selected
        switch(shape)
        {
            case PrimitiveShape.Sphere:
            {
                   jmesh = meshGenerator.Sphere(1.0f, 128);
                } break;

            case PrimitiveShape.Cube:
            {
                jmesh = meshGenerator.Cube();
            }
            break;

            case PrimitiveShape.Quad:
            {
                jmesh = meshGenerator.Quad();
            } break;

            case PrimitiveShape.Cylinder:
            {
                jmesh = meshGenerator.Cylinder();
            }
            break;
        }


        if (jmesh == null)
        {
            Debug.LogError("Failed to instantiate requested mesh");
            return;
        }

		var filter = obj.GetComponent<JMeshFilter>();
		filter.SetMesh(jmesh);


		var comp = obj.GetComponent<JMeshOneRingMover>();
		comp.SetMesh(jmesh);
		comp.SetActive(true);


		var comp2 = obj.GetComponent<JMeshMover>();
		comp2.SetMesh(jmesh);
		comp2.SetActive(true);


		var comp3 = obj.GetComponent<JMeshSubdivider>();
		comp3.SetMesh(jmesh);
		comp3.SetActive(true);


		var comp4 = obj.GetComponent<JMeshFaceExtrude>();
		comp4.SetMesh(jmesh);
		comp4.SetActive(true);


		var comp5 = obj.GetComponent<JMeshInsetTriangle>();
		comp5.SetMesh(jmesh);
		comp5.SetActive(true);


		activeObject.SetActiveObject(obj);


    }

}
