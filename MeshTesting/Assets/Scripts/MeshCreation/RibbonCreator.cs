﻿using UnityEngine;

public class RibbonCreator : MonoBehaviour {

    public ActiveGameObject activeObject;
    public Transform ribbonPrefab;
	public ComputeShader meshUpdateShader;

    public void CreateNewRibbon(Transform startingTransform)
    {

        var obj =  GameObject.Instantiate(ribbonPrefab, startingTransform.position, startingTransform.rotation).gameObject;

        
		// generate custom JMesh
        g3.DMesh3 dmesh = new g3.DMesh3();
        JMesh mesh = new JMesh(dmesh);
		mesh.gpuMesh.updateMeshShader = meshUpdateShader;

        // setup mesh filter and set mesh
        var filter = obj.GetComponent<JMeshFilter>(); 
        filter.mesh = mesh;
		    

        // set mesh on mesh manipulator
        var a = obj.GetComponent<JMeshOneRingMover>();
        a.SetMesh(mesh);
        a.SetActive(true);




        // set mesh on ribbon creator
        var b = obj.GetComponent<JRibbonCreator>();
        b.SetMesh(mesh);


		//// allows the mesh to be moved
		var c = obj.GetComponent<JMeshMover>();
		c.SetMesh(mesh);

		// set mesh on mesh manipulator
		var d = obj.GetComponent<JMeshSubdivider>();
		d.SetMesh(mesh);
		d.SetActive(true);


		activeObject.SetActiveObject(obj);


    }
	
}

