﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using g3;

public class JMeshGenerator : MonoBehaviour {

	#region static generators

	public ComputeShader meshUpdateShader;

    public JMesh Sphere(float rad, int subdivisions)
    {
        var gen = new g3.Sphere3Generator_NormalizedCube()
        {
            Radius = rad
        };

        return GPUMeshFromGenerator(gen);
    }

    public  JMesh Cube()
    {
        var gen = new g3.TrivialBox3Generator()
        {

        };

        return GPUMeshFromGenerator(gen);
    }

    public  JMesh GridCube()
    {
        var gen = new g3.GridBox3Generator()
        {
        };

        return GPUMeshFromGenerator(gen);
    }

    public  JMesh Disc(float rad, int subdivisions)
    {
        var gen = new g3.TrivialDiscGenerator()
        {
            Radius = rad,
            Slices = subdivisions
        };

        return GPUMeshFromGenerator(gen);
    }

    public  JMesh Quad()
    {
        var gen = new g3.TrivialRectGenerator()
        {
        };

        return GPUMeshFromGenerator(gen);
    }

    public  JMesh Cylinder()
    {
        var gen = new g3.CappedCylinderGenerator()
        {

        };

        return GPUMeshFromGenerator(gen);

    }

    private JMesh GPUMeshFromGenerator(MeshGenerator generator)
    {
        var gen = generator.Generate();
        var dmesh = gen.MakeDMesh();
        var gpuMesh = new GPUMesh(dmesh);
		var jmesh = new JMesh(dmesh, gpuMesh);
		jmesh.gpuMesh.updateMeshShader = meshUpdateShader;
		return jmesh;
    }



    #endregion

    #region generators using implicit surfaces

    public JMesh ImplicitSphere(float radius, Vector3d origin, int resolution)
    {
        var isoSurface = new ImplicitSphere3d()
        {
            Radius = radius,
            Origin = origin
        };

        return Generate(isoSurface, 32);
    }

    public JMesh ImplicitCube(int resolution)
    {
        var isoSurface = new ImplicitBox3d()
        {
            
        };

        return Generate(isoSurface, resolution);
    }

    public JMesh ImplicitLine(Vector3d start, Vector3d end, double radius, int resolution)
    {
        var isoSurface = new ImplicitLine3d()
        {
            Segment = new Segment3d(start, end),
            Radius = radius
        };

        return Generate(isoSurface, resolution);
    }

    public JMesh Generate(BoundedImplicitFunction3d isoSurface, int resolution)
    {

        MarchingCubes c = new MarchingCubes();
        c.Implicit = isoSurface;
        c.RootMode = MarchingCubes.RootfindingModes.LerpSteps;      // cube-edge convergence method
        c.RootModeSteps = 5;                                        // number of iterations
        c.Bounds = isoSurface.Bounds();
        c.CubeSize = c.Bounds.MaxDim / resolution;
        c.Bounds.Expand(3 * c.CubeSize);                            // leave a buffer of cells
        c.Generate();
        MeshNormals.QuickCompute(c.Mesh);                           // generate normals

        JMesh jmesh = new JMesh(c.Mesh);
		jmesh.gpuMesh.updateMeshShader = meshUpdateShader;

		return jmesh;

    }




    #endregion

}
