﻿using UnityEngine;

[CreateAssetMenu]
public class ActiveGameObject : ScriptableObject {

	public GameObject activeObject;

    public void SetActiveObject(GameObject obj)
    {
        if (activeObject != null)
        {
           // activeObject.GetComponent<JMeshOneRingMover>().SetActive(false);
        }

        activeObject = obj;
        //activeObject.GetComponent<JMeshOneRingMover>().SetActive(true);
	}

}
