﻿using UnityEngine;

[CreateAssetMenu]
public class PlayerData : ScriptableObject {
    public Transform head;
    public Transform rightHand;
    public Transform leftHand;
}
