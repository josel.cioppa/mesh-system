using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using g3;

public class UVMapMesh : MonoBehaviour
{

    [DllImport("libigl2unity_bin.dll")]
    public static extern bool ComputeLSCM_UnityMesh(IntPtr arr_verts, int arr_nverts, IntPtr arr_faces, int arr_nfaces, IntPtr arr_uvs, int arr_nuvs);




    /*
    * Uses libigl to do computations
    */

    // just computes the LSCM for the mesh this script is attached to
    void ComputeLSCM_libigl()
    {
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch(); // benchmarking speeds

        MeshFilter mf = GetComponent<MeshFilter>();
        var mesh = mf.sharedMesh;



        DMesh3 g3mesh = new DMesh3();
        g3mesh.UpdateFromUnityMesh(mesh, false);

        //DMesh3Builder.Build(mesh.vertices, mesh.triangles, mesh.normals);

        // copy vertex data to float array
        //g3mesh.VerticesBuffer.ToArray(); // double array, possible optimization as current floats are being passed to the dll
        sw.Start();
        //int vert_arr_length = mesh.vertices.Length * 3; // 3 floats per vertex
        int vert_arr_length = g3mesh.VertexCount * 3; // 3 floats per vertex
        int vert_arr_idx = 0;
        float[] vert_floats = new float[vert_arr_length];
        //foreach (Vector3 v3 in mesh.vertices)
        //foreach (Vector3d v3 in g3mesh.Vertices())
        //{
        //    vert_floats[vert_arr_idx] = (float)v3.x;
        //    vert_floats[vert_arr_idx+1] = (float)v3.y;
        //    vert_floats[vert_arr_idx+2] = (float)v3.z;
        //    vert_arr_idx+=3;
        //}
        sw.Stop();
        Debug.Log("Copy verts to float array: " + sw.ElapsedMilliseconds);
        sw.Reset();

        // copy face data to int array
        sw.Start();
        //int tri_arr_length = mesh.triangles.Length; // 3 ints per triangle
        int tri_arr_length = g3mesh.TriangleCount * 3; // 3 ints per triangle
        sw.Stop();
        Debug.Log("Copy faces to int array: " + sw.ElapsedMilliseconds);
        sw.Reset();

        // create uv data to float array
        sw.Start();
        //int uv_arr_length = mesh.vertices.Length * 2; // 2 floats per uv vertex
        int uv_arr_length = g3mesh.VertexCount * 2; // 2 floats per uv vertex
        //float[] uv_floats = new float[uv_arr_length];
        double[] uv_verts = new double[uv_arr_length];
        sw.Stop();
        Debug.Log("Create uv floats array: " + sw.ElapsedMilliseconds);
        sw.Reset();

        // get memory handles to avoid GC coming in and messing things up
        //GCHandle vert_handle = GCHandle.Alloc(vert_floats, GCHandleType.Pinned);
        GCHandle vert_handle = GCHandle.Alloc(g3mesh.VerticesBuffer.ToArray(), GCHandleType.Pinned);
        IntPtr vert_address = vert_handle.AddrOfPinnedObject();

        //GCHandle tri_handle = GCHandle.Alloc(mesh.triangles, GCHandleType.Pinned);
        GCHandle tri_handle = GCHandle.Alloc(g3mesh.TrianglesBuffer.ToArray(), GCHandleType.Pinned);
        IntPtr tri_address = tri_handle.AddrOfPinnedObject();

        GCHandle uv_handle = GCHandle.Alloc(uv_verts, GCHandleType.Pinned);
        IntPtr uv_address = uv_handle.AddrOfPinnedObject();

        // send to C++ to make changes
        sw.Start();
        bool bSuccess = ComputeLSCM_UnityMesh(vert_address, vert_arr_length, tri_address, tri_arr_length, uv_address, uv_arr_length);
        sw.Stop();
        Debug.Log("C++ lib to compute uv map (" + bSuccess + ") : " + sw.ElapsedMilliseconds);
        sw.Reset();

        // copy just the uvs back into unity mesh
        sw.Start();
        Vector2[] newUVs = new Vector2[mesh.vertices.Length];
        int array_idx = 0;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Vector2 computedUV = new Vector2();
            computedUV.x = (float)uv_verts[array_idx];
            computedUV.y = (float)uv_verts[array_idx+1];
            newUVs[i] = computedUV;
            array_idx += 2;
        }
        mesh.uv = newUVs;
        sw.Stop();
        Debug.Log("Copy data back into Unity mesh: " + sw.ElapsedMilliseconds);
        sw.Reset();

        // only free the memory once we're done with everything
        vert_handle.Free();
        tri_handle.Free();
        uv_handle.Free();
    }

	public GameObject flattenGO;

    // computes the LSCM and outputs the results into a game object named "flattened" for visualization
    void ComputeAndVisualize_LSCM()
    {

		if (flattenGO == null)
		{
			Debug.LogError("please set a flattened GameObject");
			return;
		}

        ComputeLSCM_libigl();

        /// Debug purposes only: output to a file to manually verify results
        // MeshFilter mf_3d = GetComponent<MeshFilter>();
        // string mesh_string = MeshToString(mf_3d);
        // WriteToFile(mesh_string, "/home/po/dev/data/MPVR/uv_mapped.obj");

        // copy just the uvs back into another unity mesh
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch(); // benchmarking speeds
        sw.Start();

        MeshFilter mf_3d = GetComponent<MeshFilter>();
        var mesh_3d = mf_3d.sharedMesh; // get the flattened mesh for viewing

        MeshFilter flat_mf = flattenGO.GetComponent<MeshFilter>();
        Vector3[] flat_verts = new Vector3[mesh_3d.vertices.Length];
        for (int i = 0; i < flat_verts.Length; i++)
        {
            Vector3 flat_3d = new Vector3();
            flat_3d.x = mesh_3d.uv[i].x;
            flat_3d.y = 0.0f;
            flat_3d.z = mesh_3d.uv[i].y;
            flat_verts[i] = flat_3d;
        }

        Mesh flat_mesh = new Mesh();
        flat_mesh.vertices = flat_verts;
        flat_mesh.triangles = mesh_3d.triangles;
        flat_mf.sharedMesh = flat_mesh;
        flat_mf.sharedMesh.RecalculateNormals();

        sw.Stop();
        Debug.Log("Copy computed UVs in a 3d mesh for visualization: " + sw.ElapsedMilliseconds);
        sw.Reset();
    }


	public void GenerateUVS(DMesh3 mesh)
	{

		
	}

    void Start()
    {
        ComputeAndVisualize_LSCM(); // computes the LSCM of the attached mesh and then output the results to another game object called "flattened"
    }    
}
