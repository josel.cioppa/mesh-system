﻿using UnityEngine;

public class ObjectScaler : MonoBehaviour {

	public ActiveGameObject target;
	public Transform otherHand;
	public Vector3 originalScale;
	public float originalHandDistance;

	public void OnStartScaling()
	{
		if (target.activeObject == null)
        {
            return;
        }
		originalScale = target.activeObject.transform.localScale;
		originalHandDistance = Vector3.Distance(transform.position, otherHand.position);
	}

	public void Scale()
	{
		if (target.activeObject == null)
        {
			return;
        }
		float newScaleFactor = Vector3.Distance(transform.position, otherHand.position) / originalHandDistance;
		target.activeObject.transform.localScale = newScaleFactor * originalScale;
    }
	
}
