﻿using UnityEngine;
using g3;

// various utility code
public class UnityUtils
{


	// stupid per-type conversion functions because fucking C# 
	// can't do typecasts in generic functions
	public static Vector3[] dvector_to_vector3(DVector<double> vec)
	{


		int nLen = vec.Length / 3;
		Vector3[] result = new Vector3[nLen];
		for (int i = 0; i < nLen; ++i)
		{
			result[i].x = (float)vec[3 * i];
			result[i].y = (float)vec[3 * i + 1];
			result[i].z = (float)vec[3 * i + 2];
		}
		return result;
	}
	public static Vector3[] dvector_to_vector3(DVector<float> vec)
	{
		int nLen = vec.Length / 3;
		Vector3[] result = new Vector3[nLen];
		for (int i = 0; i < nLen; ++i)
		{
			result[i].x = vec[3 * i];
			result[i].y = vec[3 * i + 1];
			result[i].z = vec[3 * i + 2];
		}
		return result;
	}
	public static Vector2[] dvector_to_vector2(DVector<float> vec)
	{
		int nLen = vec.Length / 2;
		Vector2[] result = new Vector2[nLen];
		for (int i = 0; i < nLen; ++i)
		{
			result[i].x = vec[2 * i];
			result[i].y = vec[2 * i + 1];
		}
		return result;
	}
	public static Color[] dvector_to_color(DVector<float> vec)
	{
		int nLen = vec.Length / 3;
		Color[] result = new Color[nLen];
		for (int i = 0; i < nLen; ++i)
		{
			result[i].r = vec[3 * i];
			result[i].g = vec[3 * i + 1];
			result[i].b = vec[3 * i + 2];
		}
		return result;
	}
	public static int[] dvector_to_int(DVector<int> vec)
	{
		// todo this could be faster because we can directly copy chunks...
		int nLen = vec.Length;
		int[] result = new int[nLen];
		for (int i = 0; i < nLen; ++i)
			result[i] = vec[i];
		return result;
	}

	

}

