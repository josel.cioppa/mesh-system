﻿using UnityEngine;

public class JRibbonCreator : MonoBehaviour {

    public PlayerData playerData;
    JMesh mesh;

    Vector3 lastA, lastB, lastPos;
    Vector2 scale = new Vector2(0.03f, 0.01f);
    float thresh = 0.1f;
    float handSpeed = 0.01f;
    public int currentRibbonIndex = 0;


    public void SetMesh(JMesh mesh)
    {
        this.mesh = mesh;
    }


	public void OnClick()
    {

        var position = transform.InverseTransformPoint(playerData.rightHand.position);
        var forward = transform.InverseTransformDirection(playerData.rightHand.forward);
        var right = transform.InverseTransformDirection(playerData.rightHand.right);

        right *= scale.x;
        forward *= scale.y;

        var v1 = mesh.dMesh.AppendVertex(position - right - forward);
        var v2 = mesh.dMesh.AppendVertex(position + right - forward);
        var v3 = mesh.dMesh.AppendVertex(position - right + forward);
        var v4 = mesh.dMesh.AppendVertex(position + right + forward);

        var t1 = mesh.dMesh.AppendTriangle(v1, v2, v3, currentRibbonIndex);
        var t2 = mesh.dMesh.AppendTriangle(v3, v2, v4, currentRibbonIndex);

        lastA = position - right + forward;
        lastB = position + right + forward;
        lastPos = position;

		// list of vertex updates to push to GPU
		VertexUpdate[] vertexUpdates = new VertexUpdate[]
		{
			VertexUpdate.FromVertexInfo(v1, mesh.dMesh.GetVertexAll(v1)),
			VertexUpdate.FromVertexInfo(v2, mesh.dMesh.GetVertexAll(v2)),
			VertexUpdate.FromVertexInfo(v3, mesh.dMesh.GetVertexAll(v3)),
			VertexUpdate.FromVertexInfo(v4, mesh.dMesh.GetVertexAll(v4))
		};

		mesh.PushNewVertices(vertexUpdates);

		// list of triangle updates to push to GPU
		TriangleUpdate[] triangleUpdates = new TriangleUpdate[]
		{
			new TriangleUpdate()
			{
				a = v1,
				b = v2,
				c = v3,
				index = t1
			},

			new TriangleUpdate()
			{
				a = v3,
				b = v2,
				c = v4,
				index = t2
			}

		};
		mesh.PushNewTriangles(triangleUpdates);


	}



    public void OnHold() {
        
        var position = transform.InverseTransformPoint(playerData.rightHand.position);
        var forward = transform.InverseTransformDirection(playerData.rightHand.forward);
        var right = transform.InverseTransformDirection(playerData.rightHand.right);

        if (Vector3.Distance(position, lastPos) < thresh)
        {
            return;
        }

        right *= scale.x;
        forward *= scale.y;

        var v1 = mesh.dMesh.AppendVertex(lastA);
        var v2 = mesh.dMesh.AppendVertex(lastB);
        var v3 = mesh.dMesh.AppendVertex(position - right + forward);
        var v4 = mesh.dMesh.AppendVertex(position + right + forward);

		var t1 = mesh.dMesh.AppendTriangle(v1, v2, v3, currentRibbonIndex);
        var t2 = mesh.dMesh.AppendTriangle(v3, v2, v4, currentRibbonIndex);

		g3.MeshNormals.QuickCompute(mesh.dMesh);

        lastA = position - right + forward;
        lastB = position + right + forward;
        lastPos = position;
	

		// list of vertex updates to push to GPU
		VertexUpdate[] vertexUpdates = new VertexUpdate[]
		{
			VertexUpdate.FromVertexInfo(v1, mesh.dMesh.GetVertexAll(v1)),
			VertexUpdate.FromVertexInfo(v2, mesh.dMesh.GetVertexAll(v2)),
			VertexUpdate.FromVertexInfo(v3, mesh.dMesh.GetVertexAll(v3)),
			VertexUpdate.FromVertexInfo(v4, mesh.dMesh.GetVertexAll(v4))		
		};
		mesh.PushNewVertices(vertexUpdates);


		TriangleUpdate[] triangleUpdates = new TriangleUpdate[]
		{
			new TriangleUpdate()
			{
				a = v1,
				b = v2,
				c = v3,
				index = t1
			},
			new TriangleUpdate()
			{
				a = v3,
				b = v2,
				c = v4,
				index = t2
			}
		};
		mesh.PushNewTriangles(triangleUpdates);

	}

    public void OnRelease()
    {
        currentRibbonIndex++;
    }

}
