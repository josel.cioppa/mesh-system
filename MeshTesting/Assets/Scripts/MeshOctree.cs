﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class OctreeNode
{

	public g3.DMesh4 mesh;
	public List<int> vertices = new List<int>();
	public OctreeNode[] children;
	public g3.Vector3d center;
	public float width;

	public OctreeNode(g3.Vector3d center, float width)
	{
		this.center = center;
		this.width = width;
	}

	public void Insert(int vID, int limit)
	{
		if (vertices.Count > limit)
		{	
			Split(limit);
		}

		if (children != null)
		{
			var position = mesh.GetVertex(vID);
			int index = IndexFromPosition(position);
			children[index].Insert(vID, limit);
		} else
		{
			vertices.Add(vID);
		}
	}


	public void Remove(int vID)
	{
		if (children == null)
		{
			vertices.Remove(vID);
		} else
		{
			var position = mesh.GetVertex(vID);
			int index = IndexFromPosition(position);
			children[index].Remove(vID);
		}
	}

	public void Split(int limit)
	{

		children = new OctreeNode[8];

		for (int i = 0; i < 2; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				for (int k = 0; k < 2; k++)
				{
					g3.Vector3d direction = new Vector3(i == 0 ? -1 : i, j == 0 ? -1 : 1, k == 0 ? -1 : k);
					g3.Vector3d newChildCenter = center + 0.5f * width * direction;
					int index = i + 2 * j + 4 * k;
					children[index] = new OctreeNode(newChildCenter, 0.5f * width);
					children[index].mesh = mesh;
				}
			}
		}

		for (int i = 0; i < vertices.Count; i++)
		{
			int vID = vertices[i];
			var position = mesh.GetVertex(vID);
			int index = IndexFromPosition(position);
			children[index].Insert(vID, limit);
		}

		vertices.Clear();

	}

	int IndexFromPosition(g3.Vector3d position)
	{
		int i = position.x >= center.x ? 1 : 0;
		int j = position.y >= center.y ? 1 : 0;
		int k = position.z >= center.z ? 1 : 0;
		return i + 2 * j + 4 * k;	
	}

	public void DebugRender()
	{
		Gizmos.DrawWireCube((Vector3) center, new Vector3(2 * width, 2 * width, 2 * width));

		if (children == null) return;

		for (int i = 0; i < 8; i++)
		{
			if (children[i] == null) continue;
			children[i].DebugRender();
		}
	}

	public int ClosestVertexID(Vector3 handPos)
	{
		int closestVID = -1;

		if (vertices.Count > 0)
		{

			float minDist = float.MaxValue;

			for (int i = 0; i < vertices.Count; i++)
			{

				var pos = mesh.GetVertex(vertices[i]);
				float dist = Vector3.SqrMagnitude((Vector3)pos - handPos);

				if (dist < minDist)
				{
					minDist = dist;
					closestVID = vertices[i];
				}

			}

		} else
		{

			int index = IndexFromPosition(handPos);

			if (children != null && children[index] != null)
			{
				closestVID =  children[index].ClosestVertexID(handPos);
			}

		}

		return closestVID;

	}

	public int ClosestQuadWithinRange(Vector3 handPos, float range)
	{
		var closestVertID = ClosestVertexIDWithinRange(handPos, range);
		float smallestDist = float.MaxValue;
		int quadID = -1;

		foreach (var qid in mesh.VtxQuadsItr(closestVertID))
		{

			float dist = Vector3.Magnitude(handPos - (Vector3)mesh.GetPointInQuad(qid, 0.5f * Vector2.one));

			if (dist < smallestDist)
			{
				quadID = qid;
				smallestDist = dist;
			}

		}
		return quadID;
	}

	public int ClosestEdgeIDWithinRange(Vector3 handPos, float range)
	{
		var closestVertID = ClosestVertexIDWithinRange(handPos, range);
		float smallestDist = float.MaxValue;
		int edgeID = -1;

		foreach(var edge in mesh.VtxEdgesItr(closestVertID))
		{

			float dist = Vector3.Magnitude(handPos - (Vector3)mesh.GetEdgePoint(edge, 0.5f));

			if (dist < smallestDist)
			{
				edgeID = edge;
				smallestDist = dist;
			}

		}

		return edgeID;
	}

	public int ClosestVertexIDWithinRange(Vector3 handPos, float range)
	{

		var vertsInRange = VertexIDSWithinRange(handPos, range);

		if (vertsInRange.Count == 0)
		{
			return -1;
		}

		int minID = vertsInRange[0];
		float minDist = Vector3.Distance((Vector3) mesh.GetVertex(minID), handPos);

		for (int i = 1; i < vertsInRange.Count; i++)
		{
			float dist = Vector3.Distance((Vector3) mesh.GetVertex(vertsInRange[i]), handPos);

			if (dist < minDist)
			{
				minDist = dist;
				minID = vertsInRange[i];
			}

		}
		return minID;
	}


	public List<int> VertsWithinRange(Vector3 handPos, float range)
	{

		List<int> vertsInRange = new List<int>();

		if (vertices.Count > 0)
		{
			for (int i = 0; i < vertices.Count; i++)
			{
				var pos = mesh.GetVertex(vertices[i]);
				float dist = Vector3.Magnitude((Vector3)pos - handPos);

				if (dist < range)
				{
					vertsInRange.Add(vertices[i]);					
				}
			}
		}
		else
		{
			if (children != null)
			{
				for (int i = 0; i < 8; i++)
				{
					if (children[i] != null)
					{
						if (children[i].IntersectsSphere(handPos, range))
						{
							vertsInRange.AddRange(children[i].VertsWithinRange(handPos, range));
						}
					}
				}
			}
		}

		return vertsInRange;

	}

	public List<int> QuadsWithinRange(Vector3 handPos, float range)
	{

		List<int> quadsInRange = new List<int>();

		if (vertices.Count > 0)
		{
			for (int i = 0; i < vertices.Count; i++)
			{

				foreach(var qid in mesh.VtxQuadsItr(vertices[i]))
				{

					var pos = mesh.GetPointInQuad(qid, 0.5f * Vector2.one); 
					float dist = Vector3.Magnitude((Vector3)pos - handPos);

					if (dist < range)
					{
						quadsInRange.Add(qid);
					}

				}
			}
		}
		else
		{
			if (children != null)
			{
				for (int i = 0; i < 8; i++)
				{
					if (children[i] != null)
					{
						if (children[i].IntersectsSphere(handPos, range))
						{
							quadsInRange.AddRange(children[i].QuadsWithinRange(handPos, range));
						}
					}
				}
			}
		}

		return quadsInRange;

	}

	public bool IntersectsSphere(Vector3 handPos, float range)
	{
		Bounds sphereBounds = new Bounds(handPos, (range * 2.0f) * Vector3.one);
		Bounds cubeBounds = new Bounds((Vector3) center, (width * 2.0f) * Vector3.one);
		return sphereBounds.Intersects(cubeBounds);
	}

	public int ClosestQuadID(Vector3 handPos)
	{

		int vID = ClosestVertexID(handPos);

		if (vID < 0)
		{
			return vID;
		}

		int closestQuad = -1;
		float minDist = float.MaxValue, dist = float.MaxValue;

		foreach(var qID in mesh.VtxQuadsItr(vID))
		{

			dist = Vector3.SqrMagnitude(handPos - (Vector3) mesh.GetPointInQuad(qID, 0.5f * g3.Vector2d.One));

			if (dist < minDist)
			{
				minDist = dist;
				closestQuad = qID;
			}

		}

		return closestQuad;

	}


	internal List<int> VertexIDSWithinRange(Vector3 handPos, float squaredRadius)
	{

		List<int> idsWithinRange = new List<int>();

		if (vertices.Count > 0)
		{
			foreach (int id in vertices)
			{
				if (Vector3.SqrMagnitude(handPos - (Vector3) mesh.GetVertex(id)) < squaredRadius) {
					idsWithinRange.Add(id);
				}
			}
		} else
		{
			if (children != null)
			{
				for (int i = 0; i < 8; i++)
				{
					if (children[i] != null)
					{
						if (Vector3.SqrMagnitude((Vector3)children[i].center - handPos) < squaredRadius)
						{
							var childIDS = children[i].VertexIDSWithinRange(handPos, squaredRadius);
							idsWithinRange.AddRange(childIDS);
						}
					}
				}
			}
		}

		return idsWithinRange;
	}

}

public class Octree
{

	public int vertexLimit = 200;
	public OctreeNode baseNode;

	public Octree(g3.DMesh4 mesh, g3.Vector3d center, float width)
	{
		baseNode = new OctreeNode(center, width);
		baseNode.mesh = mesh;
	}

	public void Clear(g3.DMesh4 mesh)
	{
		var c = baseNode.center;
		var w = baseNode.width;
		baseNode = new OctreeNode(c, w);
		baseNode.mesh = mesh;
	}

	public void Clear(g3.DMesh4 mesh, g3.Vector3d c, float w)
	{	
		baseNode = new OctreeNode(c, w);
		baseNode.mesh = mesh;
	}

	public void Build(g3.DMesh4 mesh)
	{
		foreach (int vID in mesh.VertexIndices())
		{
			baseNode.Insert(vID, vertexLimit);
		}
	}

	void Insert(int vertID)
	{
		baseNode.Insert(vertID, vertexLimit);
	}

	void Remove(int vertID)
	{
		baseNode.Remove(vertID);
	}

	public void DebugRender()
	{
		baseNode.DebugRender();
	}

	public List<int> VertexIDWithinRange(Vector3 handPos, float squaredRadius)
	{
		return baseNode.VertsWithinRange(handPos, squaredRadius);
	}

	public List<int> QuadsWithinRange(Vector3 handPos, float squaredRadius)
	{
		return baseNode.QuadsWithinRange(handPos, squaredRadius);
	}

	public int ClosestVertexID(Vector3 handPos)
	{
		return baseNode.ClosestVertexID(handPos);
	}

	public int ClosestQuadID(Vector3 handPos)
	{
		return baseNode.ClosestQuadID(handPos);
	}

	internal int ClosestEdgeWithinRange(Vector3 pos, float rad)
	{
		return baseNode.ClosestEdgeIDWithinRange(pos, rad);
	}

	internal int ClosestQuadWithinRange(Vector3 pos, float rad)
	{
		return baseNode.ClosestQuadWithinRange(pos, rad);
	}
}

public class MeshOctree : MonoBehaviour
{

	public JQuadMeshFilter filter;
	Octree octree;

	public void Initialize()
	{
		var center = filter.mesh.dMesh.meshBounds.center;
		var width = Mathf.Max(filter.mesh.dMesh.meshBounds.extents.x, Mathf.Max(filter.mesh.dMesh.meshBounds.extents.y, filter.mesh.dMesh.meshBounds.extents.z));
		octree = new Octree(filter.mesh.dMesh, center, width);
		octree.Build(filter.mesh.dMesh);
	}

	public int ClosestEdgeWithinRange(Vector3 pos, float rad)
	{
		return octree.ClosestEdgeWithinRange(pos, rad);
	}

	public int ClosestQuadWithinRange(Vector3 pos, float rad)
	{
		return octree.ClosestQuadWithinRange(pos, rad);
	}

	public List<int> VerticesWithinRange(Vector3 handPos, float radius)
	{
		return octree.VertexIDWithinRange(handPos, radius);
	}

	public List<int> QuadsWithinRange(Vector3 handPos, float radius)
	{
		return octree.QuadsWithinRange(handPos, radius);
	}

	public int ClosestQuadID(Vector3 handPos)
	{
		return octree.ClosestQuadID(handPos);
	}

	public void Rebuild()
	{
		var c = filter.mesh.dMesh.meshBounds.center;
		var w = filter.mesh.dMesh.meshBounds.extents.x;
		octree.Clear(filter.mesh.dMesh, c, w);
		octree.Build(filter.mesh.dMesh);
	}

	//public void OnDrawGizmos()
	//{
	//	if (octree == null)
	//	{
	//		return;
	//	}
	//	octree.DebugRender();	
	//}

}
