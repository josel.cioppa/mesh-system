﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class UIHitData : ScriptableObject {
	public Vector3 hitPosition;
	public Vector3 raycastOrigin;
	public GameObject hitObject;
}
