﻿using UnityEngine;

public class ButtonSelector : MonoBehaviour {

	public Material selectedMat, unselectedMat;
	public Transform up, down, left, right;
	public VRInputManager input;

	public enum Direction {  up, down, left, right };
	public Direction currentDirection;

	Transform currentButton;

	public Event e_left, e_down, e_up, e_right;

	public void Start()
	{
		currentButton = up;
		SetDirection(Direction.up);
	}

	public void Update()
	{

		if (VRInputManager.leftThumbstick.y > 0.9f)
		{
			if (currentDirection != Direction.up)
			{
				SetDirection(Direction.up);
				return;
			}
		}

		if (VRInputManager.leftThumbstick.y < -0.9f)
		{
			if (currentDirection != Direction.down)
			{
				SetDirection(Direction.down);
				return;
			}
		}

		if (VRInputManager.leftThumbstick.x > 0.9f)
		{
			if (currentDirection != Direction.right)
			{
				SetDirection(Direction.right);
				return;
			}
		}

		if (VRInputManager.leftThumbstick.x < -0.9f)
		{
			if (currentDirection != Direction.left)
			{
				SetDirection(Direction.left);
				return;
			}
		}


	}

	private void SetDirection(Direction newDir)
	{

		currentButton.GetComponent<MeshRenderer>().material = unselectedMat;
		currentDirection = newDir;

		switch (newDir) {

			case Direction.up: {
				up.GetComponent<MeshRenderer>().material = selectedMat;
				currentButton = up;
				e_up.Invoke();	
			} break;

			case Direction.down:
			{
				down.GetComponent<MeshRenderer>().material = selectedMat;
				currentButton = down;
				e_down.Invoke();
			}
			break;

			case Direction.left:
			{
				left.GetComponent<MeshRenderer>().material = selectedMat;
				currentButton = left;
				e_left.Invoke();
			}

			break;

			case Direction.right:
			{
				right.GetComponent<MeshRenderer>().material = selectedMat;
				currentButton = right;
				e_right.Invoke();
			}

			break;

		}

	}

}
