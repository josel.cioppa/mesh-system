﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosestEdgeHighlighter : MeshTool {

	public PlayerData player;
	public JQuadMeshFilter filter;
	public int lastHighlightedEdge = -1;
	public int[] highlightedEdges = new int[1];

	public void Update()
	{

		var eID = mesh.octree.ClosestEdgeWithinRange(transform.InverseTransformPoint(player.rightHand.position), 0.1f);

		if (eID == -1)
		{
			return;
		}

		if (eID == lastHighlightedEdge)
		{
			return;
		}

		mesh.ClearEdgeHighlights(highlightedEdges);
		highlightedEdges[0] = eID;
		mesh.HighlightEdges(highlightedEdges);

		lastHighlightedEdge = eID;

	}

}
