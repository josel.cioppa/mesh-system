﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleFaceHighlighter : MeshTool {

	public PlayerData player;
	int[] quadsInRange = null;

	void Update () {

		var handPos = transform.InverseTransformPoint(player.rightHand.position);

		if (quadsInRange != null)
		{
			if (quadsInRange.Length > 0)
			{
				mesh.ClearQuadHighlights(quadsInRange);
			}
		}

		quadsInRange = mesh.octree.QuadsWithinRange(handPos, 0.1f).ToArray();

		if (quadsInRange.Length > 0)
		{
			mesh.HighlightQuads(quadsInRange);
		}

	}

}
