﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeMover : MeshTool {

	public PlayerData player;
	public int closestEdge = -1;
	public int[] edges = new int[1];
	g3.Index2i vertIDS;
	g3.NewVertexInfo[] vertInfos = new g3.NewVertexInfo[2];

	bool grabbing = false;

	public void Update()
	{

		if (grabbing)
		{
			return;
		}

		var eID = mesh.octree.ClosestEdgeWithinRange(transform.InverseTransformPoint(player.rightHand.position), 0.1f);

		if (eID == -1 || eID == closestEdge)
		{
			return;
		}

		closestEdge = eID;
			
	}

	public Vector3 originalHandPos;

	public void GrabEdge()
	{
		if (closestEdge < 0)
		{
			return;
		}

		grabbing = true;

		originalHandPos = transform.InverseTransformPoint(player.rightHand.position);
		vertIDS = mesh.dMesh.GetEdgeV(closestEdge);
		vertInfos[0] = mesh.dMesh.GetVertexAll(vertIDS.a);
		vertInfos[1] = mesh.dMesh.GetVertexAll(vertIDS.b);
	}

	public void MoveEdge()
	{

		if (closestEdge < 0)
		{
			return;
		}

		var dir = transform.InverseTransformPoint(player.rightHand.position) - originalHandPos;

		var newVA = new g3.NewVertexInfo()
		{
			v = vertInfos[0].v + dir,
			n = vertInfos[0].n,
			c = vertInfos[0].c,
			uv = vertInfos[0].uv
		};

		var newVB = new g3.NewVertexInfo()
		{
			v = vertInfos[1].v + dir,
			n = vertInfos[1].n,
			c = vertInfos[1].c,
			uv = vertInfos[1].uv
		};


		mesh.dMesh.SetVertex(vertIDS.a, newVA);
		mesh.dMesh.SetVertex(vertIDS.b, newVB);

		mesh.PushVertexUpdates(new VertexUpdate[]
		{
			VertexUpdate.FromVertexInfo(vertIDS.a, newVA),
			VertexUpdate.FromVertexInfo(vertIDS.b, newVB)
		});

	}

	public void ReleaseEdge()
	{
		grabbing = false;
		mesh.octree.Rebuild();
	}

}
