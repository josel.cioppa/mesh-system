﻿using UnityEngine;

public class FaceMover :  MeshTool
{

	public PlayerData player;
	public int closestFace = -1;
	g3.Index4i vertIDS;
	g3.NewVertexInfo[] vertInfos = new g3.NewVertexInfo[4];

	bool grabbing = false;
	public Vector3 originalHandPos;

	public void Update()
	{

	

		if (grabbing)
		{
			return;
		}

		var eID = mesh.octree.ClosestQuadWithinRange(transform.InverseTransformPoint(player.rightHand.position), 0.1f);

		if (eID == -1 || eID == closestFace)
		{
			return;
		}

		closestFace = eID;

	}


	public void GrabFace()
	{
		if (closestFace < 0)
		{
			return;
		}

		grabbing = true;

		originalHandPos = transform.InverseTransformPoint(player.rightHand.position);
		vertIDS = mesh.dMesh.GetQuad(closestFace);

		vertInfos[0] = mesh.dMesh.GetVertexAll(vertIDS.a);
		vertInfos[1] = mesh.dMesh.GetVertexAll(vertIDS.b);
		vertInfos[2] = mesh.dMesh.GetVertexAll(vertIDS.c);
		vertInfos[3] = mesh.dMesh.GetVertexAll(vertIDS.d);

	}

	public void MoveFace()
	{

		if (closestFace < 0)
		{
			return;
		}

		var dir = transform.InverseTransformPoint(player.rightHand.position) - originalHandPos;

		var newVA = new g3.NewVertexInfo()
		{
			v = vertInfos[0].v + dir,
			n = vertInfos[0].n,
			c = vertInfos[0].c,
			uv = vertInfos[0].uv
		};

		var newVB = new g3.NewVertexInfo()
		{
			v = vertInfos[1].v + dir,
			n = vertInfos[1].n,
			c = vertInfos[1].c,
			uv = vertInfos[1].uv
		};

		var newVC = new g3.NewVertexInfo()
		{
			v = vertInfos[2].v + dir,
			n = vertInfos[2].n,
			c = vertInfos[2].c,
			uv = vertInfos[2].uv
		};

		var newVD = new g3.NewVertexInfo()
		{
			v = vertInfos[3].v + dir,
			n = vertInfos[3].n,
			c = vertInfos[3].c,
			uv = vertInfos[3].uv
		};

		mesh.dMesh.SetVertex(vertIDS.a, newVA);
		mesh.dMesh.SetVertex(vertIDS.b, newVB);
		mesh.dMesh.SetVertex(vertIDS.c, newVC);
		mesh.dMesh.SetVertex(vertIDS.d, newVD);

		mesh.PushVertexUpdates(new VertexUpdate[]
		{
			VertexUpdate.FromVertexInfo(vertIDS.a, newVA),
			VertexUpdate.FromVertexInfo(vertIDS.b, newVB),
			VertexUpdate.FromVertexInfo(vertIDS.c, newVC),
			VertexUpdate.FromVertexInfo(vertIDS.d, newVD)
		});

	}

	public void ReleaseFace()
	{
		grabbing = false;
		mesh.octree.Rebuild();
	}

}
