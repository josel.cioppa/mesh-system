﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThumbstickScaler : MonoBehaviour {

	public Transform target;
	public float m_scaleSpeed = 2.0f;

	// Update is called once per frame
	void Update () {

		var axis = VRInputManager.rightThumbstick.y;


		if (axis > 0.5f)
		{
			var newScale = target.localScale;
			newScale -= (Time.deltaTime * m_scaleSpeed * Vector3.one);
			newScale.x = Mathf.Clamp(newScale.x, 0.5f, 1.5f);
			newScale.y = Mathf.Clamp(newScale.y, 0.5f, 1.5f);
			newScale.z = Mathf.Clamp(newScale.z, 0.5f, 1.5f);
			target.localScale = newScale;
		}

		if (axis < -0.5f)
		{
			var newScale = target.localScale;
			newScale += (Time.deltaTime * m_scaleSpeed * Vector3.one);
			newScale.x = Mathf.Clamp(newScale.x, 0.5f, 1.5f);
			newScale.y = Mathf.Clamp(newScale.y, 0.5f, 1.5f);
			newScale.z = Mathf.Clamp(newScale.z, 0.5f, 1.5f);
			target.localScale = newScale;
		}

	}
}
